$( document ).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '#go_to_add_test', function(){
        window.location = "addtest";
    });

    
    $('.btn-number').click(function(e){
        
        e.preventDefault();
        
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                
                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                } 
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
       // $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        
        
    });
    $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
    });

    $('.add-parameter').click(function(){

        var html = "";
        html += '<div class="parameter-full m-b-sm clearfix">';
        html += '<div class="col-xs-6 col-md-7 nopadding">';
        html += '<input class="form-control" type="text">';
        html += '</div>';
        html += '<div class="col-xs-5 col-md-4 nopadding">';
        html += '<div class="input-group">';
        html += '<span class="input-group-btn">';
        html += '<button type="button" class="btn btn-gray btn-number" data-type="minus" data-field="quant[1]">';
        html += '<span class="icon icon-minus-circle"></span>';
        html += '</button>';
        html += '</span>';
        html += '<input type="text" name="quant[1]" style="width: 50px;" class="form-control text-center input-number" value="10" min="1" max="100">';
        html += '<span class="input-group-btn">';
        html += '<button type="button" class="btn btn-gray btn-number" data-type="plus" data-field="quant[1]">';
        html += '<span class="icon icon-plus-circle"></span>';
        html += '</button>';
        html += '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-xs-1 col-md-1 nopadding text-center" style="padding-left: 28px;">';
        html += '<a href="#" class="ad-minus"><i class="icon icon-minus-circle"></i></a>';
        html += '</div>';
        html += '</div>';
        html += '<div class="clearfix appenddiv"></div>';
        $('.appenddiv:last').append(html);
    });

    var learner_table=$('.test_learner').DataTable({
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"test_learner_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.dropdown_learner_department_id = $(".learner_department option:selected").val();
                d.dropdown_learner_function_id = $(".learner_function option:selected").val();
                d.dropdown_learner_level_id = $(".learner_level option:selected").val();
                d.dropdown_learner_status_id = $(".learner_status option:selected").val();
                d.searchtxt=$("#searchlearnertxt").val();
            },
        },
        columns: [
                {data:'CheckAll',name:"chk_box",
                title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="assign_learner" type="checkbox"><span class="custom-control-indicator"></span></label>'},
                { name: 'Employee Code',data:'EmployeeCode',title:'Employee Code'},
                { name: 'User Name',data:'UserName' ,title: 'User Name'},
                { name: 'Email Id',data:'EmailId', title: 'Email Id' },
                { name: 'Department',data:'Department',title: 'Department' },
                { name: 'Contact Detail',data:'ContactDetail',title: 'Contact Detail'},
                { name: 'Manager Name',data:'ManagerName',title: 'Manager Name'},
                { name: 'Course Status',data:'CourseStatus',title: 'Course Status' },
                { name: 'Level',data:'Level',title: 'Level' },
                { name: 'Enroll Courses',data:'EnrollCourses', title: 'Enroll Courses' }
            ],
        "columnDefs": [ {
            "targets": [0], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

     var invite_learner_table=$('.invite_test_learner').DataTable({
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"test_invite_learner_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.dropdown_learner_department_id = $(".learner_department option:selected").val();
                d.dropdown_learner_function_id = $(".learner_function option:selected").val();
                d.dropdown_learner_level_id = $(".learner_level option:selected").val();
                d.dropdown_learner_status_id = $(".learner_status option:selected").val();
                d.searchtxt=$("#searchlearnertxt").val();
            },
        },
        columns: [
                {data:'CheckAll',name:"chk_box",
                title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="invite_learner" type="checkbox"><span class="custom-control-indicator"></span></label>'},
                { name: 'Employee Code',data:'EmployeeCode',title:'Employee Code'},
                { name: 'User Name',data:'UserName' ,title: 'User Name'},
                { name: 'Email Id',data:'EmailId', title: 'Email Id' },
                { name: 'Department',data:'Department',title: 'Department' },
                { name: 'Contact Detail',data:'ContactDetail',title: 'Contact Detail'},
                { name: 'Manager Name',data:'ManagerName',title: 'Manager Name'},
                { name: 'Course Status',data:'CourseStatus',title: 'Course Status' },
                { name: 'Level',data:'Level',title: 'Level' },
                { name: 'Enroll Courses',data:'EnrollCourses', title: 'Enroll Courses' }
            ],
        "columnDefs": [ {
            "targets": [0], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $('#test_learner_searchButton').click(function(){
        learner_table.ajax.reload();
    });

    $('.assign_learner_to_test').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        $.ajax({
            type: "POST",
            url: "assign_learner_to_test",
            data: {
                "chk_check_value":arr
            }, 
            success: function(data)
            {
                alert("success");
                learner_table.ajax.reload();
                invite_learner_table.ajax.reload();
            }
        });
    })

    $('.invite_learner_to_test').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        $.ajax({
            type: "POST",
            url: "invite_learner_to_test",
            data: {
                "chk_check_value":arr
            }, 
            success: function(data)
            {
                alert("success");
            }
        });
    })

    //for filter show/hide
    $('.refine_mbutton').click(function(){
        $('.Cus_card-body').slideToggle("slow");
        $('.refine_mbutton').toggleClass("active");
    });

    // //when click on all checkbox
    // $(document).on('click','.select_all_tab', function(){

    //     var $tab_name=$(this).attr("tab_name");
    //     alert(tab_name);
    //     if($(".select_all_tab").is(':checked')){
    //         $('.'+$tab_name).find('.all_user_checkbox').prop('checked',true);
            
    //     }
    //     else{
    //         $('.'+$tab_name).find('.all_user_checkbox').prop('checked',false);
           
            
    //     }
    // });

});
    
    
    //end of learner code-------------------------------------------------------------

