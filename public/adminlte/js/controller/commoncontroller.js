$( document ).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) {
     return this.optional(element) || /^[a-z\s]+$/i.test(value);
    });

    $('.fil').click(function() {
        if ($('.Cus_card-actions, .Cus_card-header').hasClass('active')){
            $('.Cus_card-actions, .Cus_card-header').removeClass('active');  
        } else {
            $('.Cus_card-actions, .Cus_card-header').addClass('active');
          }
    });
    //code for showing dyaamic column list
    $('.col').on('click',function(){
        $('.overrlay').show(500);
    });
    $('.clse').on('click',function(){
        $('.overrlay').hide();
    });
    //end of code

    //code for combine listing
    $('.list-title').click(function(){
        $('.overrlay').show(500); 
    });

    $('.btn-full').click(function(){
        $('.overrlay').hide(); 
    });

    $(".numericOnly").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });
  
  
    // $('.refine_mbutton').click(function(){
    //     $('.Cus_card-body').slideToggle("slow");
    //     $('.refine_mbutton').toggleClass("active");
    // });
   //end of code

    

    //code for setting data-visibility value for checkbox
    $('.switch-input').click(function(){
        if($(this).is(':checked')){
            $(this).closest('li').attr("data-visible", "1");
        }else{
            $(this).closest('li').attr("data-visible", "0");
        }
    });
    //end code

    //code for dragging column
    $(".sortable_menu").sortable({
        items: 'li[class!=non_sortable]',
        distance: 5,
        delay: 300,
        opacity: 0.6,
        cursor: 'move',
        axis: 'y',
        cancel: '.non_sortable',
    });
    $( ".sortable_menu" ).disableSelection();
    //end code

    //code for saving column order and column visibility in session
    $('.save_order').click(function(){
        var form_name=$(this).attr("form_name");
        //alert(form_name);
        var new_col_order='';
        $('.list-order-item').each(function (){
            new_col_order=new_col_order+','+$(this).attr('data-item')+'-'+$(this).attr('data-visible');
        });
        new_col_order = new_col_order.substring(1);
        //for user listing
        if(form_name=="users"){
            $('#user_hidden_column_array').val(new_col_order);
        }else if (form_name=="functions") {
            $('#functions_hidden_column_array').val(new_col_order);
        }else if (form_name=="department") {
            $('#department_hidden_column_array').val(new_col_order);
        }else if (form_name=="development") {
            $('#development_hidden_column_array').val(new_col_order);
        }else if (form_name=="seniority") {
            $('#seniority_hidden_column_array').val(new_col_order);
        }else if(form_name=="competency"){
            $('#competency_hidden_column_array').val(new_col_order);
        }else if(form_name=="designation"){
            $('#designation_hidden_column_array').val(new_col_order);
        }else if(form_name=="learners"){
           $('#learners_hidden_column_array').val(new_col_order);
        }else if(form_name=="learners_team"){
           $('#learners_team_hidden_column_array').val(new_col_order);
        }else if(form_name=="team_listing"){
           $('#team_listing_hidden_column_array').val(new_col_order);
        }else if(form_name=="manager"){
            $('#user_hidden_column_array').val(new_col_order);
        }else if (form_name=="all_user") {
            $('#all_user_hidden_column_array').val(new_col_order);
        }else if (form_name=="hr") {
            $('#hr_hidden_column_array').val(new_col_order);
        }else if (form_name=="expert") {
            $('#expert_hidden_column_array').val(new_col_order);
        }else if (form_name=="category") {
            $('#category_hidden_column_array').val(new_col_order);
        }else if (form_name=="webinar") {
            $('#webinar_hidden_column_array').val(new_col_order);
        }else if (form_name=="test") {
            $('#test_hidden_column_array').val(new_col_order);
        }
        $.ajax({
            url: 'drag_n_drop',
            data: {
               column_array:new_col_order,
               form_name:form_name

            },
            type: 'POST',
            success: function(response) {
                location.reload();
            }
        });
    });
    //end of code

    //when clcik on single checkbox
    $(document).on('click','.myCheckbox', function(){
        if($(".myCheckbox").is(':checked')){
            //class to show button tools when checkbox check
            $(this).closest("tr").addClass('selected');
            $('.common_action').removeClass("hide");
        }
        else{
            //class to hide button tools
            $('.common_action').addClass("hide");
            var select_all_master=0;
            $(".myCheckbox").each(function(){
                if(!$(".myCheckbox").is(':checked')){
                    select_all_master=1;
                }
            });
            if(select_all_master==1){
                $('.select_all').prop('checked',false);
            }

        }
    });

    //when click on all checkbox
    $(document).on('click','.select_all', function(){
        if($(".select_all").is(':checked')){
            $('.myCheckbox').prop('checked', true);
            //class to show button when checkbox check
            $('.common_action').removeClass("hide");
            $('table tr').addClass('selected');
        }
        else{
            $('.myCheckbox').prop('checked', false);
            $('.common_action').addClass("hide");
            $('table tr').removeClass('selected');
        }
    });

    $('.ms').multipleSelect({
            width: '100%'
    });

    

    
    
});

function save_user_type(){
    var role_id=$(this).attr('role_id');
    // alert(role_id);
    $.ajax({
            url: 'save_user_type_for_permission',
            data: {
               role_id:role_id,
            },
            type: 'POST',
            success: function(response) {
                location.reload();
            }
    });
}



