$(document).ready(function(){
    $('#calendarx').fullCalendar({
      header: {
        left: 'title',
        center:'agendaDay,agendaWeek,month',
        right: 'prev,next today'
      },
      defaultDate: '2018-10-06',
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectHelper: true,
      select: function(start, end) {
        var title = prompt('Event Title:');
        var eventData;
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
         $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      eventRender: function(event, element) {
        if (event.SomeBool) {
            element.addClass('bg');
        }
    }
    });
    var calendar = $('#calendar').fullCalendar({
       editable: true,
       header: {
        left: ' today,prevYear,nextYear',
        center: 'title',
        right: 'prev,next,basicDay,month'
       },
       events: {
            url: "getMonthlyEventList"
        }
      });
})