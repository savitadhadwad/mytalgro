$( document ).ready(function() {
    $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });

    //code for dynamic column order
    var column_val=$("#designation_hidden_column_array").val();
    //console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {data:col_name[0],name:"chk_box",
        title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',
        class:col_name[0]};
        else if(col_name[0]=="DesignationCode")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Designation Code",class:col_name[0]};
        else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:col_name[0]};
    }
    //end of code
    //console.log(arrfinal);
  	
    var col="";
    var designation_table=$('.example').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"designation_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.dropdown_tdm_desination = $("#dropdown_tdm_desination option:selected").val();
                d.searchtxt=$("#searchtxt").val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": [0,3], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });
    
    
    //code for show/hide columns
    var col_array = $('#designation_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "designation_table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            designation_table.columns( col_class_name ).visible( true );
        }else{
            designation_table.columns( col_class_name ).visible( false );
        }
    }
    //end of code
    //search filter
    $('#designation_searchButton').click(function () {
        designation_table.ajax.reload();
    });
    //showing edit form
    $(document).on('click', '.edit_button', function(){
        var edit_id=$(this).attr("edit_id");
        $.ajax({
            type: "POST",
            url: "show_designation_edit_form",
            datatype:"JSON",
            data: {edit_id:edit_id}, 
            success: function(data)
            {
                var result = JSON.parse(data);
                $('#hidden_tdm_id').val(result[0]['tdm_id']);
                $('#tdm_designation_code').val(result[0]['tdm_designation_code']);
                $('#tdm_designation').val(result[0]['tdm_designation']);
                $('.dynamic_add_edit_lable').text("Edit");
                $('#err_tdm_designation_code').html("");
                $('#err_tdm_designation').html("");
                $('#err_code_exist').html("");
            }
        });
        
       $('#designation_model').modal('show');
    });

    
    $('#Frmdesignation').validate({
        rules: {
          tdm_designation_code: {
               required: true,
               minlength: 3
           },
          tdm_designation :{
               required: true,
               minlength: 5            
          }
        },
        messages: {
          tdm_designation_code: {
               required: "Code is required",
               minlength: "Code must contain at least {0} characters"
           },
          tdm_designation: {
               required: "Designation is required",
               minlength: "Designation must contain at least {0} characters"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
        submitHandler: function (form) {
            var tdm_designation=$('#tdm_designation').val();
            var tdm_designation_code=$('#tdm_designation_code').val();
            if($('#hidden_tdm_id').val()!=""){
                var tdm_id=$('#hidden_tdm_id').val();
               
                $.ajax({
                    type: "POST",
                    url: "update_designation_master",
                    data: {
                        tdm_id:tdm_id,
                        tdm_designation:tdm_designation,
                        tdm_designation_code:tdm_designation_code
                    }, 
                    success: function(data)
                    {
                        $("#err_tdm_designation_code").html("");
                        $("#err_tdm_designation").html("");
                        $("#err_code_exist").html("");
                        var result = JSON.parse(data);
                        if(result['status']== 'error'){
                            $("#err_tdm_designation_code").html(result['errors']['tdm_designation_code']);
                            $("#err_tdm_designation").html(result['errors']['tdm_designation']);
                            $("#err_code_exist").html(result['errors']['tdm_code_exist']);
                            return false;
                        }
                        if(result['status']=="success"){
                            $('.dynamic_add_edit_lable').text("Add");
                            
                            document.getElementById("Frmdesignation").reset();
                            designation_table.ajax.reload();
                            $('#add_edit_sucess_msg').removeClass("hide");
                            setTimeout(function(){
                                $('#add_edit_sucess_msg').addClass("hide");
                                $('#designation_model').modal('hide');
                            },2000);

                        }
                    }
                });
                //return false;
            }else{
                //save record
                $.ajax({
                    type: "POST",
                    url: "add_designation_form",
                    datatype:"JSON",
                    data: {tdm_designation_code:tdm_designation_code,
                            tdm_designation:tdm_designation,
                    }, 
                    success: function(data)
                    {
                        //alert(data);
                        $("#err_tdm_designation_code").html("");
                        $("#err_tdm_designation").html("");
                        $("#err_code_exist").html("");
                        var result = JSON.parse(data);
                        if(result['status']== 'error'){
                            $("#err_tdm_designation_code").html(result['errors']['tdm_designation_code']);
                            $("#err_tdm_designation").html(result['errors']['tdm_designation']);
                            $("#err_code_exist").html(result['errors']['tdm_code_exist']);
                            return false;
                        }
                        if(result['status']=="success"){
                            $('#add_edit_sucess_msg').removeClass("hide");
                            
                            document.getElementById("Frmdesignation").reset();
                            designation_table.ajax.reload();

                            setTimeout(function(){
                                $('#add_edit_sucess_msg').addClass("hide");
                                $('#designation_model').modal('hide');
                                },2000);
                        }
                    }
                });
                    //return false;
                }//end of else condition
            }
    });

    //for delete single designation
    $(document).on('click', '.submit_form', function(){
        var delete_id=$(this).attr("delete_id");
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this Code!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel it!",   
                closeOnConfirm: false,   
               closeOnCancel: false 
            },function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "delete_designation_code",
                        data: {
                            delete_id:delete_id
                        }, 
                        success: function(data)
                        {
                             swal("Deleted!", "Your Designation has been deleted.", "success"); 
                             designation_table.ajax.reload();
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Designation is safe :)", "error");   
                } 
            });
    });

    //common action
    $('.click_common_action').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Designation!";
        else if(action=="enable")
            var text="You want to enable all Designation";
        else if(action=="disable")
            var text="You want to disable all Designation";
       
        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "designation_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            if(action=="delete")
                                swal("Deleted!", "Your Designation has been Deleted.", "success");
                            else if(action=="enable")
                                swal("Enable","Your Designation has been Enable ","success");
                            else if(action=="disable")
                                swal("Disable","Your Designation has been disable","success"); 
                            designation_table.ajax.reload();
                            $('.select_all').prop('checked',false);
                            $('.example tr').removeClass("selected");
                        }
                    });

                   
                } else {     
                  swal("Cancelled", "Your Codes are safe :)", "error");   
                }
            }); 
    });
    
    //add or edit competency model box on close event
    $('#designation_model').on('hidden.bs.modal', function (e) {
        $('.dynamic_add_edit_lable').text("Add");
        $('#err_tdm_designation_code').html("");
        $('#err_tdm_designation').html("");
        $('#err_code_exist').html("");
        document.getElementById("Frmdesignation").reset();
        $('#hidden_tdm_id').val("");
        $('#designation_model').modal('hide');
    });
    //bulk upload competency model box on close event
    $('#upload_designation_modal').on('hidden.bs.modal', function (e) {
        $('#all_upload_error_msg').addClass("hide");
        $('#upload_success_msg').addClass("hide");
        if(!$('#blank_error_msg').hasClass("hide")){
            $('#blank_error_msg').addClass("hide");
        }
        document.getElementById("Frmdesignationcsv").reset();
        $('#upload_designation_modal').modal('hide');
    });

    //cancel add/edit competency model box
    $('.cancel_edit_form').click(function(){
        $('#err_tdm_designation_code').html("");
        $('#err_tdm_designation').html("");
        $('#err_code_exist').html("");
        $('#hidden_tdm_id').val("");
        $('#designation_model').modal('hide');
    });

    //bulk upload excel file
    $('.upload_designation_csv').click(function(){
        if(!$("#upload_success_msg").hasClass("hide"))
            $('#upload_success_msg').addClass("hide");
        if(!$("#all_upload_error_msg").hasClass("hide"))
            $('#all_upload_error_msg').addClass("hide");
        if(!$("#blank_error_msg").hasClass("hide"))
            $('#blank_error_msg').addClass("hide");
        $('.error_msg').text("");
        $('.myprogress').css('width', '0');
        $('.msg').text('');

        //validation for file format
        var file_data = $('#upload_designation').prop('files')[0];
        var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
        if(filename=="" || filename=="undefined"){
            $('#blank_error_msg').removeClass("hide");
            $('#blank_error_msg').html("Please Upload Excel file");
            return false;
        }
        var ext = $("#upload_designation").val().split('.').pop();
        var fileExtension = ['XLS', 'XLSX', 'xlsx', 'xlx'];
        
        if ($.inArray(ext, fileExtension) == -1) {
            $('#blank_error_msg').removeClass("hide");
            $('#blank_error_msg').html("Please upload excel file only.formats are allowed : "+fileExtension.join(', '));
            return false;
        }
        var form_data = new FormData();
        form_data.append('file', file_data);
        $('.msg').text('Uploading in progress...');

        $.ajax({
            type: "POST",
            url: "upload_designation_csv",
            dataType    : 'text',           // what to expect back from the PHP script, if anything
            cache       : false,
            contentType : false,
            processData : false,
            data        : form_data,

            // this part is progress bar
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.myprogress').text(percentComplete + '%');
                        $('.myprogress').css('width', percentComplete + '%');
                    }
                }, false);
                return xhr;
            },
            success: function(data){
                //console.log(data);
                $('.myprogress').css('width', '0%');
                $('.myprogress').text('0%');
                $('.msg').text('');
                document.getElementById("Frmdesignationcsv").reset();
                var result = JSON.parse(data);
                var newHTML = [];
                if(result['status']=='all_duplicate'){
                    var substr=result['errors'];
                    for (i = 0; i < substr.length; ++i) {
                        newHTML.push(substr[i]+',');

                    }
                    var lastChar = newHTML.join("").slice(-1);
                    if (lastChar == ',') {
                      var strVal = newHTML.join("").slice(0, -1);
                    }
                    var array = strVal.split(',');
                    $.each(array, function(index, value) {
                      $("#failed_record_list").append("<li>" + value + "</li>");
                    });
                    $('#total_record').text(result['total_count_record']);
                    $('#failed_record').text(result['failed_record']);
                    $("#all_upload_error_msg").removeClass("hide");
               }else if (result['status']=='success_n_duplicate') {
                    var substr=result['errors'];
                    for (i = 0; i < substr.length; ++i) {
                        newHTML.push(substr[i]+',');
                    }
                    var lastChar = newHTML.join("").slice(-1);
                    if (lastChar == ',') {
                      var strVal = newHTML.join("").slice(0, -1);
                    }
                    var array = strVal.split(',');
                    $.each(array, function(index, value) {
                      $("#failed_record_list").append("<li>" + value + "</li>");
                    });
                    $('#total_record').text(result['total_count_record']);
                    $('#failed_record').text(result['failed_record']);
                    $("#all_upload_error_msg").removeClass("hide");
                    designation_table.ajax.reload();
               }else if(result['status']=='no_record_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("No record found in excel");
               }else if(result['status']=="success_all_record"){
                    $('#upload_sucess_msg').removeClass("hide");
                    designation_table.ajax.reload();
                    setTimeout(function(){
                        $('#upload_sucess_msg').addClass("hide");
                        $('#upload_designation_modal').modal('hide');
                    },  2000);
               }else if(result['status']=='header_format_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("Please upload proper format.Please see download sample format for referrence");
               }else if(result['status']=='format_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("Please upload only Excel file");
               }
            }//success function end
        });
    });

    
});


