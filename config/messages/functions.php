<?php 
return [
	'FUNCTIONS_LABEL'=>'Functions',
	'FUNCTIONS_LIST' => 'Functions List',
    'FUNCTIONS_CODE_ERROR' => 'Please Enter Competecy Code',
    'FUNCTIONS_TITLE_ERROR'=>'Please Enter Competecy Title',
    'FUNCTIONS_CODE_EXIST' => 'Functions Code already exist',
    'FUNCTIONS_BULK_VALID_FORMAT'=>'Please upload valid format',
    'FUNCTIONS_VALID_FORMAT_SUCCESS'=>'File uploaded Successfully',
    'FUNCTIONS_INSERT_ERROR'=>'File has some issue.Please check the format'
 ];

