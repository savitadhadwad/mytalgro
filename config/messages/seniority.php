<?php 
return [
	'SENIORITY_LABEL'=>'Seniority',
	'SENIORITY_LIST' => 'Seniority List',
    'SENIORITY_CODE_ERROR' => 'Please Enter Competecy Code',
    'SENIORITY_TITLE_ERROR'=>'Please Enter Competecy Title',
    'SENIORITY_CODE_EXIST' => 'Seniority Code already exist',
    'SENIORITY_BULK_VALID_FORMAT'=>'Please upload valid format',
    'SENIORITY_VALID_FORMAT_SUCCESS'=>'File uploaded Successfully',
    'SENIORITY_INSERT_ERROR'=>'File has some issue.Please check the format'
 ];

