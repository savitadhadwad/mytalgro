<?php 
return [
	'USER_LEVEL_TOOLTIP'=>'Testing user level tooltip',
	'EXPERT_AREA_TOOLTIP'=>'Expert Area Tooltip',
	'USER_BIO'=>'User Bio',
	'USER_TYPE_ERROR'=>'Please Select User Type',
	'PERMISSION_TYPE_ERROR'=>'Please Select Permission Type',
	'EMPLOYEE_CODE_ERROR'=>'Please Enter Employee Code',
	'USER_FIRSTNAME_ERROR'=>'Please Enter Firstname',
	'USER_LASTNAME_ERROR'=>'Please Enter Lastname',
	'USER_MIDDLENAME_ERROR'=>'Please Enter Middlename',
	'GENDER_ERROR'=>'Please Select Gender',
	'EMAIL_ERROR'=>'Please Select Email',
	'MOBILE_NO_ERROR'=>'Please enter Mobile Number',
	'EMAIL_EXIST_ERROR'=>'Email already exist.please try again with another email id',
	'ADD_USER_TITLE_BAR_DESCRIPTION'=>'Lising of all User',
	'ADD_USER_TITLE_BAR'=>'Add Learner'
 ];