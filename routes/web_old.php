<?php
Route::get('/', 'Auth\LoginController@showLoginForm')->name('auth.login');
Route::get('course/{slug}', ['uses' => 'CoursesController@show', 'as' => 'courses.show']);
Route::post('course/payment', ['uses' => 'CoursesController@payment', 'as' => 'courses.payment']);
Route::post('course/{course_id}/rating', ['uses' => 'CoursesController@rating', 'as' => 'courses.rating']);

Route::get('lesson/{course_id}/{slug}', ['uses' => 'LessonsController@show', 'as' => 'lessons.show']);
Route::post('lesson/{slug}/test', ['uses' => 'LessonsController@test', 'as' => 'lessons.test']);

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::resource('home', 'Admin\DashboardController');
    // Route::get('/home', 'Admin\DashboardController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');  
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);

    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('courses', 'Admin\CoursesController');
    Route::post('courses_mass_destroy', ['uses' => 'Admin\CoursesController@massDestroy', 'as' => 'courses.mass_destroy']);
    Route::post('courses_restore/{id}', ['uses' => 'Admin\CoursesController@restore', 'as' => 'courses.restore']);
    Route::delete('courses_perma_del/{id}', ['uses' => 'Admin\CoursesController@perma_del', 'as' => 'courses.perma_del']);
    Route::resource('lessons', 'Admin\LessonsController');
    Route::post('lessons_mass_destroy', ['uses' => 'Admin\LessonsController@massDestroy', 'as' => 'lessons.mass_destroy']);
    Route::post('lessons_restore/{id}', ['uses' => 'Admin\LessonsController@restore', 'as' => 'lessons.restore']);
    Route::delete('lessons_perma_del/{id}', ['uses' => 'Admin\LessonsController@perma_del', 'as' => 'lessons.perma_del']);
    Route::resource('questions', 'Admin\QuestionsController');
    Route::post('questions_mass_destroy', ['uses' => 'Admin\QuestionsController@massDestroy', 'as' => 'questions.mass_destroy']);
    Route::post('questions_restore/{id}', ['uses' => 'Admin\QuestionsController@restore', 'as' => 'questions.restore']);
    Route::delete('questions_perma_del/{id}', ['uses' => 'Admin\QuestionsController@perma_del', 'as' => 'questions.perma_del']);
    Route::resource('questions_options', 'Admin\QuestionsOptionsController');
    Route::post('questions_options_mass_destroy', ['uses' => 'Admin\QuestionsOptionsController@massDestroy', 'as' => 'questions_options.mass_destroy']);
    Route::post('questions_options_restore/{id}', ['uses' => 'Admin\QuestionsOptionsController@restore', 'as' => 'questions_options.restore']);
    Route::delete('questions_options_perma_del/{id}', ['uses' => 'Admin\QuestionsOptionsController@perma_del', 'as' => 'questions_options.perma_del']);
    Route::resource('tests', 'Admin\TestsController');
    Route::post('tests_mass_destroy', ['uses' => 'Admin\TestsController@massDestroy', 'as' => 'tests.mass_destroy']);
    Route::post('tests_restore/{id}', ['uses' => 'Admin\TestsController@restore', 'as' => 'tests.restore']);
    Route::delete('tests_perma_del/{id}', ['uses' => 'Admin\TestsController@perma_del', 'as' => 'tests.perma_del']);
    Route::post('/spatie/media/upload', 'Admin\SpatieMediaController@create')->name('media.upload');
    Route::post('/spatie/media/remove', 'Admin\SpatieMediaController@destroy')->name('media.remove');

    Route::post('getSelectedPermissions', 'Admin\RolesController@getSelectedPermissions');
    //show content library section
    Route::resource('contentlibrary', 'Admin\ContentLibraryController');
    //for edit content from content library
    Route::post('editArticle','Admin\ContentLibraryController@editArticle');
    //for delete content for content library
    Route::post('deleteArticle','Admin\ContentLibraryController@deleteArticle');
    //update content from content library
    Route::post('updateArticle','Admin\ContentLibraryController@updateArticle');
    //add content in content library
    Route::post('addArticle','Admin\ContentLibraryController@addArticle');
    //update content active/inactive
    Route::post('updateIsActiveContent','Admin\ContentLibraryController@updateIsActiveContent');
    //add content to favourite tab
    Route::post('updateIsFavContent','Admin\ContentLibraryController@updateIsFavContent');
    //get developement area list in dropdown
    Route::post('getDevelopementArea','Admin\ContentLibraryController@getDevelopementArea');
    //get contents in list
    Route::post('getArticles','Admin\ContentLibraryController@getArticles');
    //get content data as per pagination dynamically
    Route::resource('pagination', 'Admin\ContentLibraryController@pagination');
    //on change of pagination pages get data
    Route::post('ajax/pagination','Admin\ContentLibraryController@pagination');
    //get next record in pagination
    Route::get('ajax/getNextArt','Admin\ContentLibraryController@getNextArt');
    //get crop image in content
    Route::post('image-crop', 'Admin\ContentLibraryController@imageCropPost');
    //show team management page
    Route::resource('team', 'Admin\TeamController');  
    //add team 
    Route::post('addTeam','Admin\TeamController@addTeam'); 

    Route::any('learners_ajax_listing','Admin\TeamController@learners_ajax_listing');   

    Route::any('learners_common_action', 'Admin\TeamController@learners_common_action');    

     //for adding functions form
    Route::post("add_learners_form","Admin\TeamController@save_learners_data");
    //for showing edit form
    Route::post("show_learners_edit_form","Admin\TeamController@show_learners_edit_form");
    //for updating learners master
    Route::post("update_learners_master","Admin\TeamController@update_learners_master");
    //for uploading learners csv 
    Route::any("upload_learners_csv","Admin\TeamController@upload_learners_csv");
    //for deleting seniority code
    Route::post("delete_learners_code","Admin\TeamController@delete_learners_code");
    //for exporting excel file for learners
    Route::any("learners_excel","Admin\TeamController@learners_download_excel");
    //for exporting pdf file
    Route::any("learners_pdf","Admin\TeamController@learners_download_PDF");
    //for downloading sample file
    Route::any("download_sample_learners_excel","Admin\TeamController@download_sample_learners_excel");     

    //for sorting column and saving order in session
    Route::any('drag_n_drop'  , 'Admin\CommonController@save_session_info');

    
    // //for user 
    Route::any('user_manager_ajax_listing', 'Admin\UsersController@user_manager_ajax_listing');
    //for all user
    Route::any('all_user_ajax_listing','Admin\UsersController@user_all_ajax_listing');
    //for hr listing
    Route::any('hr_ajax_listing','Admin\UsersController@hr_ajax_listing');
    //for expert listing
    Route::any('expert_ajax_listing','Admin\UsersController@expert_ajax_listing');
    //for learner listing
    Route::any('learner_ajax_listing','Admin\UsersController@learner_ajax_listing');
    //for instructor listing
    Route::any('instructor_ajax_listing','Admin\UsersController@instructor_ajax_listing');
    //for deleting single user
    Route::post("delete_user","Admin\UsersController@delete_user");
    //for user common action
    Route::any('user_common_action', 'Admin\UsersController@user_common_action');
    //for exporting pdf for users
    Route::any("user_pdf/{user_role}","Admin\UsersController@user_download_PDF");
    //for exporting excel file for user
    Route::any("user_excel/{user_role}","Admin\UsersController@user_download_excel");

    //for dispalying competency
    Route::resource('competency','Admin\CompetencyController');
    Route::any('competency_ajax_listing', 'Admin\CompetencyController@competency_ajax_listing');
    //for common action  competency_common_action
    Route::any('competency_common_action', 'Admin\CompetencyController@competency_common_action');
    //for adding competency form
    Route::post("add_competency_form","Admin\CompetencyController@save_competency_data");
    //for showing edit form
    Route::post("show_competency_edit_form","Admin\CompetencyController@show_competency_edit_form");
    //for updating competency master
    Route::post("update_competency_master","Admin\CompetencyController@update_competency_master");
    //for uploading competency csv 
    Route::any("upload_competency_csv","Admin\CompetencyController@upload_competency_csv");
    //for deleting competency code
    Route::post("delete_competency_code","Admin\CompetencyController@delete_competency_code");
    //for exporting excel file for competency
    Route::any("competency_excel","Admin\CompetencyController@competency_download_excel");
    //for exporting pdf file
    Route::any("competency_pdf","Admin\CompetencyController@competency_download_PDF");
    //for downloading sample file
    Route::any("download_sample_competency_excel","Admin\CompetencyController@download_sample_competency_excel");
    

    //for designation//
    Route::resource('designation','Admin\DesignationController');
    //for ajax listing
    Route::any('designation_ajax_listing', 'Admin\DesignationController@designation_ajax_listing');
    //for common action  designation_common_action
    Route::any('designation_common_action', 'Admin\DesignationController@designation_common_action');
     //for adding competency form
    Route::post("add_designation_form","Admin\DesignationController@save_designation_data");
    //for showing edit form
    Route::post("show_designation_edit_form","Admin\DesignationController@show_designation_edit_form");
    //for updating competency master
    Route::post("update_designation_master","Admin\DesignationController@update_designation_master");
    //for uploading competency csv 
    Route::any("upload_designation_csv","Admin\DesignationController@upload_designation_csv");
    //for deleting competency code
    Route::post("delete_designation_code","Admin\DesignationController@delete_designation_code");
    //for exporting excel file for competency
    Route::any("designation_excel","Admin\DesignationController@designation_download_excel");
    //for exporting pdf file
    Route::any("designation_pdf","Admin\DesignationController@designation_download_PDF");
    //for downloading sample file
    Route::any("download_sample_designation_excel","Admin\DesignationController@download_sample_designation_excel");


    //for department//
    Route::resource('department','Admin\DepartmentController');
    //for ajax listing
    Route::any('department_ajax_listing', 'Admin\DepartmentController@department_ajax_listing');
    //for common action  designation_common_action
    Route::any('department_common_action', 'Admin\DepartmentController@department_common_action');
     //for adding competency form
    Route::post("add_department_form","Admin\DepartmentController@save_department_data");
    //for showing edit form
    Route::post("show_department_edit_form","Admin\DepartmentController@show_department_edit_form");
    //for updating competency master
    Route::post("update_department_master","Admin\DepartmentController@update_department_master");
    //for uploading competency csv 
    Route::any("upload_department_csv","Admin\DepartmentController@upload_department_csv");
    //for deleting competency code
    Route::post("delete_department_code","Admin\DepartmentController@delete_department_code");
    //for exporting excel file for competency
    Route::any("department_excel","Admin\DepartmentController@department_download_excel");
    //for exporting pdf file
    Route::any("department_pdf","Admin\DepartmentController@department_download_PDF");
    //for downloading sample file
    Route::any("download_sample_department_excel","Admin\DepartmentController@download_sample_department_excel");

    //for development//
    Route::resource('development','Admin\DevelopmentController');
    //for ajax listing
    Route::any('development_ajax_listing', 'Admin\DevelopmentController@development_ajax_listing');
    //for common action  designation_common_action
    Route::any('development_common_action', 'Admin\DevelopmentController@development_common_action');
     //for adding competency form
    Route::post("add_development_form","Admin\DevelopmentController@save_development_data");
    //for showing edit form
    Route::post("show_development_edit_form","Admin\DevelopmentController@show_development_edit_form");
    //for updating competency master
    Route::post("update_development_master","Admin\DevelopmentController@update_development_master");
    //for uploading competency csv 
    Route::any("upload_development_csv","Admin\DevelopmentController@upload_development_csv");
    //for deleting competency code
    Route::post("delete_development_code","Admin\DevelopmentController@delete_development_code");
    //for exporting excel file for competency
    Route::any("development_excel","Admin\DevelopmentController@development_download_excel");
    //for exporting pdf file
    Route::any("development_pdf","Admin\DevelopmentController@development_download_PDF");
    //for downloading sample file
    Route::any("download_sample_development_excel","Admin\DevelopmentController@download_sample_development_excel");

    //for seniority
    Route::resource('seniority','Admin\SeniorityController');
    //for ajax listing
    Route::any('seniority_ajax_listing', 'Admin\SeniorityController@seniority_ajax_listing');
    //for common action  seniority_common_action
    Route::any('seniority_common_action', 'Admin\SeniorityController@seniority_common_action');
     //for adding seniority form
    Route::post("add_seniority_form","Admin\SeniorityController@save_seniority_data");
    //for showing edit form
    Route::post("show_seniority_edit_form","Admin\SeniorityController@show_seniority_edit_form");
    //for updating seniority master
    Route::post("update_seniority_master","Admin\SeniorityController@update_seniority_master");
    //for uploading seniority csv 
    Route::any("upload_seniority_csv","Admin\SeniorityController@upload_seniority_csv");
    //for deleting seniority code
    Route::post("delete_seniority_code","Admin\SeniorityController@delete_seniority_code");
    //for exporting excel file for seniority
    Route::any("seniority_excel","Admin\SeniorityController@seniority_download_excel");
    //for exporting pdf file
    Route::any("seniority_pdf","Admin\SeniorityController@seniority_download_PDF");
    //for downloading sample file
    Route::any("download_sample_seniority_excel","Admin\SeniorityController@download_sample_seniority_excel");

    //for Category
    Route::resource('course_category','Admin\CategoryController');
    //for ajax listing
    Route::any('category_ajax_listing', 'Admin\CategoryController@category_ajax_listing');
    //for common action  category_common_action
    Route::any('category_common_action', 'Admin\CategoryController@category_common_action');
     //for adding category form
    Route::post("add_category_form","Admin\CategoryController@save_category_data");
    //for showing edit form
    Route::post("show_category_edit_form","Admin\CategoryController@show_category_edit_form");
    //for updating category master
    Route::post("update_category_master","Admin\CategoryController@update_category_master");
    //for uploading category csv 
    Route::any("upload_category_csv","Admin\CategoryController@upload_category_csv");
    //for deleting category code
    Route::post("delete_category_code","Admin\CategoryController@delete_category_code");
    //for exporting excel file for category
    Route::any("category_excel","Admin\CategoryController@category_download_excel");
    //for exporting pdf file
    Route::any("category_pdf","Admin\CategoryController@category_download_PDF");
    //for downloading sample file
    Route::any("download_sample_category_excel","Admin\CategoryController@download_sample_category_excel");

    //for functions
    Route::resource('functions','Admin\FunctionsController');
    //for ajax listing
    Route::any('functions_ajax_listing', 'Admin\FunctionsController@functions_ajax_listing');
    //for common action  functions_common_action
    Route::any('functions_common_action', 'Admin\FunctionsController@functions_common_action');
     //for adding functions form
    Route::post("add_functions_form","Admin\FunctionsController@save_functions_data");
    //for showing edit form
    Route::post("show_functions_edit_form","Admin\FunctionsController@show_functions_edit_form");
    //for updating functions master
    Route::post("update_functions_master","Admin\FunctionsController@update_functions_master");
    //for uploading functions csv 
    Route::any("upload_functions_csv","Admin\FunctionsController@upload_functions_csv");
    //for deleting seniority code
    Route::post("delete_functions_code","Admin\FunctionsController@delete_functions_code");
    //for exporting excel file for functions
    Route::any("functions_excel","Admin\FunctionsController@functions_download_excel");
    //for exporting pdf file
    Route::any("functions_pdf","Admin\FunctionsController@functions_download_PDF");
    //for downloading sample file
    Route::any("download_sample_functions_excel","Admin\FunctionsController@download_sample_functions_excel"); 



    //for showing add user form
    Route::any("add_user_form/{hidden_user_type}","Admin\UsersController@add_user_form"); 
    //save record of add user
    Route::any("add_user_form/add_user/add_user_ajax","Admin\UsersController@add_user_ajax");  
    //showing edit form of user
    Route::any("user_restore/edit_user_ajax","Admin\UsersController@edit_user_ajax");
    
    Route::any('user_restore/{user_id}', ['uses' => 'Admin\UsersController@show_user_edit_form', 'as' => 'users.restore']);
    Route::any('user_restore/edit_user/psyometric_test_ajax_listing','Admin\UsersController@psyometric_test_ajax_listing');

    Route::any('user_restore/edit_user/course_ajax_listing','Admin\UsersController@course_ajax_listing');

    Route::any("add_user_form/add_user/getstate_list","Admin\UsersController@getUsersStatesList");

    Route::any("user_restore/add_user/getstate_list","Admin\UsersController@getUsersStatesList");
    

});

Route::any('admin/user_restore/psyometric_test_ajax_listing','Admin\UsersController@psyometric_test_ajax_listing');

//notification on header
Route::post('app/Profile/notification','Profile\NotificationController@getData');

Route::post('readNotification','Profile\NotificationController@readNotification');
//notification ends here
//for profile functionality
Route::get('profiles','Profile\ProfileController@index');

Route::post('uploadimg','Profile\ProfileController@uploadProfilePicture');

Route::post('updateProfile','Profile\ProfileController@updateProfile');
//profile functionality ends here







