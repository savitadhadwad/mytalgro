<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Hash;
use Validator;
use DB;
use Mail;

class ChangePasswordController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Where to redirect users after password is changed.
     *
     * @var string $redirectTo
     */
    protected $redirectTo = '/change_password';

    /**
     * Change password form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePasswordForm()
    {
        $user = Auth::getUser();

        return view('auth.change_password', compact('user'));
    }

    /**
     * Change password.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        $user = Auth::getUser();
        $this->validator($request->all())->validate();
        if (Hash::check($request->get('current_password'), $user->password)) {
            $user->password = $request->get('new_password');
            $user->save();

       $posts=DB::select("select * from email_template where email_code='change_password'");

       $posts=json_decode(json_encode($posts),true);
       $template=$posts[0]['email_template'];

       // $template=htmlentities($template);
       
           $data=[];
           // $link=url(config('app.url').route('password.reset', $token, false));
           $template= str_replace("<USER>",auth()->user()->name,$template);
           // $template=str_replace("link",$link,$template);
          
           
           Mail::send([], $data, function ($message) use ($template)
           { 
               $message->from(Config::get('myconstants.FROM_EMAIL'))->subject('Change Password');

               $message->to(auth()->user()->email);
               $message->setBody($template,'text/html');

           });

            $request->session()->flash('message','Password change successfully!');
            $request->session()->flash('alert-class','alert-success');
            return redirect()->to('/change_password'); 
            // return redirect($this->redirectTo)->with('success', 'Password change successfully!');
        } else {
            $request->session()->flash('message','Current password is incorrect');
            $request->session()->flash('alert-class','alert-danger');
            return redirect()->to('/change_password');
            // return redirect()->back()->withErrors('Current password is incorrect');
        }
    }

    /**
     * Get a validator for an incoming change password request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'current_password' => 'required',
            'new_password' => 'required|min:6|confirmed',
        ]);
    }
}
