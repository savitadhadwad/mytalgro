<?php

namespace App\Http\Controllers\Admin;

use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;

class TestController extends Controller
{
    use CommonTrait;
    
    public function index(){
        $array_code_data=$this->get_functions_code_list();
        $array_courses=$this->get_courses();
        return view('admin.test.index', compact('array_code_data','array_courses'));
    }
    public function createTest(){
        return view('admin.test.create_test');
    }
    //listing of competency
    public function test_ajax_listing(Request $request){
        
        $columns=array(0=>'chk_box',1=>'tte_test_name',2=>'tte_test_function');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_functions_code=$request->input('dropdown_functions_code');
        $dropdown_tte_course_id=$request->input('dropdown_tte_course_id');
        $test_type=$request->input('test_type');
        $txt_parameter=$request->input('txt_parameter');
        $search=$request->input('searchtxt');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tte_test_name) LIKE '".$search."%') ";
            // $where .=" OR LOWER(tte_test_function) LIKE '".$search."%' )";
        }
        if($dropdown_functions_code!=""){
            $where .=" AND ";
            $where .=" tte_test_function IN('".$dropdown_functions_code."' )";
        }

        if($dropdown_tte_course_id!=""){
            $where .=" AND ";
            $where .=" tte_course_id ='".$dropdown_tte_course_id."' ";
        }
        if($txt_parameter!=""){
            $where .=" AND ";
            $where .=" tte_parameters IN('".$txt_parameter."' )";
        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY tte_id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;
        
        $total_record = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('test_hidden_column_array')){
                $col_arr_value = $request->session()->get('test_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                 foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"   value="'.$row['tte_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="TestName"){
                            $nesteddata['TestName']=ucwords($row['tte_test_name']);   
                        }
                        if($value[0]=="Parameters"){
                            $nesteddata['Parameters']=ucwords($row['tte_parameters']);
                        }
                        if($value[0]=="AssociatedCourses"){
                            $nesteddata['AssociatedCourses']=$row['tc_title'];
                        }
                        if($value[0]=="TestFunction"){
                            $nesteddata['TestFunction']=ucwords($row['tte_test_function']);
                        }
                        if($value[0]=="Date"){
                            $nesteddata['Date']=ucwords($row['tte_start_date']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tte_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tte_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"   value="'.$row['tte_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['TestName']=ucwords($row['tte_test_name']);
                    $nesteddata['Parameters']=ucwords($row['tte_parameters']);
                    $nesteddata['AssociatedCourses']=ucwords($row['tc_title']);
                    $nesteddata['TestFunction']=ucwords($row['tte_test_function']);
                    $nesteddata['Date']=ucwords($row['tte_start_date']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tte_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tte_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //competency common action enable,disable and delete
    public function test_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_test')
                    ->whereIn('tte_id', $arr_value)
                    ->update(['tte_is_active' => 1]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_test')
                    ->whereIn('tte_id', $arr_value)
                    ->update(['tte_is_active' => 0]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_test')
                    ->whereIn('tte_id', $arr_value)
                    ->update(['tte_is_deleted' => 1]);
           
        }
    }
    
    //delete functions code
    public function delete_test_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_test')
                ->where('tte_id', $delete_id)
                ->update(['tte_is_deleted' => 1]);
        }
    }
    //download data into excel format
    public function test_download_excel(){
        $array_data = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0'") );
        $array_data=json_decode(json_encode($array_data),true);
        $customer_array[] = array('Test Name', 'Parameters','Associated Courses','Test Function','Date');
         foreach($array_data as $arr){
          $customer_array[] = array(
           'Test Name'  => $arr->tte_test_name,
           'Parameters'   => $arr->tte_parameters,
           'Associated Courses'=>$arr->tc_title,
           'Test Function'=>$arr->tte_test_function,
           'Date'=>$arr->tte_start_date
          );
        }
        return Excel::create('Test', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function test_download_PDF(){
        $test_data = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0'") );
        $test_data=json_decode(json_encode($test_data),true);
        view()->share('test',$test_data);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.test_pdf');
        return $pdf->download('testpdfview.pdf');
    }
    //add test view file
    public function addTest(){
        $array_department=$this->get_department_code_list();
        $array_function=$this->get_functions_code_list();
        $array_level=$this->get_seniority_code_list();
        $array_expert_area=$this->get_development_code_list();
        $array_competency=$this->get_code_list();
        $array_courses=$this->get_courses();
       $total_record = DB::select( DB::raw("SELECT tfm_id,tfm_function FROM tbl_functions_master WHERE tfm_is_deleted='0' AND tfm_is_active = '1' "));
       $testFunctionList=json_decode(json_encode($total_record),true);

       return view('admin.test.addtest', compact('testFunctionList','array_department','array_function','array_level','array_expert_area','array_competency','array_courses'));

       //return view('admin.test.addtest',['testFunctionList'=>$total_record]);
    }

    public function test_learner_ajax_listing(Request $request){
        $edit_test_id=$request->session()->get('test_id');
        $edit_test_id=1;
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');

        $dropdown_learner_department_id=$request->input('dropdown_learner_department_id');
        $dropdown_learner_function_id=$request->input('dropdown_learner_function_id');
        $dropdown_learner_level_id=$request->input('dropdown_learner_level_id');
        $dropdown_learner_status_id=$request->input('dropdown_learner_status_id');

        
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_learner_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_learner_department_id."' ";
        }
        if($dropdown_learner_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_learner_function_id."' ";
        }
        if($dropdown_learner_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_learner_level_id."' ";
        }
        if($dropdown_learner_status_id!=""){
            $where .=" AND ";
            if($dropdown_learner_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id

        where u.id NOT IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 ) AND u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


            $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.id NOT IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 ) AND u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('test_learner_hidden_column_array')){
                $col_arr_value = $request->session()->get('test_learner_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="assign_learner" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EmployeeCode"){
                            $nesteddata['EmployeeCode']=$row['employee_code'];
                        }
                        if($value[0]=="UserName"){
                            $nesteddata['UserName']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="EmailId"){
                            $nesteddata['EmailId']=$row['email'];
                        }
                        if($value[0]=="Department"){
                            $nesteddata['Department']=$row['tdm_department'];
                        }
                        if($value[0]=="ContactDetail"){
                            $nesteddata['ContactDetail']=$row['mobile_no'];
                        }
                        if($value[0]=="ManagerName"){
                            $nesteddata['ManagerName']=$row['manager_name'];
                        }
                        if($value[0]=="CourseStatus"){
                            $nesteddata['CourseStatus']="Ongoing";
                        }
                        
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="EnrollCourses"){
                            $nesteddata['EnrollCourses']="2";
                        }
                        
                        
                        
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="assign_learner" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EmployeeCode']=$row['employee_code'];
                    $nesteddata['UserName']=$row['user_firstname']."".$row['user_lastname'];
                    $nesteddata['EmailId']=$row['email'];
                    $nesteddata['Department']=$row['tdm_department'];
                    $nesteddata['ContactDetail']=$row['mobile_no'];
                    $nesteddata['ManagerName']=$row['manager_name'];
                    $nesteddata['CourseStatus']="Ongoing";
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['EnrollCourses']="4";
                    
                    
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function assign_learner_to_test(Request $request){
        //$test_id=$request->session()->get('test_id');
        $test_id=1;
        $arr_req=$request->all();
        $arr_value= $arr_req['chk_check_value'];
        $date=date('Y-m-d H:i:s');
        foreach ($arr_value as  $value) {
           DB::table('tbl_test_learner_relation')->insert([
            [  
                'ttlr_test_id' => $test_id,
                'ttlr_user_id' => $value,
                'ttlr_created_at'=>$date
                ]
            ]);
        }
        $last_insert_id= DB::getPdo()->lastInsertId();
    }

    public function test_invite_learner_ajax_listing(Request $request){
        $edit_test_id=$request->session()->get('test_id');
        $edit_test_id=1;
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');

        $dropdown_learner_department_id=$request->input('dropdown_learner_department_id');
        $dropdown_learner_function_id=$request->input('dropdown_learner_function_id');
        $dropdown_learner_level_id=$request->input('dropdown_learner_level_id');
        $dropdown_learner_status_id=$request->input('dropdown_learner_status_id');

        
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_learner_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_learner_department_id."' ";
        }
        if($dropdown_learner_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_learner_function_id."' ";
        }
        if($dropdown_learner_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_learner_level_id."' ";
        }
        if($dropdown_learner_status_id!=""){
            $where .=" AND ";
            if($dropdown_learner_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u 
        
        left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id

        where u.id IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 ) AND u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


            $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u 
       
        left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.id IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 ) AND u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('test_invite_learner_hidden_column_array')){
                $col_arr_value = $request->session()->get('test_invite_learner_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="invite_learner" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EmployeeCode"){
                            $nesteddata['EmployeeCode']=$row['employee_code'];
                        }
                        if($value[0]=="UserName"){
                            $nesteddata['UserName']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="EmailId"){
                            $nesteddata['EmailId']=$row['email'];
                        }
                        if($value[0]=="Department"){
                            $nesteddata['Department']=$row['tdm_department'];
                        }
                        if($value[0]=="ContactDetail"){
                            $nesteddata['ContactDetail']=$row['mobile_no'];
                        }
                        if($value[0]=="ManagerName"){
                            $nesteddata['ManagerName']=$row['manager_name'];
                        }
                        if($value[0]=="CourseStatus"){
                            $nesteddata['CourseStatus']="Ongoing";
                        }
                        
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="EnrollCourses"){
                            $nesteddata['EnrollCourses']="2";
                        }
                        
                        
                        
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="invite_learner" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EmployeeCode']=$row['employee_code'];
                    $nesteddata['UserName']=$row['user_firstname']."".$row['user_lastname'];
                    $nesteddata['EmailId']=$row['email'];
                    $nesteddata['Department']=$row['tdm_department'];
                    $nesteddata['ContactDetail']=$row['mobile_no'];
                    $nesteddata['ManagerName']=$row['manager_name'];
                    $nesteddata['CourseStatus']="Ongoing";
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['EnrollCourses']="4";
                    
                    
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function invite_learner_to_test(Request $request){
        $test_id=$request->session()->get('test_id');
        $arr_req=$request->all();
        $arr_value= $arr_req['chk_check_value'];
        $date=date('Y-m-d H:i:s');
       
        //test details
        $test_details = DB::table('tbl_test')
            ->where('tte_id',$test_id)
            ->where('tte_is_deleted',0)
            ->get();
        $test_details=json_decode(json_encode($test_details),true);
        //send mail to user
        $email_template = DB::table('tbl_test_settings')
            ->where('tts_test_id',$test_id)
            ->where('tts_is_deleted',0)
            ->get();
        $email_template=json_decode(json_encode($email_template),true);
        $email_body=$posts[0]['tts_email_body'];
        $email_subject=$posts[0]['tts_email_subject'];
        $data=[];
        foreach ($arr_value as  $value) {
            $users = DB::table('users')
                        ->where('id',$value)
                        ->where('is_deleted',0)
                        ->get();
            $users=json_decode(json_encode($users),true);
            $email_body= str_replace("<FIRSTNAME>",$users[0]['user_firstname'],$email_body);
            $email_body.= str_replace("<MIDDLENAME>",$users[0]['user_middlename'],$email_body);
            $email_body.= str_replace("<LASTNAME>",$users[0]['user_lastname'],$email_body);
            $email_body.= str_replace("<EMAIL>",$users[0]['email'],$email_body);
            $email_body.= str_replace("<MOBILENO>",$users[0]['mobile_no'],$email_body);
            $email_body.= str_replace("<TESTNAME>",$test_details[0]['tte_test_name'],$email_body);
            $email_body.= str_replace("<TESTSTARTDATE>",$test_details[0]['tte_start_date'],$email_body);
            $email_body.= str_replace("<TESTENDDATE>",$test_details[0]['tte_start_date'],$email_body);
            $email_body.= str_replace("<TESTTIME>",$test_details[0]['tte_test_duration'],$email_body);
            
           
           Mail::send([], $data, function ($message) use ($email_body)
           { 
               $message->from(Config::get('myconstants.FROM_EMAIL'))->subject($email_subject);

               $message->to($users[0]['email']);
               $message->setBody($email_body,'text/html');
           });



          DB::table('tbl_test_learner_relation')
            ->where('ttlr_test_id', $test_id)
            ->where('ttlr_user_id',$value)
            ->update(['ttlr_has_learner_invite' => 1]);
        }
    }
    
}
