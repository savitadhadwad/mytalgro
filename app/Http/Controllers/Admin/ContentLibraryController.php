<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ContentLibraryRequest;
use DB;
use Config;

class ContentLibraryController extends Controller
{
       public function index()
    {
    	// $cont_lib = DB::table('tbl_content_library')->where(['tcl_is_deleted'=>0])->orderBy('tcl_created_at','desc')->get()->toArray();
     //  	$cont_lib = json_decode(json_encode($cont_lib),true);

      	$competencyList = DB::table('tbl_competency_master')->where(['tcm_is_deleted'=>0,'tcm_is_disabled'=>0])->orderBy('tcm_competency_title', 'asc')->get();
        $competencyList = json_decode(json_encode($competencyList),true);

        $developementAreaList = DB::table('tbl_development_area')->where(['tda_is_deleted'=>0,'tda_is_active'=>1])->orderBy('tda_development', 'asc')->get();
        $developementAreaList = json_decode(json_encode($developementAreaList),true);

        $levelList = DB::table('tbl_seniority_level')->where(['tsl_is_deleted'=>0,'tsl_is_active'=>1])->orderBy('tsl_seniority', 'asc')->get();
        $levelList = json_decode(json_encode($levelList),true);

        $courseList = DB::select( DB::raw("SELECT tc_id,tc_title FROM tbl_courses WHERE tc_is_deleted='0' AND tc_is_active = '1' "));
        $courseList = json_decode(json_encode($courseList),true);

        $categoryList = DB::select( DB::raw("SELECT tccm_id,tccm_category FROM tbl_course_category_master WHERE tccm_is_deleted='0' AND tccm_is_active ='1'"));
        $categoryList=json_decode(json_encode($categoryList),true);

    	// return view('admin.courses.contentlibrary')->with('cont_lib',$cont_lib);
    	return view('admin.courses.contentlibrary',['competencyList'=>$competencyList,'developementAreaList'=>$developementAreaList,'levelList'=>$levelList,'courseList'=>$courseList,'categoryList'=>$categoryList]);
    }

    public function openAddArticle(){
        $competencyList = DB::table('tbl_competency_master')->where(['tcm_is_deleted'=>0,'tcm_is_disabled'=>0])->orderBy('tcm_competency_title', 'asc')->get();
        $competencyList = json_decode(json_encode($competencyList),true);  
        
        $developementAreaList = DB::table('tbl_development_area')->where(['tda_is_deleted'=>0,'tda_is_active'=>1])->orderBy('tda_development', 'asc')->get();
        $developementAreaList = json_decode(json_encode($developementAreaList),true);

        $levelList = DB::table('tbl_seniority_level')->where(['tsl_is_deleted'=>0,'tsl_is_active'=>1])->orderBy('tsl_seniority', 'asc')->get();
        $levelList = json_decode(json_encode($levelList),true);

        return view('admin.contentlibrary.add_article',['competencyList'=>$competencyList,'developementAreaList'=>$developementAreaList,'levelList'=>$levelList]);
    }

    public function openAddAudVid(){
        $competencyList = DB::table('tbl_competency_master')->where(['tcm_is_deleted'=>0,'tcm_is_disabled'=>0])->orderBy('tcm_competency_title', 'asc')->get();
        $competencyList = json_decode(json_encode($competencyList),true);  
        
        $developementAreaList = DB::table('tbl_development_area')->where(['tda_is_deleted'=>0,'tda_is_active'=>1])->orderBy('tda_development', 'asc')->get();
        $developementAreaList = json_decode(json_encode($developementAreaList),true);

        $levelList = DB::table('tbl_seniority_level')->where(['tsl_is_deleted'=>0,'tsl_is_active'=>1])->orderBy('tsl_seniority', 'asc')->get();
        $levelList = json_decode(json_encode($levelList),true);        
        return view('admin.contentlibrary.add_audvid',['competencyList'=>$competencyList,'developementAreaList'=>$developementAreaList,'levelList'=>$levelList]);
    }  

    public function openAddDocument(){
        $competencyList = DB::table('tbl_competency_master')->where(['tcm_is_deleted'=>0,'tcm_is_disabled'=>0])->orderBy('tcm_competency_title', 'asc')->get();
        $competencyList = json_decode(json_encode($competencyList),true);  
        
        $developementAreaList = DB::table('tbl_development_area')->where(['tda_is_deleted'=>0,'tda_is_active'=>1])->orderBy('tda_development', 'asc')->get();
        $developementAreaList = json_decode(json_encode($developementAreaList),true);

        $levelList = DB::table('tbl_seniority_level')->where(['tsl_is_deleted'=>0,'tsl_is_active'=>1])->orderBy('tsl_seniority', 'asc')->get();
        $levelList = json_decode(json_encode($levelList),true);            
        return view('admin.contentlibrary.add_document',['competencyList'=>$competencyList,'developementAreaList'=>$developementAreaList,'levelList'=>$levelList]);
    }  

    public function openAddWebinar(){
        $competencyList = DB::table('tbl_competency_master')->where(['tcm_is_deleted'=>0,'tcm_is_disabled'=>0])->orderBy('tcm_competency_title', 'asc')->get();
        $competencyList = json_decode(json_encode($competencyList),true);  
        
        $developementAreaList = DB::table('tbl_development_area')->where(['tda_is_deleted'=>0,'tda_is_active'=>1])->orderBy('tda_development', 'asc')->get();
        $developementAreaList = json_decode(json_encode($developementAreaList),true);

        $levelList = DB::table('tbl_seniority_level')->where(['tsl_is_deleted'=>0,'tsl_is_active'=>1])->orderBy('tsl_seniority', 'asc')->get();
        $levelList = json_decode(json_encode($levelList),true); 

        $webinarTypeList = DB::table('tbl_webinar_type')->where(['twt_is_deleted'=>0,'twt_is_active'=>1])->orderBy('twt_name', 'asc')->get();
        $webinarTypeList = json_decode(json_encode($webinarTypeList),true);

        $webinarTimezoneList = DB::table('tbl_time_zone_master')->where(['ttzm_is_deleted'=>0,'ttzm_is_active'=>1])->orderBy('ttzm_name', 'asc')->get();
        $webinarTimezoneList = json_decode(json_encode($webinarTimezoneList),true);

        $instructorList = DB::table('users')
                                        ->join('role_user','role_user.user_id','=','users.id')
                                        ->join('roles','role_user.role_id','=','roles.id')
                                        ->select(array('users.id as uid','users.user_firstname as uname'))
                                        ->where(['roles.id'=>6])->orderBy('users.user_firstname', 'asc')->get();
        $instructorList = json_decode(json_encode($instructorList),true);        

        return view('admin.contentlibrary.add_webinar',['competencyList'=>$competencyList,'developementAreaList'=>$developementAreaList,'levelList'=>$levelList,'webinarTypeList'=>$webinarTypeList,'webinarTimezoneList'=>$webinarTimezoneList,'instructorList'=>$instructorList]);
    }          

    public function addArticle(Request $request){
        $body = "";
        $image_name = "";
        $embed_url = $request->embed_url;
        if($request->form_type == "documents"){
            $form_type = "documents";
            $file = $request->file('select_document');
            if($file != "" || $file != null){
                $image_name = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                if(!in_array($fileExtension, array("doc","docx","ppt","pptx","xls","xlsx","pdf","txt"))){
                    return '{"status":"error","message":"Please select proper file type"}';
                }
                $destinationPath = public_path() . "\img\contentlibrary\articles\\video\\";
                $result = $file->move($destinationPath,$file->getClientOriginalName());
            }
        }else if($request->form_type == "videos"){
            $form_type = "videos";
            $file = $request->file('select_video');
            if($file != "" || $file != null){
                $image_name = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                if(!in_array($fileExtension, array("mp3","mp4","wmv"))){
                    return '{"status":"error","message":"Please select proper file type"}';
                }
                $destinationPath = public_path() . "\img\contentlibrary\articles\\video\\";
                $result = $file->move($destinationPath,$file->getClientOriginalName());
            }
        }else if($request->form_type == "webinar"){
            $form_type = "webinar";
        }else{
            $form_type = "article";
            $body = $request->body;
            if($body == ""){
                return '{"status":"error","message":"'.Config::get('messages.contentlibrary.BODY_REQUIRED').'"}';
            }

        }
    	$file_name = "";
    	if($request->get_crop_img != ""){
    		$file_name = $request->get_crop_img;
    		
    	} 
    	$title = $request->title;
    	if($title == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.TITLE_REQUIRED').'"}';
    	}
        if(!preg_match('/^[a-zA-Z\s]+$/', $title)){
            return '{"status":"error","message":"Please enter proper title"}';
        }
    	$description = $request->description;
    	$competency = $request->competency;
    	$development_area = $request->development_area;
    	$level = $request->level;
    	if($level == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.LEVEL_REQUIRED').'"}';
    	}
    	$keywords = $request->keywords;
    	if($keywords == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.KEYWORDS_REQUIRED').'"}';
    	}
    	$author = $request->author;
    	if($author == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.AUTHOR_REQUIRED').'"}';
    	}

        $is_active = $request->is_active;
        $is_available = $request->is_available;        
    	$tcl_created_at = Config::get('myconstants.current_date'); //get current date which stored in config/myconstants.php
    	$tcl_updated_at = Config::get('myconstants.current_date');

        $webinar_type = $webinar_seats_available = $webinar_date = $webinar_timezone = $webinar_from_time = $webinar_to_time = "";
        if($request->form_type == "webinar"){
            $webinar_type = $request->webinar_type;
            $webinar_seats_available = $request->webinar_seats_available;
            $webinar_date = date(Config::get('myconstants.save_datetime_format'),strtotime($request->webinar_date));
            $webinar_timezone = $request->webinar_timezone;
            $webinar_from_time = date(Config::get('myconstants.save_time_format'),strtotime($request->webinar_from_time));
            $webinar_to_time = date(Config::get('myconstants.save_time_format'),strtotime($request->webinar_to_time));
            $webinar_instructor = $request->webinar_instructor;
        }

    	$result = DB::table('tbl_content_library')->insert(['tcl_type'=>$form_type,'tcl_cover_image'=>$file_name,'tcl_title'=>$title,'tcl_document'=>$image_name,'tcl_embed_video'=>$embed_url,'tcl_description'=>$description,'tcl_body'=>$body,'tcl_competency'=>$competency,'tcl_developement_area'=>$development_area,'tcl_level'=>$level,'tcl_keywords'=>$keywords,'tcl_author'=>$author,'tcl_is_active'=>$is_active,'tcl_is_deleted'=>'0','tcl_is_available'=>$is_available,'tcl_created_at'=>$tcl_created_at,'tcl_created_by'=>auth()->user()->id,'tcl_updated_at'=>$tcl_updated_at,'tcl_updated_by'=>auth()->user()->id,'tcl_is_favourite'=>0,'tcl_seats_available'=>$webinar_seats_available,'tcl_webinar_date'=>$webinar_date,'tcl_webinar_from_time'=>$webinar_from_time,'tcl_webinar_to_time'=> $webinar_to_time,'tcl_webinar_type'=>$webinar_type,'tcl_webinar_timezone'=>$webinar_timezone,'tcl_webinar_instructor'=>$webinar_instructor]);

    	if ($result) {
                if($request->form_type == "documents"){
                    $success_msg = Config::get('messages.contentlibrary.DOCUMENTS_ADD_SUCCESS');
                }else if($request->form_type == "videos"){
                    $success_msg = Config::get('messages.contentlibrary.VIDEO_ADD_SUCCESS');
                }else if($request->form_type == "webinar"){
                    $success_msg = Config::get('messages.contentlibrary.WEBINAR_ADD_SUCCESS');
                }else{
                    $success_msg = Config::get('messages.contentlibrary.ARTICLE_ADD_SUCCESS');
                }
    		return '{"status":"success","message":"'.$success_msg.'"}';
    		// return back()->with('success',Config::get('messages.contentlibrary.ARTICLE_ADD_SUCCESS'));
    	}else{
                if($request->form_type == "documents"){
                    $error_msg = Config::get('messages.contentlibrary.DOCUMENTS_ADD_ERROR');
                }else if($request->form_type == "videos"){
                    $success_msg = Config::get('messages.contentlibrary.VIDEO_ADD_ERROR');
                }else if($request->form_type == "webinar"){
                    $success_msg = Config::get('messages.contentlibrary.WEBINAR_ADD_ERROR');
                }else{
                    $error_msg = Config::get('messages.contentlibrary.ARTICLE_ADD_ERROR');
                }
    		return '{"status":"error","message":"'.$error_msg.'"}';
    		// return back()->with('error',Config::get('messages.contentlibrary.ARTICLE_ADD_ERROR'));
    	}

    	// return view('admin.courses.contentlibrary');
    }	



    public function editArticle(Request $request){
        if(!empty($request->session()->get('content_id'))){
    	$article_id = $request->session()->get('content_id');
    	$cont_lib = DB::table('tbl_content_library')->where(['tcl_id'=>$article_id,'tcl_is_deleted'=>0])->get();
      	$cont_lib = json_encode($cont_lib);

      	$course_article = DB::table('tbl_article_course')->join('tbl_courses','tbl_article_course.tac_course_id','=','tbl_courses.tc_id')->where(['tbl_article_course.tac_article_id'=>$article_id,'tbl_courses.tc_is_deleted'=>0])->get();
      	$course_article = json_encode($course_article);

        $get_values_of_library = json_decode($cont_lib,true);
        for($i=0;$i<count($get_values_of_library);$i++){
            $get_values_of_library[$i]['tcl_webinar_date'] = date(Config::get('myconstants.display_calender_date_format'),strtotime($get_values_of_library[$i]['tcl_webinar_date']));
            $get_values_of_library[$i]['tcl_webinar_from_time'] = date(Config::get('myconstants.display_calender_time_format'),strtotime($get_values_of_library[$i]['tcl_webinar_from_time']));
            $get_values_of_library[$i]['tcl_webinar_to_time'] = date(Config::get('myconstants.display_calender_time_format'),strtotime($get_values_of_library[$i]['tcl_webinar_to_time']));
        }

        $cont_lib = json_encode($get_values_of_library);
      	// $cont_lib = array_merge($cont_lib,$course_article);

      	// $cont_lib = json_encode($cont_lib);

      	$cont_lib = '{"cont_lib":'.$cont_lib.',"course_article":'.$course_article.'}';

    	return $cont_lib;
        }else{
            return "false";
        }
    }

    public function updateArticle(Request $request){

        // dd($result);
        $body = "";
        $image_name= "";
        if($request->form_type == "documents"){
            $form_type = "documents";
            $file = $request->file('select_document');
            if($file != "" || $file != null){
                $image_name = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                if(!in_array($fileExtension, array("doc","docx","ppt","pptx","xls","xlsx","pdf","txt"))){
                    return '{"status":"error","message":"Please select proper file type"}';
                }
                $destinationPath = public_path() . "\img\contentlibrary\articles\\video\\";
                $result = $file->move($destinationPath,$file->getClientOriginalName());
                DB::table('tbl_content_library')->where('tcl_id',$request->edit_article_id)->update(['tcl_document'=>$image_name]);
            }
        }else if($request->form_type == "videos"){
            $form_type = "videos";
            $file = $request->file('select_video');
            if($file != "" || $file != null){
                $image_name = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                if(!in_array($fileExtension, array("mp3","mp4","wmv"))){
                    return '{"status":"error","message":"Please select proper file type"}';
                }
                $destinationPath = public_path() . "\img\contentlibrary\articles\\video\\";
                $result = $file->move($destinationPath,$file->getClientOriginalName());
                DB::table('tbl_content_library')->where('tcl_id',$request->edit_article_id)->update(['tcl_document'=>$image_name]);
            }
            if($request->embed_url != ""){
                DB::table('tbl_content_library')->where('tcl_id',$request->edit_article_id)->update(['tcl_embed_video'=>$request->embed_url]);                
            }
        }else if($request->form_type == "webinar"){
            $form_type = "webinar";
        }else{
            $form_type = "article";
            $body = $request->edit_article_body;
            if($body == ""){
                return '{"status":"error","message":"'.Config::get('messages.contentlibrary.BODY_REQUIRED').'"}';
            }
        }        

    	if($request->get_crop_img != ""){
    		$file_name = $request->get_crop_img;
    		DB::table('tbl_content_library')->where('tcl_id',$request->edit_article_id)->update(['tcl_cover_image'=>$file_name]);
    	}

        if($request->get_crop_img_small != ""){
            $file_name = $request->get_crop_img_small;
            DB::table('tbl_content_library')->where('tcl_id',$request->edit_article_id)->update(['tcl_cover_image_small'=>$file_name]);
        }

    	$title = $request->edit_article_title;
    	if($title == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.TITLE_REQUIRED').'"}';
    	}
        if(!preg_match('/^[a-zA-Z\s]+$/', $title)){
            return '{"status":"error","message":"Please enter proper title"}';
        }
    	$description =$request->edit_article_description;
    	$competency = $request->edit_copentency;
    	$development_area = $request->edit_development_area;
    	$level = $request->edit_level;
    	if($level == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.LEVEL_REQUIRED').'"}';
    	}
    	$keywords = $request->edit_article_keywords;
    	if($keywords == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.KEYWORDS_REQUIRED').'"}';
    	}
    	$author = $request->edit_article_author;
    	if($author == ""){
    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.AUTHOR_REQUIRED').'"}';
    	}

        $is_active = $request->edit_article_active;
        $is_available = $request->edit_article_available;

    	$tcl_updated_at = Config::get('myconstants.current_date');

        $webinar_type = $webinar_seats_available = $webinar_date = $webinar_timezone = $webinar_from_time = $webinar_to_time = "";
        if($request->form_type == "webinar"){
            $webinar_type = $request->webinar_type;
            $webinar_seats_available = $request->webinar_seats_available;
            $webinar_date = date(Config::get('myconstants.save_datetime_format'),strtotime($request->webinar_date));
            $webinar_timezone = $request->webinar_timezone;
            $webinar_from_time = date(Config::get('myconstants.save_time_format'),strtotime($request->webinar_from_time));
            $webinar_to_time =  date(Config::get('myconstants.save_time_format'),strtotime($request->webinar_to_time));
            $webinar_instructor = $request->webinar_instructor;
            $result = DB::table('tbl_content_library')->where('tcl_id',$request->edit_article_id)->update(['tcl_seats_available'=>$webinar_seats_available,'tcl_webinar_date'=>$webinar_date,'tcl_webinar_from_time'=>$webinar_from_time,'tcl_webinar_to_time'=> $webinar_to_time,'tcl_webinar_type'=>$webinar_type,'tcl_webinar_timezone'=>$webinar_timezone,'tcl_webinar_instructor'=>$webinar_instructor]);            
        }        

        $result = DB::table('tbl_content_library')->where('tcl_id',$request->edit_article_id)->update(['tcl_type'=>$form_type,'tcl_title'=>$title,'tcl_description'=>$description,'tcl_body'=>$body,'tcl_competency'=>$competency,'tcl_developement_area'=>$development_area,'tcl_level'=>$level,'tcl_keywords'=>$keywords,'tcl_author'=>$author,'tcl_is_active'=>$is_active,'tcl_is_deleted'=>'0','tcl_is_available'=>$is_available,'tcl_updated_at'=>$tcl_updated_at,'tcl_updated_by'=>auth()->user()->id]);

            if($request->form_type == "documents"){
                return '{"status":"success","message":"'.Config::get('messages.contentlibrary.DOCUMENTS_UPDATE_SUCCESS').'"}';
            }else if($request->form_type == "videos"){
                return '{"status":"success","message":"'.Config::get('messages.contentlibrary.VIDEO_UPDATE_SUCCESS').'"}';
            }else if($request->form_type == "webinar"){
                return '{"status":"success","message":"'.Config::get('messages.contentlibrary.WEBINAR_UPDATE_SUCCESS').'"}';
            }else{
            	return '{"status":"success","message":"'.Config::get('messages.contentlibrary.ARTICLE_UPDATE_SUCCESS').'"}';
            }
        	// return back()->with('success',Config::get('messages.contentlibrary.ARTICLE_UPDATE_SUCCESS'));

    }



    public function deleteArticle(Request $request){
      try{
      	$cont_lib = DB::table('tbl_article_course')->where(['tac_article_id'=>$request->article_id])->get()->count();
      	if($cont_lib == 0){
	        DB::table('tbl_content_library')->where('tcl_id',$request->article_id)->update(['tcl_is_deleted'=>1]);
	        return '{"status":"success","url":"'.url('/admin/contentlibrary').'"}';
    	}else{
    		$err_msg = Config::get('messages.contentlibrary.ARTICLE_DELETE_FAILED');
    		return '{"status":"fail","message":"'.$err_msg.'","url":"'.url('/admin/contentlibrary').'"}';
    	}
      }catch(\Exception $e){
      		$err_msg = Config::get('messages.contentlibrary.ARTICLE_DELETE_ERROR');
            return '{"status":"fail","message":"'.$err_msg.'","url":"'.url('/admin/contentlibrary').'"}';
      }   	
    }

    public function getDevelopementArea(Request $request){
    	$DevelopementAreaList = DB::table('tbl_development_area')->where(['tda_is_deleted'=>0,'tda_is_active'=>1,'tda_competency_id'=>$request->competency_id])->orderBy('tda_development', 'asc')->get();
        $DevelopementAreaList = json_encode($DevelopementAreaList);  
        return $DevelopementAreaList;
    }

    public function getArticles($id,$tab_name){
    	if($id == "" || $id == 0){
    		$id = 4;
    	}
    	 return $cont_lib = DB::table('tbl_content_library')
							    	        ->when($tab_name, function ($query, $tab_name) {
							                    return $query->where('tcl_type', $tab_name);
							                })
                                            ->where(function ($q) {
                                                $q->where(['tcl_is_active'=>1,'tcl_is_deleted'=>0])
                                                ->orWhere(function ($q) {
                                                $q->where('tcl_is_active', '=', 0)
                                                      ->where('tcl_is_deleted','=',0)
                                                      ->where('tcl_created_by', '=', auth()->user()->id);
                                                });
                                            })
    	 									->orderBy('tcl_created_at','desc')->paginate($id);
      	// return $cont_lib = json_encode($cont_lib);
    }

    public function getFavArticles($id){
        if($id == "" || $id == 0){
            $id = 4;
        }
         return $cont_lib = DB::table('tbl_content_library')
                                            ->where(function ($q) {
                                                $q->where(['tcl_is_active'=>1,'tcl_is_deleted'=>0,'tcl_is_favourite'=>1])
                                                ->orWhere(function ($q) {
                                                $q->where('tcl_is_active', '=', 0)
                                                      ->where('tcl_is_deleted','=',0)
                                                      ->where('tcl_is_favourite','=',1)
                                                      ->where('tcl_created_by', '=', auth()->user()->id);
                                                });
                                            })
                                            ->orderBy('tcl_created_at','desc')->paginate($id);
        // return $cont_lib = json_encode($cont_lib);
    }    

    public function pagination(Request $request){
        if($request->tab_name == "favourite"){
            $arti = $this->getFavArticles($request->pagination_no);
        }else{
            $arti = $this->getArticles($request->pagination_no,$request->tab_name);
        }
    	
    	return view('admin.ajax.pagination',['arti'=>$arti]);  
    }

    public function getNextArt(Request $request){
        if($request->tab_name == "favourite"){
            $arti = $this->getFavArticles($request->pagination_drop);
        }else{
            $arti = $this->getArticles($request->pagination_drop,$request->tab_name);
        }

    	return view('admin.ajax.pagination',['arti'=>$arti])->render();  
    }

    public function updateIsActiveContent(Request $request){
    	try{
	        DB::table('tbl_content_library')->where('tcl_id',$request->article_id)->update(['tcl_is_active'=>$request->is_active]);
	        return '{"status":"success","url":"'.url('/admin/contentlibrary').'"}';  
	    }catch(\Exception $e){
	    	$err_msg = Config::get('messages.contentlibrary.CONTENT_ISACTIVE_FAILED');
            return '{"status":"fail","message":"'.$err_msg.'","url":"'.url('/admin/contentlibrary').'"}';
      	}  	
    }	

    public function updateIsFavContent(Request $request){
        try{
            DB::table('tbl_content_library')->where('tcl_id',$request->article_id)->update(['tcl_is_favourite'=>$request->is_active]);
            return '{"status":"success","url":"'.url('/admin/contentlibrary').'"}';  
        }catch(\Exception $e){
            $err_msg = Config::get('messages.contentlibrary.CONTENT_ISACTIVE_FAILED');
            return '{"status":"fail","message":"'.$err_msg.'","url":"'.url('/admin/contentlibrary').'"}';
        }   
    }    

    public function imageCropPost(Request $request)
    {
       
        $data = $request->image_path;        
        
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $image_name= time().'.png';

        $path = public_path() . "\img\contentlibrary\articles\\590X140\\". $image_name;
       
        file_put_contents($path, $data);

        //code start for save image in different folder with different size

        $ImagesDirectory = public_path() . "\img\contentlibrary\articles\\590X140\\";

        $DestImagesDirectory = public_path() . "\img\contentlibrary\articles\\280X66\\";

        $this->uploadResizeImage($ImagesDirectory,$DestImagesDirectory,'280','66',$image_name);

        $DestImagesDirectoryBig = public_path() . "\img\contentlibrary\articles\\1280X300\\";

        $this->uploadResizeImage($ImagesDirectory,$DestImagesDirectoryBig,'1280','300',$image_name);

        //code ends for save image in different folder with different size
        return $image_name;
    }   

    public function imageCropPostSmall(Request $request)
    {
       
        $data = $request->image_path;        
        
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $image_name= time().'.png';

        $path = public_path() . "\img\contentlibrary\articles\\256X256\\". $image_name;
       
        file_put_contents($path, $data);

        return $image_name;
    }


    function uploadResizeImage($ImagesDirectory,$DestImagesDirectory,$NewImageWidth,$NewImageHeight,$file){

        $ImagesDirectory    = $ImagesDirectory; //Source Image Directory End with Slash
        $DestImagesDirectory    = $DestImagesDirectory; //Destination Image Directory End with Slash
        $NewImageWidth      = $NewImageWidth; //New Width of Image
        $NewImageHeight     = $NewImageHeight; // New Height of Image
        $Quality        = 80; //Image Quality

        if($dir = opendir($ImagesDirectory)){
                    $imagePath = $ImagesDirectory.$file;
                    $destPath = $DestImagesDirectory.$file;
                    $checkValidImage = @getimagesize($imagePath);

                    if(file_exists($imagePath) && $checkValidImage) //Continue only if 2 given parameters are true
                    {
                        //Image looks valid, resize.
                        if($this->resizeImage($imagePath,$destPath,$NewImageWidth,$NewImageHeight,$Quality))
                        {
                            // return $file.' resize Success!<br />';
                            /*
                            Now Image is resized, may be save information in database?
                            */

                        }else{
                            // return $file.' resize Failed!<br />';
                        }
                    }
                closedir($dir);
            }else{
                // return "false";
            }
    }

    //Function that resizes image.
    function resizeImage($SrcImage,$DestImage, $MaxWidth,$MaxHeight,$Quality)
    {
        list($iWidth,$iHeight,$type)    = getimagesize($SrcImage);
        $ImageScale             = min($MaxWidth/$iWidth, $MaxHeight/$iHeight);
        $NewWidth               = ceil($ImageScale*$iWidth);
        $NewHeight              = ceil($ImageScale*$iHeight);
        $NewCanves              = imagecreatetruecolor($NewWidth, $NewHeight);

        switch(strtolower(image_type_to_mime_type($type)))
        {
            case 'image/jpeg':
                $NewImage = imagecreatefromjpeg($SrcImage);
                break;
            case 'image/png':
                $NewImage = imagecreatefrompng($SrcImage);
                break;
            case 'image/gif':
                $NewImage = imagecreatefromgif($SrcImage);
                break;
            default:
                return false;
        }

        // Resize Image
        if(imagecopyresampled($NewCanves, $NewImage,0, 0, 0, 0, $NewWidth, $NewHeight, $iWidth, $iHeight))
        {
            // copy file
            if(imagejpeg($NewCanves,$DestImage,$Quality))
            {
                imagedestroy($NewCanves);
                return true;
            }
        }
    }    

    public function set_session_content(Request $request){
        //set session if user click on edit or view team
        if(empty($request->content_id) || $request->content_id == ""){
            $request->content_id = "";
        }
        $request->session()->put('content_id', $request->content_id);
        $request->session()->put('content_type', $request->content_type);
    } 


}
