<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;
use Input;

class UsersController extends Controller{
    use CommonTrait;
    public function index(){
        if (! Gate::allows('user_access')) {
            return abort(401);
        }
        $array_role_data=$this->get_role_list();
        $array_department=$this->get_department_code_list();
        $array_function=$this->get_functions_code_list();
        $array_level=$this->get_seniority_code_list();
        $array_expert_area=$this->get_development_code_list();
        $array_competency=$this->get_code_list();
        $array_courses=$this->get_courses();

        return view('admin.users.index', compact('array_role_data','array_department','array_function','array_level','array_expert_area','array_competency','array_courses'));
    }

   

    public function user_manager_ajax_listing(Request $request){
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');
        $dropdown_manager_department_id=$request->input('dropdown_manager_department_id');
        $dropdown_manager_function_id=$request->input('dropdown_manager_function_id');
        $dropdown_manager_level_id=$request->input('dropdown_manager_level_id');
        $dropdown_manager_status_id=$request->input('dropdown_manager_status_id');
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";     
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_manager_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_manager_department_id."' ";
        }
        if($dropdown_manager_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_manager_function_id."' ";
        }
        if($dropdown_manager_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_manager_level_id."' ";
        }
        if($dropdown_manager_status_id!=""){
            $where .=" AND ";
            if($dropdown_manager_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on u.id=ua.user_manager
        where u.is_deleted=0 and role_id='2' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


        $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on u.id=ua.user_manager
        where u.is_deleted=0 and role_id='2' and ru.is_deleted=0".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('user_hidden_column_array')){
                $col_arr_value = $request->session()->get('user_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="manager" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EMPCode"){
                            $nesteddata['EMPCode']=$row['employee_code'];
                        }
                        if($value[0]=="Name"){
                            $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="Email"){
                            $nesteddata['Email']=$row['email'];
                        }
                        if($value[0]=="Mobile"){
                            $nesteddata['Mobile']=$row['mobile_no'];
                        }
                        if($value[0]=="Dept"){
                            $nesteddata['Dept']=$row['tdm_department'];
                        }
                        if($value[0]=="Function"){
                            $nesteddata['Function']=$row['tfm_function'];
                        }
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="Manager"){
                            $nesteddata['Manager']=$row['manager_name'];
                        }
                        if($value[0]=="Courses"){
                             $manager_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                            from users u inner join tbl_manager_course_releation tmcr on u.id=tmcr.tmcr_manager_id
                            inner join tbl_courses tc on tc.tc_id=tmcr.tmcr_course_id 
                            where tmcr_manager_id=".$row['id']." group by tc_title") );
                            $manager_course=json_decode(json_encode($manager_course),true);
                            if($manager_course){
                               $nesteddata['Courses']=$manager_course[0]['course_title'];
                            }else{
                                $nesteddata['Courses']="No Course";
                            }
                        }
                        if($value[0]=="CourseEnrolled"){
                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                        }
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                                <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }

                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="manager" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EMPCode']=$row['employee_code'];
                    $nesteddata['Name']=$row['user_firstname']."  ".$row['user_lastname'];
                    $nesteddata['Email']=$row['email'];
                    $nesteddata['Mobile']=$row['mobile_no'];
                    $nesteddata['Dept']=$row['tdm_department'];
                    $nesteddata['Function']=$row['tfm_function'];
                    
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['Manager']=$row['manager_name'];
                    $nesteddata['Courses']=$row['user_manager'];

                    $manager_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                        from users u inner join tbl_manager_course_releation tmcr on u.id=tmcr.tmcr_manager_id
                        inner join tbl_courses tc on tc.tc_id=tmcr.tmcr_course_id 
                        where tmcr_manager_id=".$row['id']." group by tc_title") );
                        $manager_course=json_decode(json_encode($manager_course),true);
                        if($manager_course){
                           $nesteddata['Courses']=$manager_course[0]['course_title'];
                        }else{
                            $nesteddata['Courses']="No Course";
                        }

                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                    $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                        
                        <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;
        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );

        // print_r($json_data);
        // exit;
        echo json_encode($json_data);
    }

    public function user_all_ajax_listing(Request $request){
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');

        $dropdown_all_department_id=$request->input('dropdown_all_department_id');
        $dropdown_all_function_id=$request->input('dropdown_all_function_id');
        $dropdown_all_level_id=$request->input('dropdown_all_level_id');
        $dropdown_all_status_id=$request->input('dropdown_all_status_id');
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_all_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_all_department_id."' ";
        }
        if($dropdown_all_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_all_function_id."' ";
        }
        if($dropdown_all_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_all_level_id."' ";
        }
        if($dropdown_all_status_id!=""){
            $where .=" AND ";
            if($dropdown_all_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.email,array_to_string(array_agg(title), ',') as title,u.id,u.employee_code,u.mobile_no,array_to_string(array_agg(r.id), ',') as role_id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name,u.user_lastname
            from users u left join role_user ru on u.id=ru.user_id 
            left join roles r on ru.role_id=r.id 
            left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on ua.user_manager=u.id
            where u.is_deleted=0 and ru.is_deleted=0 ".$where." GROUP BY ru.user_id,u.user_firstname,u.email ,u.id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department, manager_name,u.user_lastname ".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


            $posts = DB::select( DB::raw("SELECT u.user_firstname,u.email,array_to_string(array_agg(title), ',') as title,u.id,u.employee_code,u.mobile_no,array_to_string(array_agg(r.id), ',') as role_id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name,u.user_lastname
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                    left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                    left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                    left join users ua on ua.user_manager=u.id
                where u.is_deleted=0 and ru.is_deleted=0 ".$where." GROUP BY ru.user_id,u.user_firstname,u.email ,u.id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department, manager_name,u.user_lastname ".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $posts=json_decode(json_encode($posts),true);

       
            
        $data=array();
        if($posts){

            if($request->session()->has('all_user_hidden_column_array')){
                $col_arr_value = $request->session()->get('all_user_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="all_user" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EMPCode"){
                            $nesteddata['EMPCode']=$row['employee_code'];
                        }
                        if($value[0]=="Name"){
                            $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="Email"){
                            $nesteddata['Email']=$row['email'];
                        }
                        if($value[0]=="Mobile"){
                            $nesteddata['Mobile']=$row['mobile_no'];
                        }
                        if($value[0]=="Dept"){
                            $nesteddata['Dept']=$row['tdm_department'];
                        }
                        if($value[0]=="Function"){
                            $nesteddata['Function']=$row['tfm_function'];
                        }
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="Manager"){
                            $nesteddata['Manager']=$row['manager_name'];
                        }
                        // if($value[0]=="Courses"){
                        //     $nesteddata['Courses']=$row['tdm_department'];
                        // }
                        if($value[0]=="CourseEnrolled"){
                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                        }
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                                <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                                <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" type="checkbox" tab_name="all_user"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EMPCode']=$row['employee_code'];
                    $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];
                    $nesteddata['Email']=$row['email'];
                    $nesteddata['Mobile']=$row['mobile_no'];
                    $nesteddata['Dept']=$row['tdm_department'];
                    $nesteddata['Function']=$row['tfm_function'];
                    
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['Manager']=$row['manager_name'];
                    //$nesteddata['Courses']=$row['user_manager'];
                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                    $course=json_decode(json_encode($course),true);
                    if($course){
                       $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                    }else{
                        $nesteddata['CourseEnrolled']="No Course Enrolled";
                    }

                    $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                        <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    
                    $data[]=$nesteddata;
                }
            }
        }

        //print_r($data);

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        
        echo json_encode($json_data);
    }

    public function expert_ajax_listing(Request $request){
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');

        $dropdown_expert_department_id=$request->input('dropdown_expert_department_id');
        $dropdown_expert_function_id=$request->input('dropdown_expert_function_id');
        $dropdown_expert_level_id=$request->input('dropdown_expert_level_id');
        $dropdown_expert_status_id=$request->input('dropdown_expert_status_id');
        $dropdown_expert_area_id=$request->input('dropdown_expert_area_id');
        $dropdown_expert_course_id=$request->input('dropdown_expert_course_id');
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .="  OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_expert_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_expert_department_id."' ";
        }
        if($dropdown_expert_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_expert_function_id."' ";
        }
        if($dropdown_expert_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_expert_level_id."' ";
        }
        if($dropdown_expert_status_id!=""){
            $where .=" AND ";
            if($dropdown_expert_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        if($dropdown_expert_area_id!=""){
            $get_expert_area_user= DB::select( DB::raw("SELECT tudr_user_id FROM tbl_user_development_relation WHERE tudr_development_id = '".$dropdown_expert_area_id."' "));
            $get_user=json_decode(json_encode($get_expert_area_user),true);
            if(count($get_user)>0){
                $ids = array_column($get_user, 'tudr_user_id');
                $expertise_area_string=implode(",",$ids);
                $where .=" AND ";
                $where .=" u.id IN ('".$expertise_area_string."') ";
            }
        }
        if($dropdown_expert_course_id!=""){
            $get_expert_course_user= DB::select( DB::raw("SELECT tlcr_learner_id FROM tbl_learner_course_releation WHERE tlcr_course_id = '".$dropdown_expert_course_id."' "));
            $get_course_user=json_decode(json_encode($get_expert_course_user),true);
            if(count($get_course_user)>0){
                $couser_learner_ids = array_column($get_course_user, 'tlcr_learner_id');
                $expertise_course_string=implode(",",$couser_learner_ids);
                $where .=" AND ";
                $where .=" u.id IN ('".$expertise_course_string."') ";
            }

        }
        
        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,
                CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.is_deleted=0 AND role_id='3' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


            $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,
                CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.is_deleted=0 AND role_id='3' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('expert_hidden_column_array')){
                $col_arr_value = $request->session()->get('expert_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="expert" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EMPCode"){
                            $nesteddata['EMPCode']=$row['employee_code'];
                        }
                        if($value[0]=="Name"){
                            $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="Email"){
                            $nesteddata['Email']=$row['email'];
                        }
                        if($value[0]=="Mobile"){
                            $nesteddata['Mobile']=$row['mobile_no'];
                        }
                        if($value[0]=="Dept"){
                            $nesteddata['Dept']=$row['tdm_department'];
                        }
                        if($value[0]=="Function"){
                            $nesteddata['Function']=$row['tfm_function'];
                        }
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="Manager"){
                            $nesteddata['Manager']=$row['manager_name'];
                        }
                        if($value[0]=="Courses"){
                            $expert_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_expert_course_relation tecr on u.id=tecr.tecr_expert_id
                                inner join tbl_courses tc on tc.tc_id=tecr.tecr_course_id 
                                where tecr_expert_id=".$row['id']." group by tc_title") );
                                $expert_course=json_decode(json_encode($expert_course),true);
                                if($expert_course){
                                   $nesteddata['Courses']=$expert_course[0]['course_title'];
                                }else{
                                    $nesteddata['Courses']="No Course";
                                }
                        }
                        if($value[0]=="CourseEnrolled"){
                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                        }
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                                <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                                <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="expert" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EMPCode']=$row['employee_code'];
                    $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];
                    $nesteddata['Email']=$row['email'];
                    $nesteddata['Mobile']=$row['mobile_no'];
                    $nesteddata['Dept']=$row['tdm_department'];
                    $nesteddata['Function']=$row['tfm_function'];
                    
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['Manager']=$row['manager_name'];
                    $expert_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                        from users u inner join tbl_expert_course_relation tecr on u.id=tecr.tecr_expert_id
                        inner join tbl_courses tc on tc.tc_id=tecr.tecr_course_id 
                        where tecr_expert_id=".$row['id']." group by tc_title") );
                        $expert_course=json_decode(json_encode($expert_course),true);
                        if($expert_course){
                           $nesteddata['Courses']=$expert_course[0]['course_title'];
                        }else{
                            $nesteddata['Courses']="No Course";
                        }
                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                    $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                        <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function hr_ajax_listing(Request $request){
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');
        
        $dropdown_hr_department_id=$request->input('dropdown_hr_department_id');
        $dropdown_hr_function_id=$request->input('dropdown_hr_function_id');
        $dropdown_hr_level_id=$request->input('dropdown_hr_level_id');
        $dropdown_hr_status_id=$request->input('dropdown_hr_status_id');
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .="  OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_hr_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_hr_department_id."' ";
        }
        if($dropdown_hr_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_hr_function_id."' ";
        }
        if($dropdown_hr_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_hr_level_id."' ";
        }
        if($dropdown_hr_status_id!=""){
            $where .=" AND ";
            if($dropdown_hr_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.is_deleted=0 AND role_id='4' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


            $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.is_deleted=0 AND role_id='4' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('hr_hidden_column_array')){
                $col_arr_value = $request->session()->get('hr_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="hr" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EMPCode"){
                            $nesteddata['EMPCode']=$row['employee_code'];
                        }
                        if($value[0]=="Name"){
                            $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="Email"){
                            $nesteddata['Email']=$row['email'];
                        }
                        if($value[0]=="Mobile"){
                            $nesteddata['Mobile']=$row['mobile_no'];
                        }
                        if($value[0]=="Dept"){
                            $nesteddata['Dept']=$row['tdm_department'];
                        }
                        if($value[0]=="Function"){
                            $nesteddata['Function']=$row['tfm_function'];
                        }
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="Manager"){
                            $nesteddata['Manager']=$row['manager_name'];
                        }
                        if($value[0]=="Courses"){
                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                            from users u inner join tbl_hr_course_relation thcr on u.id=thcr.thcr_hr_id
                            inner join tbl_courses tc on tc.tc_id=thcr.thcr_course_id 
                            where thcr_hr_id=".$row['id']." group by tc_title") );
                            $course=json_decode(json_encode($course),true);
                            if($course){
                                $nesteddata['Courses']=$course[0]['course_title'];
                            }else{
                                $nesteddata['Courses']="No Course Enrolled";
                            }
                        }
                        if($value[0]=="CourseEnrolled"){
                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                        }
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                                <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                                <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="hr" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EMPCode']=$row['employee_code'];
                    $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];
                    $nesteddata['Email']=$row['email'];
                    $nesteddata['Mobile']=$row['mobile_no'];
                    $nesteddata['Dept']=$row['tdm_department'];
                    $nesteddata['Function']=$row['tfm_function'];
                    
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['Manager']=$row['manager_name'];
                    //$nesteddata['Courses']=$row['user_manager'];

                    $hr_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                            from users u inner join tbl_hr_course_relation thcr on u.id=thcr.thcr_hr_id
                            inner join tbl_courses tc on tc.tc_id=thcr.thcr_course_id 
                            where thcr_hr_id=".$row['id']." group by tc_title") );
                            $hr_course=json_decode(json_encode($hr_course),true);
                            if($hr_course){
                                $nesteddata['Courses']=$hr_course[0]['course_title'];
                            }else{
                                $nesteddata['Courses']="No Course Enrolled";
                            }
                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                    $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                        <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function learner_ajax_listing(Request $request){
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');


        $dropdown_learner_department_id=$request->input('dropdown_learner_department_id');
        $dropdown_learner_function_id=$request->input('dropdown_learner_function_id');
        $dropdown_learner_level_id=$request->input('dropdown_learner_level_id');
        $dropdown_learner_status_id=$request->input('dropdown_learner_status_id');
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_learner_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_learner_department_id."' ";
        }
        if($dropdown_learner_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_learner_function_id."' ";
        }
        if($dropdown_learner_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_learner_level_id."' ";
        }
        if($dropdown_learner_status_id!=""){
            $where .=" AND ";
            if($dropdown_learner_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id

        where u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


            $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('learner_hidden_column_array')){
                $col_arr_value = $request->session()->get('learner_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="learner" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EMPCode"){
                            $nesteddata['EMPCode']=$row['employee_code'];
                        }
                        if($value[0]=="Name"){
                            $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="Email"){
                            $nesteddata['Email']=$row['email'];
                        }
                        if($value[0]=="Mobile"){
                            $nesteddata['Mobile']=$row['mobile_no'];
                        }
                        if($value[0]=="Dept"){
                            $nesteddata['Dept']=$row['tdm_department'];
                        }
                        if($value[0]=="Function"){
                            $nesteddata['Function']=$row['tfm_function'];
                        }
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="Manager"){
                            $nesteddata['Manager']=$row['manager_name'];
                        }
                        // if($value[0]=="Courses"){
                        //     $nesteddata['Courses']=$row['tdm_department'];
                        // }
                        if($value[0]=="CourseEnrolled"){
                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                        }
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                                <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                                <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="learner" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EMPCode']=$row['employee_code'];
                    $nesteddata['Name']=$row['user_firstname']."".$row['user_lastname'];
                    $nesteddata['Email']=$row['email'];
                    $nesteddata['Mobile']=$row['mobile_no'];
                    $nesteddata['Dept']=$row['tdm_department'];
                    $nesteddata['Function']=$row['tfm_function'];
                    
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['Manager']=$row['manager_name'];
                    //$nesteddata['Courses']=$row['user_manager'];
                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                    $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                        <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function instructor_ajax_listing(Request $request){
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');

        $dropdown_instructor_department_id=$request->input('dropdown_instructor_department_id');
        $dropdown_instructor_function_id=$request->input('dropdown_instructor_function_id');
        $dropdown_instructor_level_id=$request->input('dropdown_instructor_level_id');
        $dropdown_instructor_status_id=$request->input('dropdown_instructor_status_id');

        $dropdown_instructor_area_id=$request->input('dropdown_instructor_area_id');
        $dropdown_instructor_course_id=$request->input('dropdown_instructor_course_id');

        $dropdown_instructor_competency_id=$request->input('dropdown_instructor_competency_id');

        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";     
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_instructor_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_instructor_department_id."' ";
        }
        if($dropdown_instructor_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_instructor_function_id."' ";
        }
        if($dropdown_instructor_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_instructor_level_id."' ";
        }

        if($dropdown_instructor_area_id!=""){
            $get_instructor_area_user= DB::select( DB::raw("SELECT tudr_user_id FROM tbl_user_development_relation WHERE tudr_development_id = '".$dropdown_instructor_area_id."' "));
            $get_user=json_decode(json_encode($get_instructor_area_user),true);
            if(count($get_user)>0){
                $ids = array_column($get_user, 'tudr_user_id');
                $instructor_area_string=implode(",",$ids);
                $where .=" AND ";
                $where .=" u.id IN ('".$instructor_area_string."') ";
            }
        }
        if($dropdown_instructor_course_id!=""){
            $get_instructor_course_user= DB::select( DB::raw("SELECT tlcr_learner_id FROM tbl_learner_course_releation WHERE tlcr_course_id = '".$dropdown_instructor_course_id."' "));
            $get_course_user=json_decode(json_encode($get_instructor_course_user),true);
            if(count($get_course_user)>0){
                $couser_learner_ids = array_column($get_course_user, 'tlcr_learner_id');
                $instructor_course_string=implode(",",$couser_learner_ids);
                $where .=" AND ";
                $where .=" u.id IN ('".$instructor_course_string."') ";
            }

        }
        if($dropdown_instructor_status_id!=""){
            $where .=" AND ";
            if($dropdown_instructor_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }



        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY u.id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

     
        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,
            CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.is_deleted=0 AND role_id='6' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


        $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,
            CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.is_deleted=0 AND role_id='6' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('instructor_hidden_column_array')){
                $col_arr_value = $request->session()->get('instructor_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="instructor" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EMPCode"){
                            $nesteddata['EMPCode']=$row['employee_code'];
                        }
                        if($value[0]=="Name"){
                            $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="Email"){
                            $nesteddata['Email']=$row['email'];
                        }
                        if($value[0]=="Mobile"){
                            $nesteddata['Mobile']=$row['mobile_no'];
                        }
                        if($value[0]=="Dept"){
                            $nesteddata['Dept']=$row['tdm_department'];
                        }
                        if($value[0]=="Function"){
                            $nesteddata['Function']=$row['tfm_function'];
                        }
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="Manager"){
                            $nesteddata['Manager']=$row['manager_name'];
                        }
                        if($value[0]=="Courses"){
                            //$nesteddata['Courses']=$row['tdm_department'];
                            $hr_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_instructor_course_relation ticr on u.id=ticr.ticr_instructor_id
                                inner join tbl_courses tc on tc.tc_id=ticr.ticr_course_id 
                                where ticr_instructor_id=".$row['id']." AND ticr_is_deleted=0 group by tc_title") );
                                $hr_course=json_decode(json_encode($hr_course),true);
                                if($hr_course){
                                    $nesteddata['Courses']=$hr_course[0]['course_title'];
                                }else{
                                    $nesteddata['Courses']="No Course Enrolled";
                                }
                        }
                        if($value[0]=="CourseEnrolled"){
                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course Enrolled";
                                }
                        }
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                                <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                                <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="instructor" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EMPCode']=$row['employee_code'];
                    $nesteddata['Name']=$row['user_firstname']." ".$row['user_lastname'];
                    $nesteddata['Email']=$row['email'];
                    $nesteddata['Mobile']=$row['mobile_no'];
                    $nesteddata['Dept']=$row['tdm_department'];
                    $nesteddata['Function']=$row['tfm_function'];
                    
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['Manager']=$row['manager_name'];
                    $hr_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_instructor_course_relation ticr on u.id=ticr.ticr_instructor_id
                                inner join tbl_courses tc on tc.tc_id=ticr.ticr_course_id 
                                where ticr_instructor_id=".$row['id']." AND ticr_is_deleted=0 group by tc_title") );
                                $hr_course=json_decode(json_encode($hr_course),true);
                                if($hr_course){
                                    $nesteddata['Courses']=$hr_course[0]['course_title'];
                                }else{
                                    $nesteddata['Courses']="No Course";
                                }
                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                                inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                                where tlcr_learner_id=".$row['id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseEnrolled']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseEnrolled']="No Course";
                                }
                    $nesteddata['Action']='<a href="'.route('admin.users.show',$row['id']).'" class="i-size"><i class="icon icon-eye"></i></a>
                        <a href="'.route('admin.users.restore',$row['id']).'" class="i-size"><i class="icon icon-edit"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }
    //when user clik on user listing delete button
    public function delete_user(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('users')
                ->where('id', $delete_id)
                ->update(['is_deleted' => 1]);
        }

    }
    //when user click on common checkbox action enable,disable & delete
    public function user_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('users')
                    ->whereIn('id', $arr_value)
                    ->update(['is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('users')
                    ->whereIn('id', $arr_value)
                    ->update(['is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('users')
                    ->whereIn('id', $arr_value)
                    ->update(['is_deleted' => 1]);
           
        }
    }

    public function user_download_PDF($user_role){
        $where="";
        if($user_role=="all"){
            $where="";
        }elseif ($user_role=="manager") {
            $where="AND role_id='2'";
        }elseif ($user_role=="expert") {
            $where="AND role_id='3'";
        }elseif($user_role=='hr'){
            $where="AND role_id='4'";
        }elseif ($user_role=="learner") {
            $where="AND role_id='5'";
        }elseif ($user_role=="instructor") {
            $where="AND role_id='6'";
        }

        if($user_role=="all"){

            $users=DB::select( DB::raw("SELECT u.user_firstname,u.email,array_to_string(array_agg(title), ',') as title,u.id,u.employee_code,u.mobile_no,array_to_string(array_agg(r.id), ',') as role_id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name,u.user_lastname
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                    left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                    left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                    left join users ua on ua.user_manager=u.id
                where u.is_deleted=0 and ru.is_deleted=0  GROUP BY ru.user_id,u.user_firstname,u.email ,u.id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department, manager_name,u.user_lastname"));
                view()->share('users',$users);

                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                // pass view file
                $pdf = PDF::loadView('pdf_file.all_user_pdf');
            //$users=json_decode(json_encode($users),true);

        }elseif($user_role=="manager"){
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='2' and ru.is_deleted=0".$where));
                view()->share('users',$users);

                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                // pass view file
                $pdf = PDF::loadView('pdf_file.manager_user_pdf');
        }elseif($user_role=="expert"){
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='3' and ru.is_deleted=0".$where));
                view()->share('users',$users);

                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                // pass view file
                $pdf = PDF::loadView('pdf_file.expert_user_pdf');
        }elseif($user_role=="hr"){
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='4' and ru.is_deleted=0".$where));
                view()->share('users',$users);

                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                // pass view file
                $pdf = PDF::loadView('pdf_file.hr_pdf');
        }elseif($user_role=="instructor"){
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='6' and ru.is_deleted=0".$where));
                view()->share('users',$users);

                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                // pass view file
                $pdf = PDF::loadView('pdf_file.instructor_pdf');
        }else{
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='5' and ru.is_deleted=0".$where));
                view()->share('users',$users);

                PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
                // pass view file
                $pdf = PDF::loadView('pdf_file.user_learner_pdf');
        }
        
        // download pdf
        return $pdf->download('userpdfview.pdf');
        return view('userpdfview');
    }

    public function user_download_excel($user_role){
        $where="";
        if($user_role=="all"){
            $where="";
        }elseif ($user_role=="manager") {
            $where="AND role_id='2'";
        }elseif ($user_role=="expert") {
            $where="AND role_id='3'";
        }elseif($user_role=='hr'){
            $where="AND role_id='4'";
        }elseif ($user_role=="learner") {
            $where="AND role_id='5'";
        }elseif ($user_role=="instructor") {
            $where="AND role_id='6'";
        }
        if($user_role=="all"){

            $users=DB::select( DB::raw("SELECT u.user_firstname,u.email,array_to_string(array_agg(title), ',') as title,u.id,u.employee_code,u.mobile_no,array_to_string(array_agg(r.id), ',') as role_id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name,u.user_lastname
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                    left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                    left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                    left join users ua on ua.user_manager=u.id
                where u.is_deleted=0 and ru.is_deleted=0  GROUP BY ru.user_id,u.user_firstname,u.email ,u.id,tsl.tsl_seniority,tfm.tfm_function,tdm.tdm_department, manager_name,u.user_lastname"));
            $users=json_decode(json_encode($users),true);
            $users_array[] = array('EMPCode','Name','Email','Mobile','Department','Function','Level','Manager','CourcesEntrolled');
            foreach($users as $value){

                $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                        from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                        inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                        where tlcr_learner_id=".$value['id']." group by tc_title") );
                        $course=json_decode(json_encode($course),true);
                        if($course){
                           $course_enrolled=$course[0]['course_title'];
                        }else{
                            $course_enrolled="No Course Enrolled";
                        }

                $users_array[] = array(
               'EMPCode'  => $value['employee_code'],
               'Name'   => $value['user_firstname']."".$value['user_lastname'],
               'Email'      =>$value['email'],
               'Mobile'=>$value['mobile_no'],
               'Department'=>$value['tdm_department'],
               'Function'=>$value['tfm_function'],
               'Level'=>$value['tsl_seniority'],
               'Manager'=>$value['manager_name'],
               'CourcesEntrolled'=>$course_enrolled
              );
            }

        }elseif($user_role=="manager"){

            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
            from users u left join role_user ru on u.id=ru.user_id 
            left join roles r on ru.role_id=r.id 
            left join tbl_department_master tdm on u.user_department=tdm.tdm_id
            left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
            left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
            left join users ua on u.id=ua.user_manager
            where u.is_deleted=0 and role_id='2' and ru.is_deleted=0 order by u.id desc"));
            $users=json_decode(json_encode($users),true);
            $users_array[] = array('EMPCode','Name','Email','Mobile','Department','Function','Level','Manager','Courses','CourcesEntrolled');
            foreach($users as $value){
                $manager_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_manager_course_releation tmcr on u.id=tmcr.tmcr_manager_id
                    inner join tbl_courses tc on tc.tc_id=tmcr.tmcr_course_id 
                    where tmcr_manager_id=".$value['id']." group by tc_title") );
                    $manager_course=json_decode(json_encode($manager_course),true);
                    if($manager_course){
                        $Courses=$manager_course[0]['course_title'];
                    }else{
                        $Courses="No Course";
                    }

                $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                    inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                    where tlcr_learner_id=".$value['id']." group by tc_title") );
                    $course=json_decode(json_encode($course),true);
                    if($course){
                       $CourseEnrolled=$course[0]['course_title'];
                    }else{
                        $CourseEnrolled="No Course Enrolled";
                    }

                $users_array[] = array(
               'EMPCode'  => $value['employee_code'],
               'Name'   => $value['user_firstname']."".$value['user_lastname'],
               'Email'      =>$value['email'],
               'Mobile'=>$value['mobile_no'],
               'Department'=>$value['tdm_department'],
               'Function'=>$value['tfm_function'],
               'Level'=>$value['tsl_seniority'],
               'Manager'=>$value['manager_name'],
               'Courses'=>$Courses,
               'CourcesEntrolled'=>$CourseEnrolled
              );
            }

        }elseif($user_role=="expert"){
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
            from users u left join role_user ru on u.id=ru.user_id 
            left join roles r on ru.role_id=r.id 
            left join tbl_department_master tdm on u.user_department=tdm.tdm_id
            left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
            left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
            left join users ua on u.id=ua.user_manager
            where u.is_deleted=0 and role_id='3' and ru.is_deleted=0 order by u.id desc"));
                $users=json_decode(json_encode($users),true);
                $users_array[] = array('EMPCode','Name','Email','Mobile','Department','Function','Level','Manager','Courses','CourcesEntrolled');
                foreach($users as $value){

                    $expert_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                        from users u inner join tbl_expert_course_relation tecr on u.id=tecr.tecr_expert_id
                        inner join tbl_courses tc on tc.tc_id=tecr.tecr_course_id 
                        where tecr_expert_id=".$value['id']." group by tc_title") );
                        $expert_course=json_decode(json_encode($expert_course),true);
                        if($expert_course){
                           $Courses=$expert_course[0]['course_title'];
                        }else{
                            $Courses="No Course";
                        }
                    
                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                        from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                        inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                        where tlcr_learner_id=".$value['id']." group by tc_title") );
                        $course=json_decode(json_encode($course),true);
                        if($course){
                           $CourseEnrolled=$course[0]['course_title'];
                        }else{
                            $CourseEnrolled="No Course Enrolled";
                        }

                    $users_array[] = array(
                   'EMPCode'  => $value['employee_code'],
                   'Name'   => $value['user_firstname']."".$value['user_lastname'],
                   'Email'      =>$value['email'],
                   'Mobile'=>$value['mobile_no'],
                   'Department'=>$value['tdm_department'],
                   'Function'=>$value['tfm_function'],
                   'Level'=>$value['tsl_seniority'],
                   'Manager'=>$value['manager_name'],
                   'Courses'=>$Courses,
                   'CourcesEntrolled'=>$CourseEnrolled
                  );
                }

        }elseif($user_role=="hr"){
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='4' and ru.is_deleted=0 order by u.id desc"));
            $users=json_decode(json_encode($users),true);
            $users_array[] = array('EMPCode','Name','Email','Mobile','Department','Function','Level','Manager','Courses','CourcesEntrolled');
            foreach($users as $value){

                
                $hr_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_hr_course_relation thcr on u.id=thcr.thcr_hr_id
                    inner join tbl_courses tc on tc.tc_id=thcr.thcr_course_id 
                    where thcr_hr_id=".$value['id']." group by tc_title") );
                    $hr_course=json_decode(json_encode($hr_course),true);
                    if($hr_course){
                        $Courses=$hr_course[0]['course_title'];
                    }else{
                        $Courses="No Course Enrolled";
                    }

                
                $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                    inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                    where tlcr_learner_id=".$value['id']." group by tc_title") );
                    $course=json_decode(json_encode($course),true);
                    if($course){
                       $CourseEnrolled=$course[0]['course_title'];
                    }else{
                        $CourseEnrolled="No Course Enrolled";
                    }

                $users_array[] = array(
               'EMPCode'  => $value['employee_code'],
               'Name'   => $value['user_firstname']."".$value['user_lastname'],
               'Email'      =>$value['email'],
               'Mobile'=>$value['mobile_no'],
               'Department'=>$value['tdm_department'],
               'Function'=>$value['tfm_function'],
               'Level'=>$value['tsl_seniority'],
               'Manager'=>$value['manager_name'],
               'Courses'=>$Courses,
               'CourcesEntrolled'=>$CourseEnrolled
              );
            }

        }elseif($user_role=="instructor"){
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='6' and ru.is_deleted=0 order by u.id desc"));
            $users=json_decode(json_encode($users),true);
            $users_array[] = array('EMPCode','Name','Email','Mobile','Department','Function','Level','Manager','Courses','CourcesEntrolled');
            foreach($users as $value){

                
                $hr_course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_instructor_course_relation ticr on u.id=ticr.ticr_instructor_id
                    inner join tbl_courses tc on tc.tc_id=ticr.ticr_course_id 
                    where ticr_instructor_id=".$value['id']." AND ticr_is_deleted=0 group by tc_title") );
                    $hr_course=json_decode(json_encode($hr_course),true);
                    if($hr_course){
                        $Courses=$hr_course[0]['course_title'];
                    }else{
                        $Courses="No Course Enrolled";
                    }

                
                $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                    inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                    where tlcr_learner_id=".$value['id']." and tlcr_is_deleted=0 group by tc_title") );
                    $course=json_decode(json_encode($course),true);
                    if($course){
                       $CourseEnrolled=$course[0]['course_title'];
                    }else{
                        $CourseEnrolled="No Course Enrolled";
                    }

                $users_array[] = array(
               'EMPCode'  => $value['employee_code'],
               'Name'   => $value['user_firstname']."".$value['user_lastname'],
               'Email'      =>$value['email'],
               'Mobile'=>$value['mobile_no'],
               'Department'=>$value['tdm_department'],
               'Function'=>$value['tfm_function'],
               'Level'=>$value['tsl_seniority'],
               'Manager'=>$value['manager_name'],
               'Courses'=>$Courses,
               'CourcesEntrolled'=>$CourseEnrolled
              );
            }

        }else{
            $users=DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
                from users u left join role_user ru on u.id=ru.user_id 
                left join roles r on ru.role_id=r.id 
                left join tbl_department_master tdm on u.user_department=tdm.tdm_id
                left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
                left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
                left join users ua on u.id=ua.user_manager
                where u.is_deleted=0 and role_id='5' and ru.is_deleted=0 order by u.id desc"));
            $users=json_decode(json_encode($users),true);
            $users_array[] = array('EMPCode','Name','Email','Mobile','Department','Function','Level','Manager','CourcesEntrolled');
            foreach($users as $value){
                $users_array[] = array(
               'EMPCode'  => $value['employee_code'],
               'Name'   => $value['name'],
               'Email'      =>$value['email'],
               'Mobile'=>$value['mobile_no'],
               'Department'=>$value['tdm_department'],
               'Function'=>$value['tfm_function'],
               'Level'=>$value['tsl_seniority'],
               'CourcesEntrolled'=>$value['mobile_no'],
              );
            }
        }

        return Excel::create('users', function($excel) use ($users_array)
        {
            $excel->sheet('mySheet', function($sheet) use ($users_array)
            {
               $sheet->fromArray($users_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }

    public function getUsersStatesList(Request $request){
        //echo "gg".$request->countryid;
        $stateList = DB::table('states')->where(['country_id'=>$request->countryid])->orderBy('state_name', 'asc')->get();
        $stateList = json_encode($stateList);

        return $stateList;
    }

    public function add_user_form(){
        $manager_list=$this->get_manager_list();
        $array_department=$this->get_department_code_list();
        $array_function=$this->get_functions_code_list();
        $array_level=$this->get_seniority_code_list();
        $team_list=$this->get_team_list();
        $expert_area_list=$this->get_development_code_list();
        $countrylist=$this->getCountryList();
        $user_type_master=$this->get_user_type_master();
        $array_data=array();
        $stateList = DB::table('states')->orderBy('state_name', 'asc')->get();
        $stateList = json_decode(json_encode($stateList),true);
        $default_role_list=$this->get_role_list();
        //$user_type_master=$this->get_user_type_master();
        return view('admin.users.create', compact('team_list','array_department','array_function','array_level','manager_list','expert_area_list','countrylist','stateList','array_data','user_type_master','default_role_list'));
    }

    public function add_user_ajax(Request $request){
        $errors=array();
        if($this->validate_user($request)){
            $date=date('Y-m-d H:i:s');
            DB::table('users')->insert([
            [  
                'employee_code' => $request->input('employee_code'),
                'user_firstname' => $request->input('user_firstname'),
                'user_middlename'=>$request->input('user_middlename'),
                'user_lastname'=>$request->input('user_lastname'),
                'email'=>$request->input('email'),
                'gender'=>$request->input('gender'),
                'mobile_no'=>$request->input('mobile_no'),
                'user_address_line1'=>$request->input('user_address_line1'),
                'user_address_line2'=>$request->input('user_address_line2'),
                'user_function'=>$request->input('user_function'),
                'user_department'=>$request->input('user_department'),
                'user_level'=>$request->input('user_level'),
                'user_manager'=>$request->input('user_manager'),
                'user_skip_manager'=>$request->input('user_skip_manager'),
                'country'=>$request->input('user_country'),
                'state'=>$request->input('state'),
                'city'=>$request->input('city'),
                'pincode'=>$request->input('pincode'),
                'website_url'=>$request->input('website_url'),
                'user_blog_url'=>$request->input('user_blog_url'),
                'user_twitter_url'=>$request->input('user_twitter_url'),
                'user_linkedin_url'=>$request->input('user_linkedin_url'),
                'user_bio'=>$request->input('user_bio'),
                'created_at'=>$date
                ]
            ]);
            
            $last_insert_id= DB::getPdo()->lastInsertId();
            //upload image code
            $file = $request->file('profile_image');
            if($file!=""){
                $file_name = $file->getClientOriginalName();
                $file->move('public/img/profileimg', $file->getClientOriginalName()); 
                $file_name = "profileimg/".$file_name;
                DB::table('users')->where('id',$last_insert_id)->update(['profile_image'=>$file_name]); 
            }
            
            if(array_key_exists("user_team",$request->all())){
                $user_team=$request->input('user_team');
                if(count($user_team)>0){
                    foreach ($user_team as $team ) {
                        DB::table('tbl_learner_team')->insert([
                        [  
                        'tlt_learner_id' => $last_insert_id,
                        'tlt_team_id' => $team,
                        'tlt_created_at'=>$date
                        ]
                        ]);
                    } 
                }
            }
            
            if(array_key_exists("development_area",$request->all())){
                $development_area=$request->input('development_area');
                if(count($development_area)>0){
                    foreach ($development_area as $area ) {
                        DB::table('tbl_user_development_relation')->insert([
                        [  
                        'tudr_user_id' => $last_insert_id,
                        'tudr_development_id' => $area,
                        'tudr_created_at'=>$date
                        ]
                        ]);
                    }
                }
            }
            
            if(array_key_exists("user_type",$request->all())){
                $user_type=$request->input('user_type');
                if(count($user_type)>0){
                    foreach ($user_type as $type ) {
                        DB::table('tbl_user_type_relation')->insert([
                        [  
                        'tutr_user_id' => $last_insert_id,
                        'tutr_user_type_id' => $type,
                        ]
                        ]);
                    }
                }
            }

            if(array_key_exists("permission_type",$request->all())){
                $permission_type=$request->input('permission_type');
                if(count($permission_type)>0){
                    foreach ($permission_type as $permission ) {
                        DB::table('role_user')->insert([
                        [  
                        'user_id' => $last_insert_id,
                        'role_id' => $permission,
                        ]
                        ]);
                    }
                }
            }

            //end of code
            echo json_encode(array('status' => 'success','last_insert_id'=>$last_insert_id, 'errors' => $errors));
        }
    }
    public function edit_user_ajax(Request $request){
        $errors=array();
        $date=date('Y-m-d H:i:s');
        $edit_id=$request->input('hidden_user_id');

        if($this->validate_user($request)){
            DB::table('users')
                ->where('id', $edit_id)  // find your user by their id
                ->update(array(
                    'employee_code' => $request->input('employee_code'),
                    'user_firstname' => $request->input('user_firstname'),
                    'user_middlename'=>$request->input('user_middlename'),
                    'user_lastname'=>$request->input('user_lastname'),
                    'email'=>$request->input('email'),
                    'gender'=>$request->input('gender'),
                    'mobile_no'=>$request->input('mobile_no'),
                    'user_address_line1'=>$request->input('user_address_line1'),
                    'user_address_line2'=>$request->input('user_address_line2'),
                    'user_function'=>$request->input('user_function'),
                    'user_department'=>$request->input('user_department'),
                    'user_level'=>$request->input('user_level'),
                    'user_manager'=>$request->input('user_manager'),
                    'user_skip_manager'=>$request->input('user_skip_manager'),
                    'country'=>$request->input('user_country'),
                    'state'=>$request->input('state'),
                    'city'=>$request->input('city'),
                    'pincode'=>$request->input('pincode'),
                    'website_url'=>$request->input('website_url'),
                    'user_blog_url'=>$request->input('user_blog_url'),
                    'user_twitter_url'=>$request->input('user_twitter_url'),
                    'user_linkedin_url'=>$request->input('user_linkedin_url'),
                    'user_bio'=>$request->input('user_bio'),
                    'updated_at'=>$date
                ));
            $file = $request->file('profile_image');
            if($file!=""){
                $file_name = $file->getClientOriginalName();
                $file->move('public/img/profileimg', $file->getClientOriginalName()); 
                $file_name = "profileimg/".$file_name;
                DB::table('users')->where('id',$edit_id)->update(['profile_image'=>$file_name]); 
            }

            if($request->input('image_remove')==1){
                DB::table('users')->where('id',$edit_id)->update(['profile_image'=>""]); 
            }
            //end image code
            //update user team
            if(array_key_exists("user_team",$request->all())){
                //delete previous record
                DB::table('tbl_learner_team')
                ->where('tlt_learner_id', $edit_id)  
                ->update(array('tlt_is_active' => 1));
                $user_team=$request->input('user_team');
                if(count($user_team)>0){
                    foreach ($user_team as $team ) {
                        DB::table('tbl_learner_team')->insert([
                        [  
                        'tlt_learner_id' => $edit_id,
                        'tlt_team_id' => $team,
                        'tlt_updated_at'=>$date
                        ]
                        ]);
                    } 
                }
            }
            //end user team

            //add expertise area
            if(array_key_exists("development_area",$request->all())){
                DB::table('tbl_user_development_relation')
                    ->where('tudr_user_id', $edit_id)  
                    ->update(array('tudr_is_deleted' => 1));
                $development_area=$request->input('development_area');
                if(count($development_area)>0){
                    foreach ($development_area as $area ) {
                        DB::table('tbl_user_development_relation')->insert([
                        [  
                        'tudr_user_id' =>$edit_id,
                        'tudr_development_id' => $area,
                        'tudr_updated_at'=>$date
                        ]
                        ]);
                    }
                }
            }
            //end expertise area


            //add relation to user role
            if(array_key_exists("user_type",$request->all())){
                DB::table('tbl_user_type_relation')
                    ->where('tutr_user_id', $edit_id)  
                    ->update(array('tutr_is_deleted' => 1));
                $user_type=$request->input('user_type');
                if(count($user_type)>0){
                    foreach ($user_type as $type ) {
                        DB::table('tbl_user_type_relation')->insert([
                        [  
                        'tutr_user_id' => $edit_id,
                        'tutr_user_type_id' => $type,
                        ]
                        ]);
                    }
                }
            }



            
            if(array_key_exists("permission_type",$request->all())){
                DB::table('role_user')
                    ->where('user_id', $edit_id)  
                    ->update(array('is_deleted' => 1));
                $permission_type=$request->input('permission_type');
                if(count($permission_type)>0){
                    foreach ($permission_type as $permission ) {
                        DB::table('role_user')->insert([
                        [  
                        'user_id' => $edit_id,
                        'role_id' => $permission,
                        ]
                        ]);
                    }
                }
            }
            //end of code
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }//validate
    }

    
    public function validate_user($request){
        $errors=array();
        if(!array_key_exists("user_type",$request->all())){
            $errors['user_type'] =Config::get('messages.add_user.USER_TYPE_ERROR');
        }
        if(empty($request->input('employee_code'))){
            $errors['employee_code'] =Config::get('messages.add_user.EMPLOYEE_CODE_ERROR');
        }
        if(empty($request->input('user_firstname'))){
            $errors['user_firstname'] =Config::get('messages.add_user.USER_FIRSTNAME_ERROR');
        }
        if(empty($request->input('user_lastname'))){
            $errors['user_lastname'] =Config::get('messages.add_user.USER_LASTNAME_ERROR');
        }
        if(empty($request->input('user_middlename'))){
            $errors['user_middlename'] =Config::get('messages.add_user.USER_MIDDLENAME_ERROR');
        }
        if(empty($request->input('gender'))){
            $errors['gender'] =Config::get('messages.add_user.GENDER_ERROR');
        }
        if(empty($request->input('email'))){
            $errors['email'] =Config::get('messages.add_user.EMAIL_ERROR');
        }
        if(!empty($request->input('email'))){
            $email=$request->input('email');
                $email_users=\DB::table('users')
                    ->select('email')
                    ->WHERE('email', '=', $email)
                    ->WHERE('id', '!=', (int)$request->input('hidden_user_id'))
                    ->WHERE('is_deleted','0')
                    ->get();
                $email_users=json_decode(json_encode($email_users),true);

                if(count($email_users)>0){
                    $errors['email'] =Config::get('messages.add_user.EMAIL_EXIST_ERROR');
                }
           
        }
        if(empty($request->input('mobile_no'))){
            $errors['mobile_no'] =Config::get('messages.add_user.MOBILE_NO_ERROR');
        }
        if(!array_key_exists("permission_type",$request->all())){
            $errors['permission_type'] =Config::get('messages.add_user.PERMISSION_TYPE_ERROR');
        }
        if(count($errors)>0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
            return false;
        }else{
            return true;
        }
    }

    public function psyometric_test_ajax_listing(Request $request){

        $columns=array(0=>'tpt_test_name',1=>'tuptr_status');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $where="";
        $order_by="";
        
        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY tpt_id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;
        
        $total_record = DB::select( DB::raw("SELECT tpt_test_name,tuptr_status FROM tbl_psychometric_test tpt inner join tbl_user_psychometric_test_relation tuptr on tpt.tpt_id=tuptr.tuptr_psychometric_test_id   WHERE tpt_is_active='0' AND tuptr_id=".$request->input('hidden_user_id')." ".$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        $array_data = DB::select( DB::raw("SELECT tpt_test_name,tuptr_status FROM tbl_psychometric_test tpt inner join tbl_user_psychometric_test_relation tuptr on tpt.tpt_id=tuptr.tuptr_psychometric_test_id   WHERE tpt_is_active='0' AND tuptr_id=".$request->input('hidden_user_id')." ".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){
            foreach ($array_data as $row) {
                $nesteddata['Test']=ucwords($row['tpt_test_name']);
                $nesteddata['Status']=ucwords($row['tuptr_status']);
                $data[]=$nesteddata;
            }
        }
        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );

        echo json_encode($json_data);
    }

    public function course_ajax_listing(Request $request){
        // echo "<pre>";
        // print_r($request->all());
        $columns=array(0=>'tc_id',1=>'tc_title');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $where="";
        $order_by="";
        
        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY tc_id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;
        
        $total_record = DB::select( DB::raw("SELECT tc_title,tc_development_area,tc_competency,tc_category FROM tbl_courses tc inner join tbl_learner_course_releation tlcr on tc.tc_id=tlcr.tlcr_course_id   WHERE tc_is_active='0' AND tlcr_learner_id=".$request->input('hidden_user_id')." ".$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        $array_data = DB::select( DB::raw("SELECT tc_title,tc_development_area,tc_competency,tc_category FROM tbl_courses tc inner join tbl_learner_course_releation tlcr on tc.tc_id=tlcr.tlcr_course_id   WHERE tc_is_active='0' AND tlcr_learner_id=".$request->input('hidden_user_id')."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){
            foreach ($array_data as $row) {
                $nesteddata['CourseName']=ucwords($row['tc_title']);
                $nesteddata['CourseType']=ucwords($row['tc_course_type']);
                $nesteddata['CourseCategories']=ucwords($row['tc_category']);
                $nesteddata['CourseCompetency']=ucwords($row['tc_competency']);
                $nesteddata['CourseDevelopmentArea']=ucwords($row['tc_development_area']);
                
                $data[]=$nesteddata;
            }
        }
        //}

        
        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function get_team_learner_list($edit_user_id){
        $team_user_list=array();
        $team_user_list = DB::select( DB::raw("SELECT tlt_id FROM users u inner join tbl_learner_team tlt on u.id=tlt.tlt_learner_id   WHERE tlt_is_active='0' AND tlt_learner_id=".$edit_user_id."  "));
        $team_user_list=json_decode(json_encode($team_user_list),true);
        return $team_user_list;
    }

    public function get_user_type_list($edit_user_id){
        $role_user_list=array();
        $role_user_list = DB::table('users')
            ->select('tutr_user_type_id')
            ->join('tbl_user_type_relation', 'tbl_user_type_relation.tutr_user_id', '=', 'users.id')
            ->where('tutr_user_id', $edit_user_id)
            ->where('tbl_user_type_relation.tutr_is_deleted',0)
            ->get();
        $role_user_list=json_decode(json_encode($role_user_list),true);
        return $role_user_list;
    }

    public function get_expertise_user_list($edit_user_id){
        $expertise_user_list=array();
        $expertise_user_list = DB::table('users')
            ->select('tudr_development_id')
            ->join('tbl_user_development_relation', 'tbl_user_development_relation.tudr_user_id', '=', 'users.id')
            ->where('tudr_user_id', $edit_user_id)
            ->WHERE('tudr_is_deleted',0)
            ->get();
        $expertise_user_list=json_decode(json_encode($expertise_user_list),true);
        return $expertise_user_list;
    }

    public function get_user_type_permission_list($edit_user_id){
        $permission_user_list=array();
        $permission_user_list = DB::table('users')
            ->select('role_id')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $edit_user_id)
            ->where('role_user.is_deleted',0)
            ->get();


        $permission_user_list=json_decode(json_encode($permission_user_list),true);
        return $permission_user_list;
    }

    public function show_user_edit_form(Request $request){
        $edit_user_id=$request->user_id;
        //default data
        $manager_list=$this->get_manager_list();
        $array_department=$this->get_department_code_list();
        $array_function=$this->get_functions_code_list();
        $array_level=$this->get_seniority_code_list();
        $team_list=$this->get_team_list();
        $expert_area_list=$this->get_development_code_list();
        $countrylist=$this->getCountryList();
        $stateList = DB::table('states')->orderBy('state_name', 'asc')->get();
        $stateList = json_decode(json_encode($stateList),true);
        $team_user_list=$this->get_team_learner_list($edit_user_id);
        $expertise_user_list=$this->get_expertise_user_list($edit_user_id);
        $role_user_list =$this->get_user_type_list($edit_user_id);
        $user_type_master=$this->get_user_type_master();
        $permission_user_list=$this->get_user_type_permission_list($edit_user_id);
        $default_role_list=$this->get_role_list();
        
        //edit data
        $array_data = DB::select( DB::raw("SELECT * FROM 
            users u LEFT JOIN tbl_functions_master tfm on u.user_function=tfm.tfm_id
            left join tbl_department_master tdm on u.user_department=tdm.tdm_id
            LEFT JOIN tbl_seniority_level tsl on u.user_level=tsl.tsl_id
             WHERE id=".$edit_user_id."") );
        $array_data=json_decode(json_encode($array_data),true);

        return view('admin.users.create', compact('team_list','array_department','array_function','array_level','manager_list','expert_area_list','countrylist','stateList','array_data','team_user_list','expertise_user_list','role_user_list','user_type_master','permission_user_list','default_role_list'));
    }

    // public function get_user_type_permission(Request $request){
    //     $last_insert_id=$request->input('last_insert_id');

    //     $get_user_type = DB::select( DB::raw("SELECT * FROM tbl_user_type_relation WHERE tutmr_user_id=".(int)$last_insert_id.""));
    //     $get_user_type=json_decode(json_encode($get_user_type),true);


    // }

    

    

}
