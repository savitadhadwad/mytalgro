<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;
use Excel;
use PDF;

class TeamController extends Controller
{
    public function index()
    {
    	return view('admin.team.teamList');
    }

    public function showAddTeam()
    {
        return view('admin.team.addteam');
    }    

    public function addTeam(Request $request){
    	try {
    		$team_title = $request->team_title;
	    	if($team_title == ""){
	    		return '{"status":"error","message":"'.Config::get('messages.contentlibrary.TITLE_REQUIRED').'"}';
	    	}
	        if(!preg_match('/^[a-zA-Z\s]+$/', $team_title)){
	            return '{"status":"error","message":"Please enter proper title"}';
	        }
	        $team_description = $request->team_description;

	    	$tt_created_at = Config::get('myconstants.current_date'); //get current date which stored in config/myconstants.php
	    	$tt_updated_at = Config::get('myconstants.current_date');

            if(empty($request->session()->get('team_id'))){
                $result = DB::table('tbl_team')->insert(['tt_title'=>$team_title,'tt_description'=>$team_description,'tt_is_active'=>'1','tt_is_deleted'=>'0','tt_created_at'=>$tt_created_at,'tt_created_by'=>auth()->user()->id,'tt_updated_at'=>$tt_updated_at,'tt_updated_by'=>auth()->user()->id]);
                $result= DB::getPdo()->lastInsertId();

                session(['team_id' => $result]);
                return '{"status":"success","message":"Team created successfully!!"}';
            }else{
                $users = DB::table('tbl_team')
                ->where('tt_id', $request->session()->get('team_id'))
                ->update(['tt_title' => $team_title,'tt_description'=>$team_description]);
                return '{"status":"success","message":"Team updated successfully!!"}';
            }

    	} catch (Exception $e) {
    		return '{"status":"error","message":"Failed to add team"}';
    	}

    }

    //listing of learners
    public function learners_ajax_listing(Request $request){
        
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'name',3=>'tdm_department',4=>'user_manager',5=>'user_level',6=>'title',7=>'EnrollCourses');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_name=$request->input('dropdown_name');
        $search=$request->input('Keyword_filter');
        $department_filter = $request->input('department_filter');
        $manager_filter = $request->input('manager_filter');
        $level_filter = $request->input('level_filter');
        $enrolled_courses_filter = $request->input('enrolled_courses_filter');
        $tabname = $request->input('tabname');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(users.employee_code) LIKE '".$search."%' ";
            $where .=" OR LOWER(users.name) LIKE '".$search."%' ";
            $where .=" OR LOWER(tdm.tdm_department) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.name) LIKE '".$search."%' ";      
            $where .=" OR LOWER(tsl.tsl_seniority) LIKE '".$search."%' ";  
            $where .=" OR LOWER(r.title) LIKE '".$search."%' )";                                         
        }
        if($dropdown_name!=""){
            $where .=" AND ";
            $where .=" id ='".$dropdown_name."' ";

        }
        if(!empty($department_filter)){
            $where .=" AND ";
            $where .=" users.user_department ='".$department_filter."' ";            
        }
        if(!empty($manager_filter)){
            $where .=" AND ";
            $where .=" users.id ='".$manager_filter."' ";            
        }
        if(!empty($level_filter)){
            $where .=" AND ";
            $where .=" users.user_level ='".$level_filter."' ";            
        }
        // if(!empty($department_filter)){
        //     $where .=" AND ";
        //     $where .=" users.user_department ='".$department_filter."' ";            
        // }  
        if(!empty($request->session()->get('team_id'))){
            $notin = "AND users.id NOT IN (SELECT DISTINCT tlt_learner_id FROM tbl_learner_team WHERE tlt_team_id = ".$request->session()->get('team_id').")" ;
        }else{
            $notin = "";
        }                              

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

            $total_record = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
                left join tbl_department_master tdm on users.user_department = tdm.tdm_id
                left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
                left join users u on u.id = users.user_manager
                left join role_user ru on users.id = ru.user_id
                left join roles r on ru.role_id = r.id
                WHERE users.is_deleted='0' AND r.title = 'Learners' ".$notin." ".$where.$order_by));        
            $total_record=json_decode(json_encode($total_record),true);
            $totalFiltered=count($total_record);
            

            $array_data = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
                left join tbl_department_master tdm on users.user_department = tdm.tdm_id
                left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
                left join users u on u.id = users.user_manager
                left join role_user ru on users.id = ru.user_id
                left join roles r on ru.role_id = r.id
                WHERE users.is_deleted='0' AND r.title = 'Learners' ".$notin." ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );        
            $array_data=json_decode(json_encode($array_data),true);
        

        
        $data=array();
        if($array_data){

            if($request->session()->has('learners_hidden_column_array')){
                $col_arr_value = $request->session()->get('learners_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {

                    // $course_record = DB::select( DB::raw("SELECT 1 FROM users
                    //     inner join tbl_learner_course_releation tlcr on users.id = tlcr.tlcr_learner_id
                    //     WHERE users.is_deleted='0' "));    
                    $course_record = DB::select( DB::raw("SELECT 1 FROM tbl_learner_course_releation WHERE tlcr_learner_id =".$row['id']." "));                             
                    $course_record=json_decode(json_encode($course_record),true);
                    $courseCount=count($course_record);

                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="EmpCode"){
                            $nesteddata['EmpCode']=$row['employee_code'];   
                        }
                        if($value[0]=="EmpName"){
                            $nesteddata['EmpName']=ucwords($row['name']);
                        }
                        if($value[0]=="Department"){
                            $nesteddata['Department']=$row['tdm_department'];   
                        }
                        if($value[0]=="ManagerName"){
                            $nesteddata['ManagerName']=$row['user_manager'];
                        } 
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['user_level'];   
                        }
                        if($value[0]=="Role"){
                            $nesteddata['Role']=$row['title'];
                        }
                        if($value[0]=="EnrollCourses"){
                            $nesteddata['EnrollCourses']=$courseCount;   
                        }                                              
                        
                        // if($value[0]=="Action"){
                        //     $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        //         <a href="javascript:void(0)" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        // }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $course_record = DB::select( DB::raw("SELECT 1 FROM tbl_learner_course_releation WHERE tlcr_learner_id =".$row['id']." "));         
                    $course_record=json_decode(json_encode($course_record),true);
                    $courseCount=count($course_record);

                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['EmpCode']=$row['employee_code'];
                    $nesteddata['EmpName']=ucwords($row['name']);
                    $nesteddata['Department']=$row['tdm_department'];
                    $nesteddata['ManagerName']=$row['user_manager'];
                    $nesteddata['Level']=$row['user_level'];
                    $nesteddata['Role']=$row['title'];
                    $nesteddata['EnrollCourses']=$courseCount;
                    // $nesteddata['Email']=ucwords($row['email']);
                    // $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                    //     <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                    //     ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    } 

    //listing of learners team
    public function learners_ajax_listing_team(Request $request){
        
        $columns=array(0=>'chk_box',1=>'employee_code',2=>'name',3=>'tdm_department',4=>'user_manager',5=>'user_level',6=>'title',7=>'EnrollCourses');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_name=$request->input('dropdown_name');
        $search=$request->input('Keyword_filter');
        $department_filter = $request->input('department_filter');
        $manager_filter = $request->input('manager_filter');
        $level_filter = $request->input('level_filter');
        $enrolled_courses_filter = $request->input('enrolled_courses_filter');
        $tabname = $request->input('tabname');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(users.employee_code) LIKE '".$search."%' ";
            $where .=" OR LOWER(users.name) LIKE '".$search."%' ";
            $where .=" OR LOWER(tdm.tdm_department) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.name) LIKE '".$search."%' ";      
            $where .=" OR LOWER(tsl.tsl_seniority) LIKE '".$search."%' ";  
            $where .=" OR LOWER(r.title) LIKE '".$search."%' )";                                         
        }
        if($dropdown_name!=""){
            $where .=" AND ";
            $where .=" id ='".$dropdown_name."' ";

        }
        if(!empty($department_filter)){
            $where .=" AND ";
            $where .=" users.user_department ='".$department_filter."' ";            
        }
        if(!empty($manager_filter)){
            $where .=" AND ";
            $where .=" users.id ='".$manager_filter."' ";            
        }
        if(!empty($level_filter)){
            $where .=" AND ";
            $where .=" users.user_level ='".$level_filter."' ";            
        }
        // if(!empty($department_filter)){
        //     $where .=" AND ";
        //     $where .=" users.user_department ='".$department_filter."' ";            
        // }   
        if(!empty($request->session()->get('team_id'))){
            $team_id = "AND tlt.tlt_team_id =" .$request->session()->get('team_id');
        }else{
            $team_id = "AND tlt.tlt_team_id = 0";
        }                     

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

            $total_record = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
                left join tbl_department_master tdm on users.user_department = tdm.tdm_id
                left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
                left join users u on u.id = users.user_manager
                left join role_user ru on users.id = ru.user_id
                left join roles r on ru.role_id = r.id
                inner join tbl_learner_team tlt on tlt.tlt_learner_id= users.id
                WHERE users.is_deleted='0' AND r.title = 'Learners' ".$team_id." ".$where.$order_by));        
            $total_record=json_decode(json_encode($total_record),true);
            $totalFiltered=count($total_record);
            

            $array_data = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
                left join tbl_department_master tdm on users.user_department = tdm.tdm_id
                left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
                left join users u on u.id = users.user_manager
                left join role_user ru on users.id = ru.user_id
                left join roles r on ru.role_id = r.id
                inner join tbl_learner_team tlt on tlt.tlt_learner_id= users.id
                WHERE users.is_deleted='0' AND r.title = 'Learners' ".$team_id." ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );        
            $array_data=json_decode(json_encode($array_data),true);
        

        
        $data=array();
        if($array_data){

            if($request->session()->has('learners_hidden_column_array')){
                $col_arr_value = $request->session()->get('learners_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {

                    // $course_record = DB::select( DB::raw("SELECT 1 FROM users
                    //     inner join tbl_learner_course_releation tlcr on users.id = tlcr.tlcr_learner_id
                    //     WHERE users.is_deleted='0' "));    
                    $course_record = DB::select( DB::raw("SELECT 1 FROM tbl_learner_course_releation WHERE tlcr_learner_id =".$row['id']." "));                             
                    $course_record=json_decode(json_encode($course_record),true);
                    $courseCount=count($course_record);

                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="EmpCode"){
                            $nesteddata['EmpCode']=$row['employee_code'];   
                        }
                        if($value[0]=="EmpName"){
                            $nesteddata['EmpName']=ucwords($row['name']);
                        }
                        if($value[0]=="Department"){
                            $nesteddata['Department']=$row['tdm_department'];   
                        }
                        if($value[0]=="ManagerName"){
                            $nesteddata['ManagerName']=$row['user_manager'];
                        } 
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['user_level'];   
                        }
                        if($value[0]=="Role"){
                            $nesteddata['Role']=$row['title'];
                        }
                        if($value[0]=="EnrollCourses"){
                            $nesteddata['EnrollCourses']=$courseCount;   
                        }                                              
                        
                        // if($value[0]=="Action"){
                        //     $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        //         <a href="javascript:void(0)" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        // }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $course_record = DB::select( DB::raw("SELECT 1 FROM tbl_learner_course_releation WHERE tlcr_learner_id =".$row['id']." "));         
                    $course_record=json_decode(json_encode($course_record),true);
                    $courseCount=count($course_record);

                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['EmpCode']=$row['employee_code'];
                    $nesteddata['EmpName']=ucwords($row['name']);
                    $nesteddata['Department']=$row['tdm_department'];
                    $nesteddata['ManagerName']=$row['user_manager'];
                    $nesteddata['Level']=$row['user_level'];
                    $nesteddata['Role']=$row['title'];
                    $nesteddata['EnrollCourses']=$courseCount;
                    // $nesteddata['Email']=ucwords($row['email']);
                    // $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                    //     <a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                    //     ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //team listing
    public function learners_ajax_team_listing(Request $request){
        
        $columns=array(0=>'chk_box',1=>'team',2=>'learners',3=>'course_assigned',4=>'learning_path',5=>'action');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_name=$request->input('dropdown_name');
        $search=$request->input('Keyword_filter');
        $no_of_learners = $request->input('no_of_learners');
        $team_filter_courses_list = $request->input('team_filter_courses_list');
        $team_filter_learining_path_list = $request->input('team_filter_learining_path_list');
        $tabname = $request->input('tabname');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tbl_team.tt_title) LIKE '".$search."%' )";
            // $where .=" OR LOWER(tbl_courses.tc_title) LIKE '".$search."%' ";
            // $where .=" OR LOWER(tdm.tdm_department) LIKE '".$search."%' ";
            // $where .=" OR LOWER(tbl_learning_path.tlp_name) LIKE '".$search."%' ) ";                                      
        }
        $count_filter = "";
        if(!empty($no_of_learners)){
         $no_of_learners = explode('-',$no_of_learners);
            $count_filter .=" HAVING ";
            $count_filter .=" count(tbl_learner_team.tlt_learner_id) BETWEEN '".$no_of_learners[0]."' AND '".$no_of_learners[1]."' ";            
        }
        $ttcr_team_id_list = "";
        if(!empty($team_filter_courses_list)){
            $get_course = DB::select( DB::raw("SELECT ttcr_team_id FROM tbl_team_course_releation WHERE ttcr_course_id = '".$team_filter_courses_list."' "));
            $get_course=json_decode(json_encode($get_course),true);

            for($i=0;$i<count($get_course);$i++){
                $ttcr_team_id_list .= $get_course[$i]['ttcr_team_id'].",";
            }
        
        }

        if(!empty($team_filter_learining_path_list)){
            $get_path = DB::select( DB::raw("SELECT tltr_team_id FROM tbl_learningpath_team_relation WHERE tltr_learning_path_id = '".$team_filter_learining_path_list."' "));
            $get_path=json_decode(json_encode($get_path),true);

            for($i=0;$i<count($get_path);$i++){
                $ttcr_team_id_list .= $get_path[$i]['tltr_team_id'].",";
            }         
        }    

        if(!empty($ttcr_team_id_list) || $ttcr_team_id_list != ""){

            $ttcr_team_id_list = chop($ttcr_team_id_list,",");
            $where .=" AND ";
            $where .=" tbl_team.tt_id IN (".$ttcr_team_id_list.") ";                
        }                  

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY tbl_team.tt_id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

            $total_record = DB::select( DB::raw("SELECT tbl_team.tt_id,tbl_team.tt_title as team,(SELECT count(tbl_learner_team.tlt_learner_id) FROM tbl_learner_team WHERE tbl_learner_team.tlt_team_id = tbl_team.tt_id GROUP BY tbl_learner_team.tlt_team_id ".$count_filter." ) as learners,(SELECT array_to_string(array_agg(tbl_courses.tc_title),',') as course_title FROM tbl_team_course_releation INNER JOIN tbl_courses ON tbl_team_course_releation.ttcr_course_id = tbl_courses.tc_id WHERE tbl_team_course_releation.ttcr_team_id = tbl_team.tt_id GROUP BY tbl_team_course_releation.ttcr_team_id) as course_assigned,(SELECT array_to_string(array_agg(tbl_learning_path.tlp_name),',') as learning_path FROM tbl_learningpath_team_relation INNER JOIN tbl_learning_path ON tbl_learningpath_team_relation.tltr_learning_path_id = tbl_learning_path.tlp_id WHERE tbl_learningpath_team_relation.tltr_team_id = tbl_team.tt_id GROUP BY tbl_learningpath_team_relation.tltr_team_id) as learning_path FROM public.tbl_team WHERE tbl_team.tt_is_deleted = 0 ".$where));    

            $total_record=json_decode(json_encode($total_record),true);
            $totalFiltered=count($total_record);
            

            $array_data = DB::select( DB::raw("SELECT tbl_team.tt_id,tbl_team.tt_title as team,(SELECT count(tbl_learner_team.tlt_learner_id) FROM tbl_learner_team WHERE tbl_learner_team.tlt_team_id = tbl_team.tt_id GROUP BY tbl_learner_team.tlt_team_id) as learners,(SELECT array_to_string(array_agg(tbl_courses.tc_title),',') as course_title FROM tbl_team_course_releation INNER JOIN tbl_courses ON tbl_team_course_releation.ttcr_course_id = tbl_courses.tc_id WHERE tbl_team_course_releation.ttcr_team_id = tbl_team.tt_id GROUP BY tbl_team_course_releation.ttcr_team_id) as course_assigned,(SELECT array_to_string(array_agg(tbl_learning_path.tlp_name),',') as learning_path FROM tbl_learningpath_team_relation INNER JOIN tbl_learning_path ON tbl_learningpath_team_relation.tltr_learning_path_id = tbl_learning_path.tlp_id WHERE tbl_learningpath_team_relation.tltr_team_id = tbl_team.tt_id GROUP BY tbl_learningpath_team_relation.tltr_team_id) as learning_path FROM public.tbl_team  WHERE  tbl_team.tt_is_deleted = 0 ".$where.$order_by."  LIMIT ".$limit." OFFSET ".$start."") );        
            //.$where.$order_by." LIMIT ".$limit." OFFSET ".$start.""
            $array_data=json_decode(json_encode($array_data),true);
        

        
        $data=array();
        if($array_data){

            if($request->session()->has('team_listing_hidden_column_array')){
                $col_arr_value = $request->session()->get('team_listing_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {

                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tt_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="Team"){
                            $nesteddata['Team']=ucwords($row['team']);   
                        }
                        if($value[0]=="Learners"){
                            $nesteddata['Learners']=$row['learners'];
                        }
                        if($value[0]=="CourseAssigned"){
                            $nesteddata['CourseAssigned']=$row['course_assigned'];   
                        }
                        if($value[0]=="LearningPathAssigned"){
                            $nesteddata['LearningPathAssigned']=$row['learning_path'];
                        }                                           
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tt_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>
                                <a href="javascript:void(0)" view_id="'.$row['tt_id'].'" class="i-size view_button"><i class="icon icon-eye"></i></a>
                                <a href="javascript:void(0)" delete_id="'.$row['tt_id'].'" class="i-size delete_team"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {

                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tt_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['Team']=ucwords($row['team']);
                    $nesteddata['Learners']=$row['learners'];
                    $nesteddata['CourseAssigned']=$row['course_assigned'];
                    $nesteddata['LearningPathAssigned']=$row['learning_path'];
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tt_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>
                        <a href="javascript:void(0)" view_id="'.$row['tt_id'].'" class="i-size view_button"><i class="icon icon-eye"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['tt_id'].'" class="i-size delete_team"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }     

    public function learners_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
        if($action=="enable"){
            $users = DB::table('users')
                    ->whereIn('id', $arr_value)
                    ->update(['is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('users')
                    ->whereIn('id', $arr_value)
                    ->update(['is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('users')
                    ->whereIn('id', $arr_value)
                    ->update(['is_deleted' => 1]);
           
        }
        elseif ($action=="assign_learner") {
            //assign learner to the team // insert connection of learner-team
            foreach ($arr_value as $key => $value) {
                DB::table('tbl_learner_team')->insert(['tlt_team_id'=>$request->session()->get('team_id'),'tlt_learner_id'=>$value]);
            }
            $request->session()->put('open_tab', 'true');
           
        }
        elseif ($action=="remove_learner") {
            //delete learner to the team // delete connection of learner-team
            foreach ($arr_value as $key => $value) {
                DB::table('tbl_learner_team')->whereIn('tlt_learner_id', $arr_value)->where('tlt_team_id',$request->session()->get('team_id'))->delete();
            }
           
        }
        elseif ($action=="enable_team") {
            $users = DB::table('tbl_team')
                    ->whereIn('tt_id', $arr_value)
                    ->update(['tt_is_active' => 1]);
        }
        elseif ($action=="disable_team") {
            $users = DB::table('tbl_team')
                    ->whereIn('tt_id', $arr_value)
                    ->update(['tt_is_active' => 0]);
        }
        elseif ($action=="delete_team") {
            $users = DB::table('tbl_team')
                    ->whereIn('tt_id', $arr_value)
                    ->update(['tt_is_deleted' => 1]);
        }        
    }     

    //upload excel file for bulk learners
    public function upload_learners_csv(Request $request){

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                
                $error_msg=Config::get('messages.learners.learners_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                //convert the object data into array
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                $key_value=array_keys($key_array[0]);
                
                if($key_value[0]!="name" || $key_value[1]!="function"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                $date=date('Y-m-d H:i:s');
                if(!empty($array_data[0])){
                   $i=0;
                   
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {

                                $code_data=\DB::table('users')
                                    ->select('id','name')
                                    ->where('name', '=', $value1['name'] )
                                    ->where('is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                   $insert[] = [
                                    'name' => $value1['name'],
                                    'email' => $value1['function'],
                                    'created_at'=>$date,
                                    'updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['name'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('users')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.learners.learners_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }

    //download data into excel format
    function learners_download_excel(){
        $customer_data = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
            left join tbl_department_master tdm on users.user_department = tdm.tdm_id
            left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
            left join users u on u.id = users.user_manager
            left join role_user ru on users.id = ru.user_id
            left join roles r on ru.role_id = r.id
            WHERE users.is_deleted='0' AND r.title = 'Learners'"));        
        $customer_data=json_decode(json_encode($customer_data),true);        
         $customer_array[] = array('Emp Code', 'Emp Name','Department','Manager Name','Role','Level','Enroll Courses');
         foreach($customer_data as $customer){     
          $customer_array[] = array(
           'Emp Code'  => $customer['employee_code'],
           'Emp Name'   => $customer['name'],
           'Department'  => $customer['tdm_department'],
           'Manager Name'   => $customer['user_manager'],
           'Role'  => $customer['title'],
           'Level'   => $customer['user_level'],   
           'Enroll Courses'   => $customer['name'],                                
          );
        }
        return Excel::create('learners', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }

    //download data into excel format
    function learners_team_download_excel(Request $request){
        if(!empty($request->session()->get('team_id'))){
            $team_id = "AND tlt.tlt_team_id =" .$request->session()->get('team_id');
        }else{
            $team_id = "AND tlt.tlt_team_id = 0";
        }          
        $customer_data = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
            left join tbl_department_master tdm on users.user_department = tdm.tdm_id
            left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
            left join users u on u.id = users.user_manager
            left join role_user ru on users.id = ru.user_id
            left join roles r on ru.role_id = r.id 
            inner join tbl_learner_team tlt on tlt.tlt_learner_id= users.id
            WHERE users.is_deleted='0' AND r.title = 'Learners'".$team_id));        
        $customer_data=json_decode(json_encode($customer_data),true);        
         $customer_array[] = array('Emp Code', 'Emp Name','Department','Manager Name','Role','Level','Enroll Courses');
         foreach($customer_data as $customer){     
          $customer_array[] = array(
           'Emp Code'  => $customer['employee_code'],
           'Emp Name'   => $customer['name'],
           'Department'  => $customer['tdm_department'],
           'Manager Name'   => $customer['user_manager'],
           'Role'  => $customer['title'],
           'Level'   => $customer['user_level'],   
           'Enroll Courses'   => $customer['name'],                                
          );
        }
        return Excel::create('learners', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }    

    //download team list into excel format
    function team_list_download_excel(){
        $customer_data = DB::select( DB::raw("SELECT tbl_team.tt_id,tbl_team.tt_title,(SELECT count(tbl_learner_team.tlt_learner_id) FROM tbl_learner_team WHERE tbl_learner_team.tlt_team_id = tbl_team.tt_id GROUP BY tbl_learner_team.tlt_team_id) as learners,(SELECT array_to_string(array_agg(tbl_courses.tc_title),',') as course_title FROM tbl_team_course_releation INNER JOIN tbl_courses ON tbl_team_course_releation.ttcr_course_id = tbl_courses.tc_id WHERE tbl_team_course_releation.ttcr_team_id = tbl_team.tt_id GROUP BY tbl_team_course_releation.ttcr_team_id) as course_assigned,(SELECT array_to_string(array_agg(tbl_learning_path.tlp_name),',') as learning_path FROM tbl_learningpath_team_relation INNER JOIN tbl_learning_path ON tbl_learningpath_team_relation.tltr_learning_path_id = tbl_learning_path.tlp_id WHERE tbl_learningpath_team_relation.tltr_team_id = tbl_team.tt_id GROUP BY tbl_learningpath_team_relation.tltr_team_id) as learning_path FROM public.tbl_team WHERE tbl_team.tt_is_deleted = 0 "));        
        $customer_data=json_decode(json_encode($customer_data),true);        
         $customer_array[] = array('Team', 'Learners','Department','Course Assigned','Learning Path Assigned');
         foreach($customer_data as $customer){     
          $customer_array[] = array(
           'Team'  => $customer['tt_title'],
           'Learners'   => $customer['learners'],
           'Course Assigned'  => $customer['course_assigned'],
           'Learning Path Assigned'   => $customer['learning_path'],                               
          );
        }
        return Excel::create('team', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }

    //download data into pdf format
    public function learners_download_PDF(){
        // $learners = DB::table("users")
        //         ->where('is_deleted', 0)
        //         ->get();
        $customer_data = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
            left join tbl_department_master tdm on users.user_department = tdm.tdm_id
            left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
            left join users u on u.id = users.user_manager
            left join role_user ru on users.id = ru.user_id
            left join roles r on ru.role_id = r.id
            WHERE users.is_deleted='0' AND r.title = 'Learners'"));        
        $learners=json_decode(json_encode($customer_data),true);          
        view()->share('learners',$learners);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.learners_pdf');
        return $pdf->download('learnerspdfview.pdf');
    }

    //download data into pdf format
    public function learners_team_download_PDF(Request $request){
        if(!empty($request->session()->get('team_id'))){
            $team_id = "AND tlt.tlt_team_id =" .$request->session()->get('team_id');
        }else{
            $team_id = "AND tlt.tlt_team_id = 0";
        } 

        $customer_data = DB::select( DB::raw("SELECT users.id,users.employee_code,users.name,tdm.tdm_department,u.name as user_manager,tsl.tsl_seniority as user_level,r.title FROM users
            left join tbl_department_master tdm on users.user_department = tdm.tdm_id
            left join tbl_seniority_level tsl on users.user_level  = tsl.tsl_id 
            left join users u on u.id = users.user_manager
            left join role_user ru on users.id = ru.user_id
            left join roles r on ru.role_id = r.id 
            inner join tbl_learner_team tlt on tlt.tlt_learner_id= users.id
            WHERE users.is_deleted='0' AND r.title = 'Learners'".$team_id));        
        $learners=json_decode(json_encode($customer_data),true);          
        view()->share('learners',$learners);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.learners_pdf');
        return $pdf->download('learnersteampdfview.pdf');
    }    

    //download data into pdf format team list
    public function team_list_download_PDF(){
        $customer_data = DB::select( DB::raw("SELECT tbl_team.tt_id,tbl_team.tt_title,(SELECT count(tbl_learner_team.tlt_learner_id) FROM tbl_learner_team WHERE tbl_learner_team.tlt_team_id = tbl_team.tt_id GROUP BY tbl_learner_team.tlt_team_id) as learners,(SELECT array_to_string(array_agg(tbl_courses.tc_title),',') as course_title FROM tbl_team_course_releation INNER JOIN tbl_courses ON tbl_team_course_releation.ttcr_course_id = tbl_courses.tc_id WHERE tbl_team_course_releation.ttcr_team_id = tbl_team.tt_id GROUP BY tbl_team_course_releation.ttcr_team_id) as course_assigned,(SELECT array_to_string(array_agg(tbl_learning_path.tlp_name),',') as learning_path FROM tbl_learningpath_team_relation INNER JOIN tbl_learning_path ON tbl_learningpath_team_relation.tltr_learning_path_id = tbl_learning_path.tlp_id WHERE tbl_learningpath_team_relation.tltr_team_id = tbl_team.tt_id GROUP BY tbl_learningpath_team_relation.tltr_team_id) as learning_path FROM public.tbl_team WHERE tbl_team.tt_is_deleted = 0 "));        
        $learners=json_decode(json_encode($customer_data),true);          
        view()->share('learners',$learners);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.team_list_pdf');
        return $pdf->download('teamlistpdfview.pdf');
    }    
    //download sample format for excel
    public function download_sample_learners_excel(){
        $file= public_path('uploads\test_csv\learners.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }   

    public function getDepartmentList(){
        $total_record = DB::select( DB::raw("SELECT tdm_id,tdm_department_code,tdm_department FROM tbl_department_master WHERE tdm_is_deleted='0' "));
        return $total_record=json_encode($total_record);
    }   

    public function getManagerList(){
        $total_record = DB::select( DB::raw("SELECT id,name FROM users WHERE id IN (SELECT DISTINCT user_manager FROM users WHERE is_deleted='0')"));
        return $total_record=json_encode($total_record);
    }

    public function getLevelList(){
        $total_record = DB::select( DB::raw("SELECT tsl_id,tsl_seniority_code,tsl_seniority FROM tbl_seniority_level WHERE tsl_is_deleted='0' "));
        return $total_record=json_encode($total_record);
    }

    public function getCategory(){
        $total_record = DB::select( DB::raw("SELECT tccm_id,tccm_category FROM tbl_course_category_master WHERE tccm_is_deleted='0' AND tccm_is_active ='1'"));
        return $total_record=json_encode($total_record);
    }

    public function getDevelopementArea(){
        $total_record = DB::select( DB::raw("SELECT tda_id,tda_development FROM tbl_development_area WHERE tda_is_deleted='0' "));
        return $total_record=json_encode($total_record);
    }        

    public function getEnrolledCoursesList(){
        $total_record = DB::select( DB::raw("SELECT tc_id,tc_title FROM tbl_courses WHERE tc_is_deleted='0' AND tc_is_active = '1' "));
        return $total_record=json_encode($total_record);
    }

    public function getFullCourseList(Request $request){

        $where = "";
        $tc_id_list = "";
        if(!empty($request->filterdta)){
            $filterdta = json_decode($request->filterdta,true);       
            $search = $filterdta['keyword'];  
            $course_cat = $filterdta['course_cat'];  
            $course = $filterdta['course'];  
            $dev_area = $filterdta['dev_area'];  
            $seniority = $filterdta['seniority'];   

            if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tc_title) LIKE '".$search."%' ";
            $where .=" OR LOWER(tc_description) LIKE '".$search."%' )";                                    
            } 

            if(!empty($course)){
                $tc_id_list .= $course .",";                              
            } 

            if(!empty($course_cat)){
                $get_path = DB::select( DB::raw("SELECT tccr_course_id FROM tbl_category_course_relation WHERE tccr_category_id = '".$course_cat."' "));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tc_id_list .= $get_path[$i]['tccr_course_id'].",";
                }         
            }   

            if(!empty($dev_area)){
                $get_path = DB::select( DB::raw("SELECT tdacr_course_id FROM tbl_developement_area_course_relation WHERE tdacr_developement_area_id = '".$dev_area."' AND tdacr_is_active = 1 AND tdacr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tc_id_list .= $get_path[$i]['tdacr_course_id'].",";
                }         
            } 

            if(!empty($seniority)){
                $get_path = DB::select( DB::raw("SELECT tslcr_course_id FROM tbl_seniority_level_course_relation WHERE tslcr_seniority_level_id = '".$seniority."' AND tslcr_is_active = 1 AND tslcr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tc_id_list .= $get_path[$i]['tslcr_course_id'].",";
                }         
            }                        

            if(!empty($tc_id_list) || $tc_id_list != ""){

                $tc_id_list = chop($tc_id_list,",");
                $where .=" AND ";
                $where .=" tc_id IN (".$tc_id_list.") ";                
            }            
        }

        if(!empty($request->session()->get('team_id'))){
            $notin = "AND tc_id NOT IN (SELECT DISTINCT ttcr_course_id FROM tbl_team_course_releation WHERE ttcr_team_id = ".$request->session()->get('team_id').")" ;
        }else{
            $notin = "";
        } 

        $total_course = DB::select( DB::raw("SELECT tc_id,tc_title,tc_description FROM tbl_courses WHERE tc_is_deleted='0' AND tc_is_active = '1' ".$notin .$where));
        return $total_course=json_encode($total_course);        
    }

    public function getTeamCourseList(Request $request){
        $where = "";
        $tc_id_list = "";
        if(!empty($request->filterdta)){
            $filterdta = json_decode($request->filterdta,true);       
            $search = $filterdta['keyword'];  
            $course_cat = $filterdta['course_cat'];  
            $course = $filterdta['course'];  
            $dev_area = $filterdta['dev_area'];  
            $seniority = $filterdta['seniority'];   

            if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tc.tc_title) LIKE '".$search."%' ";
            $where .=" OR LOWER(tc.tc_description) LIKE '".$search."%' )";                                    
            } 

            if(!empty($course)){ 
                $tc_id_list .= $course .",";                              
            } 

            if(!empty($course_cat)){
                $get_path = DB::select( DB::raw("SELECT tccr_course_id FROM tbl_category_course_relation WHERE tccr_category_id = '".$course_cat."' "));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tc_id_list .= $get_path[$i]['tccr_course_id'].",";
                }         
            }   

            if(!empty($dev_area)){
                $get_path = DB::select( DB::raw("SELECT tdacr_course_id FROM tbl_developement_area_course_relation WHERE tdacr_developement_area_id = '".$dev_area."' AND tdacr_is_active = 1 AND tdacr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tc_id_list .= $get_path[$i]['tdacr_course_id'].",";
                }         
            } 

            if(!empty($seniority)){
                $get_path = DB::select( DB::raw("SELECT tslcr_course_id FROM tbl_seniority_level_course_relation WHERE tslcr_seniority_level_id = '".$seniority."' AND tslcr_is_active = 1 AND tslcr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tc_id_list .= $get_path[$i]['tslcr_course_id'].",";
                }         
            }                        

            if(!empty($tc_id_list) || $tc_id_list != ""){

                $tc_id_list = chop($tc_id_list,",");
                $where .=" AND ";
                $where .=" tc.tc_id IN (".$tc_id_list.") ";                
            }            
        }

        $total_course = DB::select( DB::raw("SELECT tc.tc_id,tc.tc_title,tc.tc_description FROM tbl_courses tc INNER JOIN tbl_team_course_releation ttcr ON tc.tc_id = ttcr.ttcr_course_id WHERE tc.tc_is_deleted='0' AND tc.tc_is_active = '1' AND ttcr.ttcr_team_id = '".$request->session()->get('team_id')."' ".$where));
        return $total_course=json_encode($total_course);        
    }    

    public function addCourseToTeam(Request $request){
        $selected_courses_comma = $request->selected_courses; 

        // $users = DB::table('tbl_team')
        //         ->where('tt_id', $request->session()->get('team_id'))
        //         ->update(['course_team' => $selected_courses]);
        $selected_courses = explode(",", $selected_courses_comma);
        foreach ($selected_courses as $key => $value) {
            $total_course = DB::select( DB::raw("SELECT 1 FROM tbl_team_course_releation WHERE ttcr_team_id = '".$request->session()->get('team_id')."' AND ttcr_course_id = '".$value."' "));
            if(empty($total_course)){
                DB::table('tbl_team_course_releation')->insert(['ttcr_team_id'=>$request->session()->get('team_id'),'ttcr_course_id'=>$value]);
            }else{
                return "false";
            }
        }
        return "true";
    }

    public function removeCourseFromTeam(Request $request){
        $selected_courses = $request->selected_courses; 
        $selected_courses = explode(",", $selected_courses);
        // foreach ($selected_courses as $key => $value) {
            DB::table('tbl_team_course_releation')->whereIn('ttcr_course_id', $selected_courses)->where('ttcr_team_id',$request->session()->get('team_id'))->delete();
        // }
        return "true";
    }

    public function getTeamDetails(Request $request){
        if(!empty($request->session()->get('team_id'))){
            $team_details = DB::select( DB::raw("SELECT tt_title,tt_description FROM tbl_team WHERE tt_is_active = 1 AND tt_is_deleted = 0 AND tt_id = '".$request->session()->get('team_id')."' "));
            return $team_details=json_encode($team_details); 
        }else{
            return "false";
        }
        
    }    

    public function getFullLearningPathList(Request $request){

        $where = "";
        $tlp_id_list = "";
        if(!empty($request->filterdta)){
            $filterdta = json_decode($request->filterdta,true);       
            $search = $filterdta['keyword'];  
            $course_cat = $filterdta['course_cat'];  
            $course = $filterdta['course'];  
            $dev_area = $filterdta['dev_area'];  
            $seniority = $filterdta['seniority'];   

            if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tlp_name) LIKE '".$search."%' ";
            $where .=" OR LOWER(tlp_description) LIKE '".$search."%' )";                                    
            } 

            if(!empty($course)){ 
                $tlp_id_list .= $course .",";                              
            } 

            if(!empty($course_cat)){
                $get_path = DB::select( DB::raw("SELECT tbl_learning_path_course_relation.tslpcr_learning_path_id AS tslpcr_learning_path_id FROM tbl_category_course_relation INNER JOIN tbl_learning_path_course_relation ON tbl_category_course_relation.tccr_course_id=tbl_learning_path_course_relation.tslpcr_course_id WHERE tbl_category_course_relation.tccr_category_id = '".$course_cat."' "));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tlp_id_list .= $get_path[$i]['tslpcr_learning_path_id'].",";
                }         
            }   

            if(!empty($dev_area)){
                $get_path = DB::select( DB::raw("SELECT tbl_learning_path_course_relation.tslpcr_learning_path_id AS tslpcr_learning_path_id FROM tbl_developement_area_course_relation INNER JOIN tbl_learning_path_course_relation ON tbl_developement_area_course_relation.tdacr_course_id=tbl_learning_path_course_relation.tslpcr_course_id WHERE tbl_developement_area_course_relation.tdacr_developement_area_id = '".$dev_area."' AND tbl_developement_area_course_relation.tdacr_is_active = 1 AND tbl_developement_area_course_relation.tdacr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tlp_id_list .= $get_path[$i]['tslpcr_learning_path_id'].",";
                }         
            } 

            if(!empty($seniority)){
                $get_path = DB::select( DB::raw("SELECT tbl_learning_path_course_relation.tslpcr_learning_path_id AS tslpcr_learning_path_id FROM tbl_seniority_level_course_relation INNER JOIN tbl_learning_path_course_relation ON tbl_seniority_level_course_relation.tslcr_course_id=tbl_learning_path_course_relation.tslpcr_course_id WHERE tbl_seniority_level_course_relation.tslcr_seniority_level_id = '".$seniority."' AND tbl_seniority_level_course_relation.tslcr_is_active = 1 AND tbl_seniority_level_course_relation.tslcr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tlp_id_list .= $get_path[$i]['tslpcr_learning_path_id'].",";
                }         
            }                        

            if(!empty($tlp_id_list) || $tlp_id_list != ""){

                $tlp_id_list = chop($tlp_id_list,",");
                $where .=" AND ";
                $where .=" tlp_id IN (".$tlp_id_list.") ";                
            }            
        }

        if(!empty($request->session()->get('team_id'))){
            $notin = "AND tlp_id NOT IN (SELECT DISTINCT tltr_learning_path_id FROM tbl_learningpath_team_relation WHERE tltr_team_id = ".$request->session()->get('team_id').")" ;
        }else{
            $notin = "";
        } 

        $total_learning_path = DB::select( DB::raw("SELECT tlp_id,tlp_name,tlp_description FROM tbl_learning_path WHERE tlp_is_deleted='0' AND tlp_is_active = '1' ".$notin .$where));
        return $total_learning_path=json_encode($total_learning_path);        
    }

    public function getTeamLearingPathList(Request $request){

        $where = "";
        $tlp_id_list = "";
        if(!empty($request->filterdta)){
            $filterdta = json_decode($request->filterdta,true);       
            $search = $filterdta['keyword'];  
            $course_cat = $filterdta['course_cat'];  
            $course = $filterdta['course'];  
            $dev_area = $filterdta['dev_area'];  
            $seniority = $filterdta['seniority'];   

            if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tlp.tlp_name) LIKE '".$search."%' ";
            $where .=" OR LOWER(tlp.tlp_description) LIKE '".$search."%' )";                                    
            } 

            if(!empty($course)){ 
                $tlp_id_list .= $course .",";                              
            } 

            if(!empty($course_cat)){
                $get_path = DB::select( DB::raw("SELECT tbl_learning_path_course_relation.tslpcr_learning_path_id AS tslpcr_learning_path_id FROM tbl_category_course_relation INNER JOIN tbl_learning_path_course_relation ON tbl_category_course_relation.tccr_course_id=tbl_learning_path_course_relation.tslpcr_course_id WHERE tbl_category_course_relation.tccr_category_id = '".$course_cat."' "));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tlp_id_list .= $get_path[$i]['tslpcr_learning_path_id'].",";
                }         
            }   

            if(!empty($dev_area)){
                $get_path = DB::select( DB::raw("SELECT tbl_learning_path_course_relation.tslpcr_learning_path_id AS tslpcr_learning_path_id FROM tbl_developement_area_course_relation INNER JOIN tbl_learning_path_course_relation ON tbl_developement_area_course_relation.tdacr_course_id=tbl_learning_path_course_relation.tslpcr_course_id WHERE tbl_developement_area_course_relation.tdacr_developement_area_id = '".$dev_area."' AND tbl_developement_area_course_relation.tdacr_is_active = 1 AND tbl_developement_area_course_relation.tdacr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tlp_id_list .= $get_path[$i]['tslpcr_learning_path_id'].",";
                }         
            } 

            if(!empty($seniority)){
                $get_path = DB::select( DB::raw("SELECT tbl_learning_path_course_relation.tslpcr_learning_path_id AS tslpcr_learning_path_id FROM tbl_seniority_level_course_relation INNER JOIN tbl_learning_path_course_relation ON tbl_seniority_level_course_relation.tslcr_course_id=tbl_learning_path_course_relation.tslpcr_course_id WHERE tbl_seniority_level_course_relation.tslcr_seniority_level_id = '".$seniority."' AND tbl_seniority_level_course_relation.tslcr_is_active = 1 AND tbl_seniority_level_course_relation.tslcr_is_deleted = 0"));
                $get_path=json_decode(json_encode($get_path),true);

                for($i=0;$i<count($get_path);$i++){
                    $tlp_id_list .= $get_path[$i]['tslpcr_learning_path_id'].",";
                }         
            }                        

            if(!empty($tlp_id_list) || $tlp_id_list != ""){

                $tlp_id_list = chop($tlp_id_list,",");
                $where .=" AND ";
                $where .=" tlp.tlp_id IN (".$tlp_id_list.") ";                
            }            
        }

        if(!empty($request->session()->get('team_id'))){
            $notin = "AND tlp_id NOT IN (SELECT DISTINCT tltr_learning_path_id FROM tbl_learningpath_team_relation WHERE tltr_team_id = ".$request->session()->get('team_id').")" ;
        }else{
            $notin = "";
        } 

        $total_learning_path = DB::select( DB::raw("SELECT tlp.tlp_id,tlp.tlp_name,tlp.tlp_description FROM tbl_learning_path tlp INNER JOIN tbl_learningpath_team_relation tltr ON tlp.tlp_id = tltr.tltr_learning_path_id WHERE tlp.tlp_is_deleted='0' AND tlp.tlp_is_active = '1' AND tltr.tltr_team_id = '".$request->session()->get('team_id')."' ".$where));
        return $total_learning_path=json_encode($total_learning_path);        
    }    

    public function addLearningPathToTeam(Request $request){
        $selected_courses = $request->selected_courses; 
        $selected_courses = explode(",", $selected_courses);
        foreach ($selected_courses as $key => $value) {
            $total_course = DB::select( DB::raw("SELECT 1 FROM tbl_learningpath_team_relation WHERE tltr_team_id = '".$request->session()->get('team_id')."' AND tltr_learning_path_id = '".$value."' "));
            if(empty($total_course)){
                DB::table('tbl_learningpath_team_relation')->insert(['tltr_team_id'=>$request->session()->get('team_id'),'tltr_learning_path_id'=>$value]);
            }else{
                return "false";
            }
        }
        return "true";
    }

    public function removeLearningPathFromTeam(Request $request){
        $selected_courses = $request->selected_courses; 
        $selected_courses = explode(",", $selected_courses);
        // foreach ($selected_courses as $key => $value) {
            DB::table('tbl_learningpath_team_relation')->whereIn('tltr_learning_path_id', $selected_courses)->where('tltr_team_id',$request->session()->get('team_id'))->delete();
        // }
        return "true";
    } 

    //when user click on delete button listing
    public function delete_team_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_team')
                ->where('tt_id', $delete_id)
                ->update(['tt_is_deleted' => 1]);

        // DB::table('tbl_team_course_releation')->where('ttcr_team_id',$delete_id)->delete();
        // DB::table('tbl_learningpath_team_relation')->where('tltr_team_id',$delete_id)->delete();
        // DB::table('tbl_learner_team')->where('tlt_team_id',$delete_id)->delete();
        }        
    }     

    public function set_session_team(Request $request){
        //set session if user click on edit or view team
        if(empty($request->team_id) || $request->team_id == ""){
            $request->team_id = "";
        }
        $request->session()->put('team_id', $request->team_id);
        $request->session()->put('btn_type', $request->btn_type);
    }       
}
