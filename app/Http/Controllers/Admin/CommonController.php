<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CommonController extends Controller
{
    

    public function save_session_info(Request $request)
    {
        if($request->input('form_name')=="users"){
            $column_array=$request->input('column_array');
            session(['user_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="competency"){
            $column_array=$request->input('column_array');
            session(['competency_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="designation"){
            $column_array=$request->input('column_array');
            session(['designation_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="department"){
            $column_array=$request->input('column_array');
            session(['department_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="development"){
            $column_array=$request->input('column_array');
            session(['development_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="functions"){
            $column_array=$request->input('column_array');
            session(['functions_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="learners"){
            $column_array=$request->input('column_array');
            session(['learners_hidden_column_array' => $column_array]);
        }        
        if($request->input('form_name')=="seniority"){
            $column_array=$request->input('column_array');
            session(['seniority_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="learners_team"){
            $column_array=$request->input('column_array');
            session(['learners_team_hidden_column_array' => $column_array]);
        } 
        if($request->input('form_name')=="team_listing"){
            $column_array=$request->input('column_array');
            session(['team_listing_hidden_column_array' => $column_array]);
        } 
        if($request->input('form_name')=="all_user"){
            $column_array=$request->input('column_array');
            session(['all_user_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="manager"){
            $column_array=$request->input('column_array');
            session(['user_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="hr"){
            $column_array=$request->input('column_array');
            session(['hr_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="expert"){
            $column_array=$request->input('column_array');
            session(['expert_hidden_column_array' => $column_array]);
        }

        if($request->input('form_name')=="category"){
            $column_array=$request->input('column_array');
            session(['category_hidden_column_array' => $column_array]);
        } 

        if($request->input('form_name')=="instructor"){
            $column_array=$request->input('column_array');
            session(['instructor_hidden_column_array' => $column_array]);
        }  
        if($request->input('form_name')=="webinar"){
            $column_array=$request->input('column_array');
            session(['webinar_hidden_column_array' => $column_array]);
        }
        if($request->input('form_name')=="test"){
            $column_array=$request->input('column_array');
            session(['test_hidden_column_array' => $column_array]);
        } 
    }
    public function save_user_type_for_permission(Request $request){
       $role_id=$request->input('role_id');

      // session(['sidebar_role_id'=>$role_id]);
       session(['sidebar_role_id'=>4]);
    }
}
