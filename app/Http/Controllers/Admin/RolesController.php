<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRolesRequest;
use App\Http\Requests\Admin\UpdateRolesRequest;
use DB;

class RolesController extends Controller
{
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('role_access')) {
            return abort(401);
        }


                $roles = Role::all();

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('role_create')) {
            return abort(401);
        }
        $permissions = DB::table('permissions')->join('module_headers','permissions.header_id','=','module_headers.id')
                                               ->select(array('module_headers.parent_id','permissions.id','permissions.title','permissions.header_id','module_headers.id as moduleid','module_headers.header_name'))
                                               ->where('module_headers.is_active','=','1')
                                               ->orderBy('permissions.header_id')
                                               ->orderBy('permissions.id')
                                               ->get();
        $permissions = json_decode(json_encode($permissions),true);
        // $permissions = \App\Permission::get()->pluck('title', 'id');

        return view('admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\StoreRolesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRolesRequest $request)
    {
        if (! Gate::allows('role_create')) {
            return abort(401);
        }
        // dd($request->input('permistionid'));
        // foreach($request->input('permistionid') as $id){
        // echo "<br>$id was checked! ";
        // }
        // $role = Role::create($request->all());
        // $role->permission()->sync(array_filter((array)$request->input('permission')));
        $role = Role::create($request->all());
        $role_id = $role->id;

        $permistionidlist = $request->input('permission');
        foreach($permistionidlist as $permissionid){
            $array[] = array('permission_id'=>$permissionid,'role_id'=>$role_id);
        }
        DB::table('permission_role')->insert($array);

        return redirect()->route('admin.roles.index');
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('role_edit')) {
            return abort(401);
        }
        // $permissions = \App\Permission::get()->pluck('title', 'id');
        $permissions = DB::table('permissions')->join('module_headers','permissions.header_id','=','module_headers.id')
                                               ->select(array('module_headers.parent_id','permissions.id','permissions.title','permissions.header_id','module_headers.id as moduleid','module_headers.header_name'))
                                               ->where('module_headers.is_active','=','1')
                                               ->orderBy('permissions.header_id')
                                               ->orderBy('permissions.id')
                                               ->get();
        $permissions = json_decode(json_encode($permissions),true);

        $role = Role::findOrFail($id);

        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update Role in storage.
     *
     * @param  \App\Http\Requests\UpdateRolesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRolesRequest $request, $id)
    {
        if (! Gate::allows('role_edit')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->update($request->all());
        $role->permission()->sync(array_filter((array)$request->input('permission')));



        return redirect()->route('admin.roles.index');
    }


    /**
     * Display Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('role_view')) {
            return abort(401);
        }
        $permissions = \App\Permission::get()->pluck('title', 'id');
        $users = \App\User::whereHas('role',
                    function ($query) use ($id) {
                        $query->where('id', $id);
                    })->get();

        $role = Role::findOrFail($id);

        return view('admin.roles.show', compact('role', 'users'));
    }


    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('role_delete')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('admin.roles.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('role_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    public function getSelectedPermissions(Request $request){
        $permissions = DB::table('permission_role')->select('permission_id')->where('role_id','=',$request->roleid)->get();
        $permissions = json_encode($permissions); 
        return $permissions;    
    }
}
