<?php

namespace App\Http\Controllers\Admin;

use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;

class FunctionsController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $array_code_data=$this->get_functions_code_list();
        return view('admin.functions.index', compact('array_code_data'));
        
    }
    //listing of competency
    public function functions_ajax_listing(Request $request){
        
        $columns=array(0=>'chk_box',1=>'tfm_function',2=>'tfm_function_code');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_functions_code=$request->input('dropdown_functions_code');
        $search=$request->input('searchtxt');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tfm_function) LIKE '".$search."%' ";
            $where .=" OR LOWER(tfm_function_code) LIKE '".$search."%' )";
        }
        if($dropdown_functions_code!=""){
            $where .=" AND ";
            $where .=" tfm_id ='".$dropdown_functions_code."' ";

        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY tfm_id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;
        
        $total_record = DB::select( DB::raw("SELECT tfm_id,tfm_function_code,tfm_function FROM tbl_functions_master WHERE tfm_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tfm_id,tfm_function_code,tfm_function FROM tbl_functions_master WHERE tfm_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('functions_hidden_column_array')){
                $col_arr_value = $request->session()->get('functions_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tfm_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="FunctionCode"){
                            $nesteddata['FunctionCode']=ucwords($row['tfm_function_code']);   
                        }
                        if($value[0]=="FunctionTitle"){
                            $nesteddata['FunctionTitle']=ucwords($row['tfm_function']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tfm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tfm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tfm_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['FunctionCode']=ucwords($row['tfm_function_code']);
                    $nesteddata['FunctionTitle']=ucwords($row['tfm_function']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tfm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tfm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //competency common action enable,disable and delete
    public function functions_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_functions_master')
                    ->whereIn('tfm_id', $arr_value)
                    ->update(['tfm_is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_functions_master')
                    ->whereIn('tfm_id', $arr_value)
                    ->update(['tfm_is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_functions_master')
                    ->whereIn('tfm_id', $arr_value)
                    ->update(['tfm_is_deleted' => 1]);
           
        }
    }
    //save functions
    function save_functions_data(Request $request){
        $errors = array();
        $input = $request->all();
        if($input['tfm_function_code']==""){
            $errors['tfm_function_code'] =Config::get('messages.functions.FUNCTIONS_CODE_ERROR');
        }
        if($input['tfm_function']==""){
            $errors['tfm_function'] = Config::get('messages.functions.FUNCTIONS_TITLE_ERROR');
        }

        $users_code = DB::table('tbl_functions_master')
        ->where('tfm_function_code', $input['tfm_function_code'])
        ->where('tfm_is_deleted', 0)
        ->get();
        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tfm_code_exist'] = Config::get('messages.functions.FUNCTIONS_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            $date=date('Y-m-d H:i:s');
            DB::table('tbl_functions_master')->insert([
            [  
                'tfm_function_code' => $input['tfm_function_code'], 
                'tfm_function' => $input['tfm_function'],
                'tfm_created_at'=>$date,
                'tfm_updated_at'=>$date
                ]
            ]);
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }
    }
    //return data for edit functions form
    public function show_functions_edit_form(Request $request){
        $input=$request->all();
        $edit_id=$input['edit_id'];
        $edit_data = DB::table('tbl_functions_master')->where('tfm_id', '=', $edit_id)->get();
        if(count($edit_data)>0){
            echo json_encode($edit_data);
        }
    }
    //update functions master
    public function update_functions_master(Request $request){
        $input=$request->all();
        $tfm_id=$input['tfm_id'];
        $errors=array();
        if($input['tfm_function_code']==""){
            $errors['tfm_function_code'] =Config::get('messages.functions.FUNCTIONS_CODE_ERROR');
        }
        if($input['tfm_function']==""){
            $errors['tfm_function'] = Config::get('messages.functions.FUNCTIONS_TITLE_ERROR');
        }
        $users_code =DB::table('tbl_functions_master')
                ->where([
                    ['tfm_function_code', '=', $input['tfm_function_code']],
                    ['tfm_id', '!=', $tfm_id],
                    ['tfm_is_deleted', '=', 0]
                ])->get();

        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tfm_code_exist'] = Config::get('messages.functions.FUNCTIONS_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            if($tfm_id>0){
                DB::table('tbl_functions_master')
                ->where('tfm_id', $tfm_id)
                ->update(['tfm_function_code' => $input['tfm_function_code'],'tfm_function'=>$input['tfm_function']]);
               echo json_encode(array('status' => 'success','errors' => $errors));

            }
        }
    }
    //upload excel file for bulk functions
    public function upload_functions_csv(Request $request){

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                
                $error_msg=Config::get('messages.functions.FUNCTIONS_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                //convert the object data into array
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                $key_value=array_keys($key_array[0]);
                
                if($key_value[0]!="functions_code" || $key_value[1]!="function"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                $date=date('Y-m-d H:i:s');
                if(!empty($array_data[0])){
                   $i=0;
                   
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {

                                $code_data=\DB::table('tbl_functions_master')
                                    ->select('tfm_id','tfm_function_code')
                                    ->where('tfm_function_code', '=', $value1['functions_code'] )
                                    ->where('tfm_is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                   $insert[] = [
                                    'tfm_function_code' => $value1['functions_code'],
                                    'tfm_function' => $value1['function'],
                                    'tfm_created_at'=>$date,
                                    'tfm_updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['functions_code'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_functions_master')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.functions.FUNCTIONS_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }
    //delete functions code
    public function delete_functions_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_functions_master')
                ->where('tfm_id', $delete_id)
                ->update(['tfm_is_deleted' => 1]);
        }
    }
    //download data into excel format
    function functions_download_excel(){
         $customer_data = DB::table('tbl_functions_master')
                        ->where('tfm_is_deleted', 0)
                        ->get()
                        ->toArray();
         $customer_array[] = array('Functions Code', 'Functions Title');
         foreach($customer_data as $customer){
          $customer_array[] = array(
           'Functions Code'  => $customer->tfm_function_code,
           'Functions Title'   => $customer->tfm_function,
          );
        }
        return Excel::create('functions', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function functions_download_PDF(){
        $functions = DB::table("tbl_functions_master")
                ->where('tfm_is_deleted', 0)
                ->get();
        view()->share('functions',$functions);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.functions_pdf');
        return $pdf->download('functionspdfview.pdf');
    }
    //download sample format for excel
    public function download_sample_functions_excel(){
        $file= public_path('uploads\test_csv\functions.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }
}
