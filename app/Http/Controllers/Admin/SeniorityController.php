<?php

namespace App\Http\Controllers\Admin;

use App\Seniority;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;

class SeniorityController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $array_code_data=$this->get_seniority_code_list();
        return view('admin.seniority.index', compact('array_code_data'));
        
    }
    //listing of seniority
    public function seniority_ajax_listing(Request $request){
        
        $columns=array(0=>'chk_box',1=>'tsl_seniority',2=>'tsl_seniority_code');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_seniority_code=$request->input('dropdown_seniority_code');
        $search=$request->input('searchtxt');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tsl_seniority) LIKE '".$search."%' ";
            $where .=" OR LOWER(tsl_seniority_code) LIKE '".$search."%' )";
        }
        if($dropdown_seniority_code!=""){
            $where .=" AND ";
            $where .=" tsl_id ='".$dropdown_seniority_code."' ";

        }

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY tsl_id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;
        
        $total_record = DB::select( DB::raw("SELECT tsl_id,tsl_seniority_code,tsl_seniority FROM tbl_seniority_level WHERE tsl_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tsl_id,INITCAP(tsl_seniority_code) as tsl_seniority_code ,INITCAP(tsl_seniority) as tsl_seniority FROM tbl_seniority_level WHERE tsl_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('seniority_hidden_column_array')){
                $col_arr_value = $request->session()->get('seniority_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tsl_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';    
                        }
                        if($value[0]=="SeniorityCode"){
                            $nesteddata['SeniorityCode']=ucwords($row['tsl_seniority_code']);   
                        }
                        if($value[0]=="SeniorityTitle"){
                            $nesteddata['SeniorityTitle']=ucwords($row['tsl_seniority']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tsl_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tsl_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tsl_id'].'">
                       <span class="custom-control-indicator"></span></label>';
                    $nesteddata['SeniorityCode']=ucwords($row['tsl_seniority_code']);
                    $nesteddata['SeniorityTitle']=ucwords($row['tsl_seniority']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tsl_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tsl_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //seniority common action enable,disable and delete
    public function seniority_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_seniority_level')
                    ->whereIn('tsl_id', $arr_value)
                    ->update(['tsl_is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_seniority_level')
                    ->whereIn('tsl_id', $arr_value)
                    ->update(['tsl_is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_seniority_level')
                    ->whereIn('tsl_id', $arr_value)
                    ->update(['tsl_is_deleted' => 1]);
           
        }
    }
    //save seniority
    function save_seniority_data(Request $request){
        $errors = array();
        $input = $request->all();
        if($input['tsl_seniority_code']==""){
            $errors['tsl_seniority_code'] =Config::get('messages.seniority.SENIORITY_CODE_ERROR');
        }
        if($input['tsl_seniority']==""){
            $errors['tsl_seniority'] = Config::get('messages.seniority.SENIORITY_TITLE_ERROR');
        }

        $users_code = DB::table('tbl_seniority_level')
        ->where('tsl_seniority_code', $input['tsl_seniority_code'])
        ->where('tsl_is_deleted', 0)
        ->get();
        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tsl_code_exist'] = Config::get('messages.seniority.SENIORITY_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            $date=date('Y-m-d H:i:s');
            DB::table('tbl_seniority_level')->insert([
            [  
                'tsl_seniority_code' => $input['tsl_seniority_code'], 
                'tsl_seniority' => $input['tsl_seniority'],
                'tsl_created_at'=>$date,
                'tsl_updated_at'=>$date
                ]
            ]);
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }
    }
    //return data for edit seniority form
    public function show_seniority_edit_form(Request $request){
        $input=$request->all();
        $edit_id=$input['edit_id'];
        $edit_data = DB::table('tbl_seniority_level')->where('tsl_id', '=', $edit_id)->get();
        if(count($edit_data)>0){
            echo json_encode($edit_data);
        }
    }
    //update seniority master
    public function update_seniority_master(Request $request){
        $input=$request->all();
        $tsl_id=$input['tsl_id'];
        $errors=array();
        if($input['tsl_seniority_code']==""){
            $errors['tsl_seniority_code'] =Config::get('messages.seniority.SENIORITY_CODE_ERROR');
        }
        if($input['tsl_seniority']==""){
            $errors['tsl_seniority'] = Config::get('messages.seniority.SENIORITY_TITLE_ERROR');
        }
        $users_code =DB::table('tbl_seniority_level')
                ->where([
                    ['tsl_seniority_code', '=', $input['tsl_seniority_code']],
                    ['tsl_id', '!=', $tsl_id],
                    ['tsl_is_deleted', '=', 0]
                ])->get();

        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tsl_code_exist'] = Config::get('messages.seniority.SENIORITY_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            if($tsl_id>0){
                DB::table('tbl_seniority_level')
                ->where('tsl_id', $tsl_id)
                ->update(['tsl_seniority_code' => $input['tsl_seniority_code'],'tsl_seniority'=>$input['tsl_seniority']]);
               echo json_encode(array('status' => 'success','errors' => $errors));

            }
        }
    }
    //upload excel file for bulk seniority
    public function upload_seniority_csv(Request $request){

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                
                $error_msg=Config::get('messages.seniority.SENIORITY_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                //convert the object data into array
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                $key_value=array_keys($key_array[0]);

                if($key_value[0]!="seniority_code" || $key_value[1]!="seniority"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                $date=date('Y-m-d H:i:s');
                if(!empty($array_data[0])){
                   $i=0;
                   
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {

                                $code_data=\DB::table('tbl_seniority_level')
                                    ->select('tsl_id','tsl_seniority_code')
                                    ->where('tsl_seniority_code', '=', $value1['seniority_code'] )
                                    ->where('tsl_is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                   $insert[] = [
                                    'tsl_seniority_code' => $value1['seniority_code'],
                                    'tsl_seniority' => $value1['seniority'],
                                    'tsl_created_at'=>$date,
                                    'tsl_updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['seniority_code'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_seniority_level')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.seniority.SENIORITY_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }
    //delete seniority code
    public function delete_seniority_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_seniority_level')
                ->where('tsl_id', $delete_id)
                ->update(['tsl_is_deleted' => 1]);
        }
    }
    //download data into excel format
    function seniority_download_excel(){
         $customer_data = DB::table('tbl_seniority_level')
                        ->where('tsl_is_deleted', 0)
                        ->get()
                        ->toArray();
         $customer_array[] = array('Seniority Code', 'Seniority');
         foreach($customer_data as $customer){
          $customer_array[] = array(
           'Seniority Code'  => $customer->tsl_seniority_code,
           'Seniority'   => $customer->tsl_seniority,
          );
        }
        return Excel::create('seniority', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function seniority_download_PDF(){
        $seniority = DB::table("tbl_seniority_level")
                ->where('tsl_is_deleted', 0)
                ->get();
        view()->share('seniority',$seniority);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.seniority_pdf');
        return $pdf->download('Senioritypdfview.pdf');
    }
    //download sample format for excel
    public function download_sample_seniority_excel(){
        $file= public_path('uploads\test_csv\seniority.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }
}
