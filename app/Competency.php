<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competency extends Model
{
    protected $fillable = ['tcm_id','tcm_competency_code','tcm_competency_title','tcm_updated_at','tcm_created_at'];
    protected $table='tbl_competency_master';
    public $primary_key='tcm_id';
    public $timestamps = false;
}
