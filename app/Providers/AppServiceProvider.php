<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('partials.sidebar',function($view){

            //dd(session()->all());exit;

            $variableName = session('sidebar_role_id');
            if($variableName==""){
                $getdata = DB::table('role_user')->join('permission_role','role_user.role_id','=','permission_role.role_id')
                        ->join('permissions','permissions.id','=','permission_role.permission_id')
                        ->join('module_headers','permissions.header_id','=','module_headers.id')
                        ->select(array('module_headers.active_segments','module_headers.redirect_link','module_headers.id','module_headers.header_name','module_headers.icon_class','module_headers.sort_order','module_headers.parent_id','role_user.role_id'))
                        ->orderBy('module_headers.sort_order','asc')
                        ->where('role_user.user_id','=',auth()->user()->id)
                        ->distinct()
                        ->get();  
            }else{
                 $getdata = DB::table('role_user')->join('permission_role','role_user.role_id','=','permission_role.role_id')
                        ->join('permissions','permissions.id','=','permission_role.permission_id')
                        ->join('module_headers','permissions.header_id','=','module_headers.id')
                        ->select(array('module_headers.active_segments','module_headers.redirect_link','module_headers.id','module_headers.header_name','module_headers.icon_class','module_headers.sort_order','module_headers.parent_id','role_user.role_id'))
                        ->orderBy('module_headers.sort_order','asc')
                        ->where('role_user.user_id','=',auth()->user()->id)
                        ->where ('role_user.role_id','=',$variableName)
                        ->distinct()
                        ->get(); 
            }
        
            
            $getdata = json_decode(json_encode($getdata),true);
            
            $view->with(['getHeaders'=>$getdata]);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        /**
         * Added missing method for package to work
         */
        \Illuminate\Support\Collection::macro('lists', function ($a, $b = null) {
            return collect($this->items)->pluck($a, $b);
        });

    }
}
