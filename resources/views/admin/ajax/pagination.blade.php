					
		@php  $getcontents = json_decode(json_encode($arti),true);@endphp
					@foreach($getcontents['data'] as $contents)
			
					<div class="col-xs-12 col-sm-6 col-md-3" id="carddiv{{ $contents['tcl_id'] }}">
					<div class="card">
                    <div class="card-image">
                    	@if($contents['tcl_cover_image'] == "")
                    		@if($contents['tcl_type'] == "article" || $contents['tcl_type'] == "documents")
                    		<a class="docs-icon" href="javascript:Void(0)"></a>
                    		@else
                    		<a class="audio-icon" href="javascript:Void(0)"></a>
                    		@endif

                    		<img class="img-responsive" src="{{ url('public/adminlte/img') }}//plain-bg.jpg" alt="">
                    	@else
                    		@if($contents['tcl_type'] == "videos")
                    		<a class="video-icon" href="javascript:Void(0)"></a>
                    		@endif
                    		<img class="img-responsive" src="{{ url('public/img/contentlibrary/articles/256X256') }}/{{$contents['tcl_cover_image_small'] }}" alt="">
                    	@endif
                      
					  <div class="overlay">
					   <label class="switch switch-primary">
					   	@if($contents['tcl_is_active'] == 1)
                        <input class="switch-input idactive" type="checkbox" id="check_switch_{{ $contents['tcl_id'] }}" onChange="updateIsActiveContent({{ $contents['tcl_id'] }})" checked="checked">
                        @else
                        <input class="switch-input idactive"  id="check_switch_{{ $contents['tcl_id'] }}" onChange="updateIsActiveContent({{ $contents['tcl_id'] }})" type="checkbox">
                        @endif
                        <span class="switch-track"></span>
                        <span class="switch-thumb"></span>
                      </label>
					   <a href="#" id="addtof" onClick="addToFavourite({{ $contents['tcl_id'] }},{{$contents['tcl_is_favourite']}})">
					   	@if($contents['tcl_is_favourite'] == 1)
					   	<i class="icon icon-heart" title="Favourite"></i>
					   	@else
					   	<i class="icon icon-heart-o" title="Favourite"></i>
					   	@endif
					   </a>
					   @if($contents['tcl_type'] == "article")
					   <a href="#" onClick="addarticle({{ $contents['tcl_id'] }})"><i class="icon icon-pencil" title="Edit"></i></a>
					   @elseif($contents['tcl_type'] == "documents")
					   <a href="#" onClick="adddocument({{ $contents['tcl_id'] }})"><i class="icon icon-pencil" title="Edit"></i></a>
					   @elseif($contents['tcl_type'] == "videos")
					   <a href="#" onClick="addav({{ $contents['tcl_id'] }})"><i class="icon icon-pencil" title="Edit"></i></a>
					   @elseif($contents['tcl_type'] == "webinar")
					   <a href="#" onClick="addwebinar({{ $contents['tcl_id'] }})"><i class="icon icon-pencil" title="Edit"></i></a>
					   @endif
                       <a href="#" onClick="deleteContent({{ $contents['tcl_id'] }})"><i class="icon icon-trash" title="Delete"></i></a>
                      </div>
                    </div>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a class="link-muted" href="#">{{ $contents['tcl_title'] }}</a>
                      </h4>
                      <small>{!! $contents['tcl_description'] !!}</small>
                    </div>
                    
                    </div>
					</div>
					
					@endforeach	
					<div class="col-xs-12 col-sm-12 col-md-12">
					</div>
					<input type="hidden" class="tab_name">
					<div class="col-xs-12 col-sm-6 col-md-6">
							    <div class="show-count">
								<label>
						         Show 
								 <select id="pagination_drop" class="input-sm pagination_count">
								 @php 
								 $TotalRecords= $getcontents['total'];
								 for($i=0;$i<=$TotalRecords;$i++){

								 if(($i % 4)  == 0 &&  $i!= 0){ @endphp 
									<option value="{{ $i }}">{{ $i }}</option>
                                  @php }} @endphp
                                 </select>
								 entries of <span>{{ $TotalRecords }}</span>
								</label>
								 </div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6">

						<div id="showpaginate" class="pull-right">{{ $arti->links() }}</div>
					</div>
					