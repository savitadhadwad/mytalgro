@extends('layouts.app')

@section('content')
        <div class="title-bar clear">
            <div class="col-xs-12 col-sm-8 col-md-8">
            <h1 class="title-bar-title">
                <i class="icon icon-file-text"></i>
                <span class="d-ib">@lang('global.roles.title')</span>
            </h1>
            <p class="title-bar-description">
              <small>Edit Roles</small>
            </p>
            </div>
            
            
        </div>

  <div class="layout-content-body">
   
    {!! Form::model($role, ['method' => 'PUT', 'route' => ['admin.roles.update', $role->id]]) !!}

    <div class="panel panel-default">
<!--         <div class="panel-heading">
            @lang('global.app_edit')
        </div> -->

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    {!! Form::hidden('roleid', $role->id,['id'=>'roleid']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('permission', 'Permissions*', ['class' => 'control-label']) !!}
 <!--                    {!! Form::select('permission[]', $permissions, old('permission') ? old('permission') : $role->permission->pluck('id')->toArray(), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!} -->
 <table class="table table-bordered table-striped">
                         <tr>
                            <th>{{ Form::checkbox('selectall',"selectall","", array('id'=>'selectall')) }}</th>
                             <th>Permissions</th>
                             <th>Access</th>
                             <th>Create</th>
                             <th>Edit</th>
                             <th>View</th>
                             <th>Delete</th>
                         </tr>  
                         @php $title = "";$i=0;$j=0;$check_id=0; @endphp
                         <tr>
                         @foreach($permissions as $permission)
                            
                         @php 
                            if($permission['parent_id'] == 0){
                                $permission['parent_id'] = "parent_".$permission['moduleid'];
                            }else{
                                $permission['parent_id'] = "child_".$permission['parent_id'];
                            }

                            if($check_id == 0 || $check_id == ""){
                             $check_id = $permission['header_id'];
                            }

                            if($permission['header_id'] == $check_id || $check_id == 0){

                            if($i==0){
                            @endphp
                            <td>{{ Form::checkbox($permission['parent_id'],"selectall","", array('class'=>'rowcheckall')) }}</td>
                                <td>{{ $permission['header_name'] }}</td>
                            @php } @endphp
                            <td>{{ Form::checkbox('permission[]',$permission['id'],"", array('id'=>$permission['id'],'class'=>'checkbox')) }}</td>
                            @php
                            $check_id = $permission['header_id'];
                            $i++;
                            }else{
                                $check_id = "";
                              
                                 @endphp
                                </tr>
                                @php 
                               @endphp
                               <td>{{ Form::checkbox($permission['parent_id'],"selectall","", array('class'=>'rowcheckall')) }}</td>
                               <td>{{ $permission['header_name'] }}</td>
                                 <td>{{ Form::checkbox('permission[]',$permission['id'],"", array('id'=>$permission['id'],'class'=>'checkbox')) }}</td>
                                @php
                                $check_id = $permission['header_id'];
                            }

                         @endphp
                         @endforeach 
                     </tr>

                    </table>
                    <p class="help-block"></p>
                    @if($errors->has('permission'))
                        <p class="help-block">
                            {{ $errors->first('permission') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
</div>
@stop

