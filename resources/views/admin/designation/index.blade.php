@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')


  <div class="title-bar clear">
      <div class="col-xs-12 col-xs-8 col-md-8">
        <h1 class="title-bar-title">
            <i class="icon icon-list-ul"></i>
            <span class="d-ib">{{Config::get('messages.designation.DESIGNATION_LIST')}}</span>
        </h1>
        <p class="title-bar-description">
          <small>Personalize my Dashboard</small>
        </p>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 ad_top">
        <button class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
        <a href="#designation_model" data-toggle="modal"  class="btn btn-primary pull-right ">Add Designation</a>
        <a href="#upload_designation_modal"  data-toggle="modal"  class="btn btn-primary pull-right">Bulk Upload</a>
      </div>
  </div>


  <div class="layout-content-body filter-bg nopadding">
      <div class="row gutter-xs">
          <div class="col-xs-12">
              <div class="Cus_card collapsed ">
                  <div class="Cus_card-header">
                      <div class="Cus_card-actions">
                          <button type="button" class="Cus_card-actions Cus_card-toggler"></button>
                      </div>
                  </div>
                  <div class="Cus_card-body collapse">
                      <div class="col-xs-6 col-md-3">
                          <h5>Designation Code</h5>
                          <select id="dropdown_tdm_desination" name="dropdown_tdm_desination" class="form-control">
                              <option value="">Select Designation Code</option>
                              @foreach ($array_code_data as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_designation_code']}}</option> 
                              @endforeach
                          </select>                
                      </div>
                      <div class="col-xs-12 col-md-12">
                          <h5>Search by Designation</h5>
                          <input class="form-control" type="text" id="searchtxt" name="searchtxt">                    
                      </div>
                      <div class="col-xs-12 col-md-4">
                      <h5></h5>
                      <button class="btn btn-primary" type="submit" id="designation_searchButton">Search</button>
                      <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
        
  <div class="layout-content-body">
    <div class="row gutter-xs">
      <div class="col-xs-12">
        <div class="card">
            <div class="card-body">
              <?php
              $default_col_order_string="CheckAll-1,DesignationCode-1,Designation-1,Action-1";
              ?>
              <div class="table-content-header">
                <div class="table-toolbar">
                  <div class="table-toolbar-tools pull-sm-left hide_tab">
                   <div class="btn-group bulk hide common_action">
                      <label>Bulk Action :</label>
                      <button class="btn btn-link link-muted click_common_action" to_do="enable" type="button"><span>Enable</span></button> 
                      <span class="pipe">|</span>
                      <button class="btn btn-link link-muted click_common_action"  to_do="disable" type="button"><span>Disable</span></button>
                      <span class="pipe">|</span>
                      <button class="btn btn-link link-muted click_common_action"  to_do="delete" type="button"><span>Delete</span></button>
                    </div>
                  </div>
                  <div class="table-toolbar-tools pull-lg-right">
                    <div class="btn-group export">
                      <a href="{{url('/admin/designation_excel')}}" type="button" class="btn btn-link link-muted">
                        <span class="icon icon-file-excel-o icon-lg icon-fw"></span>
                        <span class="visible-lg-inline">Export Excel</span>
                      </a>
                      <a href="{{url('/admin/designation_pdf')}}" type="button" class="btn btn-link link-muted">
                        <span class="icon icon-file-pdf-o icon-lg icon-fw"></span>
                        <span class="visible-lg-inline">Export PDF</span>
                      </a>
                      <div class="filter-opt1">
                        <button class="btn btn-link link-muted col" type="button">
                          <span class="icon icon-cogs icon-lg icon-fw"></span>
                          <span class="visible-lg-inline">Column</span>
                        </button>
                        <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                                <?php
                                //if session has value display column according or display default array
                                if($request->session()->has('designation_hidden_column_array')){
                                  $col_arr_value = $request->session()->get('designation_hidden_column_array');
                                }else{
                                  $col_arr_value=$default_col_order_string;
                                }
                                $col_arr_value=explode(",",$col_arr_value);

                                foreach ($col_arr_value as $key => $value) {
                                    //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                    $value=explode('-', $value);
                                    $class="";
                                    if($value[0]=='CheckAll' || $value[0]=='Action')
                                      $class='non_sortable';
                                    ?>
                                    <li class="list-group-item list-order-item <?=$class?>" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                      <span class="pull-right">
                                        <label class="switch switch-primary">
                                          <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                          <span class="switch-track"></span>
                                          <span class="switch-thumb"></span>
                                        </label>
                                      </span>
                                      <span class="icon icon-arrows"></span>
                                      <?php
                                      if ($value[0]=="CheckAll")
                                        echo "Check All";
                                      else if($value[0]=="DesignationCode")
                                        echo "Designation Code";
                                      else
                                        echo $value[0];
                                      ?>
                                      </li>
                                    <?php
                                  }
                                ?>
                              <button class="btn btn-primary btn-half save_order" form_name="designation" id="" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
              <div class="cus-tbl">
                <table id="example1" class="table table-hover table-striped example" cellspacing="0" width="100%">
                  <tbody>
                  </tbody> 
                </table>
                <?php
                if($request->session()->has('designation_hidden_column_array')){
                    $designation_hidden_column_array = $request->session()->get('designation_hidden_column_array');?>
                    <input type="hidden" name="designation_hidden_column_array" id="designation_hidden_column_array" value="<?=$designation_hidden_column_array?>"><?php
                }else{?>
                    <input type="hidden" name="designation_hidden_column_array" id="designation_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                }
                ?>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div id="designation_model" class="modal pop-modal fade" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
           <h1 class="title-bar-title">
              <i class="icon icon-file-picture-o"></i>
              <span class="d-ib">{{ Config::get('messages.designation.DESIGNATION_LABEL') }}</span>
          </h1>
          <p class="title-bar-description">
            <small><label class="dynamic_add_edit_lable">Add</label> Designation</small>
          </p>
        </div>
            
        <div class="modal-body">
          <form action="#" method="post" enctype="multipart/form-data" id="Frmdesignation">
                <div class="row">
                  <div class="over-form hide" id="add_edit_sucess_msg">
                    <div class="alert alert-success">
                        <i class="icon icon-check-circle-o"></i>
                        <strong>Success!</strong> Designation insert/update successfully.
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label id="err_code_exist" class="error"></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 form-group">
                    <h5>Designation Code <span class="color-r">*</span></h5>
                    <input class="form-control" type="text" id="tdm_designation_code" name="tdm_designation_code">
                    <label id="err_tdm_designation_code" class="error"></label>
                  </div>

                  <div class="col-md-12 form-group">
                    <h5>Designation <span class="color-r">*</span></h5>
                    <input class="form-control" type="text" id="tdm_designation" name="tdm_designation">
                    <label id="err_tdm_designation" class="error"></label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 m-t-md">
                    <input type="hidden" name="hidden_tdm_id" id="hidden_tdm_id">
                  <button type="submit" class="btn btn-primary submit_edit_form">Submit</button>
                  <button type="button" class="btn btn-default cancel_edit_form">Cancel</button>
                  </div>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>



<div id="upload_designation_modal" class="modal pop-modal fade" tabindex="-1" aria-hidden="true" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">×</span>
          <span class="sr-only">Close</span>
        </button>
         <h1 class="title-bar-title">
            <i class="icon icon-file-picture-o"></i>
            <span class="d-ib">Bulk Upload</span>
        </h1>
        <p class="title-bar-description">
          <small>Add Designation</small>
        </p>
      </div>
          
      <div class="modal-body">
        <!-- error msg for blank file-->
        <div class="alert alert-danger hide" id="blank_error_msg"></div>
        <!--sucess msg-->
        <div class="over-form hide" id="upload_sucess_msg">
          <div class="alert alert-success">
              <i class="icon icon-check-circle-o"></i>
              <strong>Success!</strong> File Uploaded successfully.
          </div>
        </div>
        <!--error msg for wrong files-->
        <div class="alert alert-danger hide" id="all_upload_error_msg">
            <i class="icon icon-times-circle-o"></i><strong>Error!</strong> 
            <label id="failed_record"></label>
            <span>/</span>
            <label id="total_record"></label> files not uploaded.
              <ul class="error" id="failed_record_list">
              </ul>
        </div>
        
        <form action="#" method="post" enctype="multipart/form-data" id="Frmdesignationcsv">
          <div class="input-group">
              <span class="input-group-btn">
                <label class="btn btn-primary file-upload-btn">
                  <input class="file-upload-input" type="file" id="upload_designation" name="upload_designation">
                  Upload CSV File
                </label>
              </span>

              <div class="border p-0">
                <div class="progress progress-lg">
                  <div class="progress-bar  progress-bar-success myprogress" role="progressbar" style="width: 0%">
                    <span class="sr-only">0% Complete (success)</span>
                    <div class="progress-value">0%</div>
                  </div>
              </div>
             </div>
          </div>
          <div class="msg"></div>
          <p class="m-t-md">Create or Update many Designation in batch(<span class="color-o ">
            <a  href="{{url('/admin/download_sample_designation_excel')}}" class="">Download sample CSV file</a></span>)</p>
          <input type="button" name="submit" class="upload_designation_csv btn btn-primary" value="Upload">
        </form>
      </div>
    </div>
  </div>
</div>

<script src="{{url('public/adminlte/js/designation/DesignationController.js') }}"></script> 
@stop