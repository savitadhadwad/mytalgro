    @inject('request', 'Illuminate\Http\Request')
    @extends('layouts.app')

    @section('content')

        <div class="title-bar clear">
			<div class="col-xs-12 col-sm-7 col-md-8">
            <h1 class="title-bar-title">
				<i class="icon icon-file-text"></i>
                <span class="d-ib">Create Test</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
            </p>
			</div>
			
			
        </div>
		
        <div class="layout-content-body">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card no-border">
					 <div class="card-body">
						   <div class="row">
	                       <div class=" col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
							   <div class="test-sec">
								<div class="row">
								<div class="col-md-6 col-xs-6">
							    <a href="javascript:Void(0)" id="go_to_add_test" class="create-test">
								<span class="icon icon-plus-circle t-i"></span>
								</a>
								<div class="test-title">Create New Test</div>
								</div>
								<div class="col-md-6 col-xs-6">
							    <a href="#" class="create-test" data-toggle="tooltip" data-placement="top" title="Will Take to Test List.
																												  The details will get populated here which can be changed.">
								<span class="far fa-copy t-i"></span>
								</a>
								<div class="test-title">Copy Test</div>
								</div>
								</div>
							   </div>
						   </div>
						</div>
					 </div>
                    </div>
                </div>
            </div>
        </div>
 <script src="{{ url('public/adminlte/js/test') }}/add_test.js"></script>       
    @stop