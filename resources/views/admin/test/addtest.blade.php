    @inject('request', 'Illuminate\Http\Request')
    @extends('layouts.app')

    @section('content')

	<div class="title-bar clear">
		<div class="col-xs-12 col-sm-8 col-md-8">
        <h1 class="title-bar-title">
			<i class="icon icon-file-text"></i>
            <span class="d-ib">Assessment <i class="icon icon-angle-right"></i> Test Details</span>
        </h1>
        <p class="title-bar-description">
          <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
        </p>
		</div>
  </div>
       
  <div class="layout-content-body">
    <div class="row gutter-xs">
      <div class="col-xs-12">
  			<div class="demo-form-wrapper">
    			<div class="panel">
            <div class="select-tp">
      			  <h5>Select test type</h5>
                    <label class="custom-control custom-control-primary custom-radio">
                    <input class="custom-control-input" name="primary" type="radio" value="checked" checked="checked">
                    <span class="custom-control-indicator"></span>Internal
                  </label>
                    <label class="custom-control custom-control-primary custom-radio">
                    <input class="custom-control-input" name="primary-disabled" type="radio" value="checked">
                    <span class="custom-control-indicator"></span>External
                  </label>
    	       </div>
    				
            <ul class="steps">
              <li class="step col-md-1 active">
                <a class="step-segment" href="#tab-1" data-toggle="tab">
                  <span class="step-icon">1</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Basic Information</strong>
                </div>
              </li>
              <li class="step col-md-1">
                <a class="step-segment" href="#tab-2" data-toggle="tab">
                  <span class="step-icon">2</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Add Questions</strong>
                </div>
              </li>
              <li class="step col-md-1">
                <a class="step-segment" href="#tab-3" data-toggle="tab">
                  <span class="step-icon">3</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Test Settings</strong>
                </div>
              </li>
      				<li class="step col-md-1">
                <a class="step-segment" href="#tab-4" data-toggle="tab">
                  <span class="step-icon">4</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">learning Path</strong>
                </div>
              </li>
      				<li class="step col-md-1">
                <a class="step-segment" href="#tab-5" data-toggle="tab">
                  <span class="step-icon">5</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Assign Learners</strong>
                </div>
              </li>
      				<li class="step col-md-1">
                <a class="step-segment" href="#tab-6" data-toggle="tab">
                  <span class="step-icon">6</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Invite Learners</strong>
                </div>
              </li>
      				<li class="step col-md-1">
                <a class="step-segment" href="#tab-7" data-toggle="tab">
                  <span class="step-icon">7</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Results</strong>
                </div>
              </li>
      				<li class="step col-md-1">
                <a class="step-segment" href="#tab-8" data-toggle="tab">
                  <span class="step-icon">8</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Set Learning Path based on test results </strong>
                </div>
              </li>
            </ul>
            <div class="tab-menu">
      			 <ul class="nav nav-tabs m-t-lg">
        			  <li class="active"><a href="#tab-1" data-toggle="tab">Test Details</a></li>
                      <li><a href="#tab-2" data-toggle="tab">Add Questions</a></li>
                      <li><a href="#tab-3" data-toggle="tab">Settings</a></li>
        			  <li><a href="#tab-4" data-toggle="tab">Create Learning Path</a></li>
        			  <li><a href="#tab-5" data-toggle="tab">Assign Learners</a></li>
        			  <li><a href="#tab-6" data-toggle="tab">Invite Learners</a></li>
        			  <li><a href="#tab-7" data-toggle="tab">Results</a></li>
        			  <li><a href="#tab-8" data-toggle="tab">Assign learning path</a></li>
      			 </ul>
            </div>
            <div class="tab-content">
              <div class="tab-pane fade  active in" id="tab-1">
    		        <div class="row">
    		       	    {{Form::open(array('id'=>'add_test_form','method'=>'post','enctype'=>'multipart/form-data'))}}
    	            <div class="col-xs-12 col-sm-6  col-md-6 col-lg-3">  
                      <h5>Test Name</h5>
    						      <div class="cus-select">
    						       {!! Form::text('test_name', '', ['class' => 'form-control','id'=>'test_title_id', 'placeholder' => '', 'required' => '']) !!}	
    						      </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <h5>Test Function</h5>
      							<div class="cus-select">
                      <select class="ms" id="test_func_id" multiple="multiple">
                        @foreach ($testFunctionList as $testFunction)
                            <option value="{{$testFunction['tfm_id']}}"> 
                              {{$testFunction['tfm_function']}}
                            </option> 
                        @endforeach
                       </select>
      							</div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
      							<div class="row">
        							<div class="col-xs-6 col-md-6">
                          <h5>Start Date</h5>
        							    <div class="input-with-icon">
                            <input class="form-control" style="padding-left: 1px;" type="text" id="start_date_id" data-provide="datepicker" data-date-today-btn="linked">
                            <span class="icon icon-calendar input-icon"></span>
                          </div>
                      </div>
        							<div class="col-xs-6 col-md-6">
                        <h5>End Date</h5>
                        <div class="input-with-icon">
                          <input class="form-control" style="padding-left: 1px;" type="text" id="end_date_id" data-provide="datepicker" data-date-today-btn="linked">
                          <span class="icon icon-calendar input-icon"></span>
                        </div>
        							</div>
      							</div>
                  </div>
    		          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">  
                    <h5>Test Duration</h5>
    		            <div class="">
    		        	    <input id="test_duration_id" class="form-control" type="text">
                        <!-- <input id="demo-timepicker-1" class="form-control" type="text">
                        <span class="icon icon-clock-o input-icon"></span> -->
                    </div>
    		          </div>
    		          <div class="col-xs-12 col-md-12 col-lg-12">  
                    <h5>Description</h5>
    		            <!-- <textarea class="form-control" rows="5"></textarea> -->
    		            {{Form::textarea('test_description','',array('class'=>'form-control ckeditor','id'=>'test_description','rows' => 5))}}
    		          </div>
    				      <div class="col-xs-12 col-md-6 col-lg-4 sec-parameter"> 
    				        <div class="parameter-title clearfix">
    				          <div class="col-xs-7 col-md-7 nopadding">
    				            <h5>Parameters</h5> 
    				          </div>
          						<div class="col-xs-5 col-md-5 nopadding">
          						  <h5>Weightage</h5> 
          						</div>
    				        </div>
    				        <div id="parameter_div" class="parameter-full m-b-sm clearfix">
          						<div class="col-xs-6 col-md-7 nopadding">
          					    <input class="form-control" type="text">
          						</div>
          						<div class="col-xs-5 col-md-4 nopadding">
          						  <div class="input-group">
                          <span class="input-group-btn">
                          <button type="button" class="btn btn-gray btn-number" data-type="minus" data-field="quant[1]">
                          <span class="icon icon-minus-circle"></span>
                          </button>
                          </span>
                          <input type="text" name="quant[1]" style="width: 50px;" class="form-control text-center input-number" value="10" min="1" max="100">
                          <span class="input-group-btn">
                          <button type="button" class="btn btn-gray btn-number" data-type="plus" data-field="quant[1]">
                          <span class="icon icon-plus-circle"></span>
                          </button>
                          </span>
                        </div>
          						</div>
    					        <div class="col-xs-1 col-md-1 nopadding text-center" style="padding-left: 28px;">
    				           <a href="#" class="ad-minus"><i class="icon icon-minus-circle"></i></a>
    			            </div>
    				        </div>
    				        <div class="clearfix appenddiv"></div>
          						<div class="col-md-12 nopadding">
          						<a href="javascript:Void(0)" class="add-parameter"><i class="icon icon-plus"></i> add another parameter</a>
          						</div>
        						<div class="col-xs-11 col-md-11 nopadding m-t-md">
          						<div class="total-parameter">
          						<label>Total Weightage:<span>100%</span></label>
          						</div>
        						</div>
            	    </div> 
    			        <div class="col-sm-12 col-md-12 m-t-md text-right">
    		            <button type="button" id="save_test_btn" class="btn btn-primary">Save</button>
                    <button type="button" id="save_add_que_btn" class="btn btn-primary">Save &amp; Add Question</button>
    		          </div>
            		  {{Form::close()}}
                </div>
              </div>
              <div class="tab-pane fade" id="tab-2">
    		        <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
              </div>
              <div class="tab-pane fade" id="tab-3">
                <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
              </div>
      			  <div class="tab-pane fade" id="tab-4">
                <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
              </div>
      			  
              <div class="tab-pane fade" id="tab-5">
                <?php
                  $default_col_order_string="CheckAll-1,EmployeeCode-1,UserName-1,EmailId-1,Department-1,ContactDetail-1,ManagerName-1,CourseStatus-1,Level-1,EnrollCourses-1";
                                  ?>
                <div class="table-content-header">
                  <div class="table-toolbar">
                    <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                      
                    </div>
                    <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                      <div class="btn-group export">
                        
                        
                        <div class="filter-opt1">
                          <button class="btn btn-link col" type="button">
                            <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                            <span class="hidden-xs">Column</span>
                          </button>
                          <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('learner_hidden_column_array')){
                                $col_arr_value = $request->session()->get('learner_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);

                              foreach ($col_arr_value as $key => $value) {
                                  //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?> " data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll"){
                                      echo "Check All";
                                    }elseif ($value[0]=="EmployeeCode") {
                                      echo "Employee Code";
                                    }elseif ($value[0]=="EnrollCourses") {
                                     echo "Enroll Courses";
                                    }else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  <?php
                                }
                              ?>
                              
                              <button class="btn btn-primary btn-half save_order" form_name="test_learner" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                          </div>
                        </div>
                      </div>
                      
                      <div class="btn-group">
                        <a href="#" class="refine_mbutton" title="Filter"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="table-content-body"> 
                  <div class="Cus_card filter-bg">
                    <div class="Cus_card-body" style="display: none;">
                      <div class="col-xs-6 col-md-3">
                        <h5>Department</h5>
                        <div class="cus-select">
                          <select class="learner_department">
                              <option value="">Select Department</option>
                              @foreach ($array_department as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Level</h5>
                        <div class="cus-select">
                          <select class="learner_level">
                            <option value="">Select Level</option>
                              @foreach ($array_level as $code)
                                 <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Function</h5>
                        <div class="cus-select">
                         <select class="learner_function">
                            <option value="">Select Function</option>
                              @foreach ($array_function as $code)
                                 <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                              @endforeach
                         </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Status</h5>
                        <div class="cus-select">
                          <select class="learner_status">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-9">
                        <h5>Search</h5>
                        <input class="form-control" type="text" id="searchlearnertxt">
                      </div>
                      <div class="col-xs-6 col-md-3">
                        
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <h5></h5>
                        <button class="btn btn-primary" id="test_learner_searchButton" type="submit">Search</button>
                        <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                    </div>
                  </div>
                  <table class="table table-hover table-striped test_learner" cellspacing="0" width="100%">
                    
                    <tbody>
                    </tbody>
                  </table>
                    <div class="col-sm-12 col-md-12 m-t-md text-right">
                      <button type="button" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-primary assign_learner_to_test">Assign Learners</button>
                    </div>
                </div>
              </div>
      			 <div class="tab-pane fade" id="tab-6">
                <?php
                  $default_col_order_string="CheckAll-1,EmployeeCode-1,UserName-1,EmailId-1,Department-1,ContactDetail-1,ManagerName-1,CourseStatus-1,Level-1,EnrollCourses-1";
                                  ?>
                <div class="table-content-header">
                  <div class="table-toolbar">
                    <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                      
                    </div>
                    <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                      <div class="btn-group export">
                        
                        
                        <div class="filter-opt1">
                          <button class="btn btn-link col" type="button">
                            <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                            <span class="hidden-xs">Column</span>
                          </button>
                          <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('learner_hidden_column_array')){
                                $col_arr_value = $request->session()->get('learner_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);

                              foreach ($col_arr_value as $key => $value) {
                                  //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?> " data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll"){
                                      echo "Check All";
                                    }elseif ($value[0]=="EmployeeCode") {
                                      echo "Employee Code";
                                    }elseif ($value[0]=="EnrollCourses") {
                                     echo "Enroll Courses";
                                    }else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  <?php
                                }
                              ?>
                              
                              <button class="btn btn-primary btn-half save_order" form_name="test_learner" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                          </div>
                        </div>
                      </div>
                     
                      <div class="btn-group">
                        <a href="#" class="refine_mbutton" title="Filter"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="table-content-body"> 
                  <div class="Cus_card filter-bg">
                    <!-- <div class="Cus_card-body" style="display: none;">
                      <div class="col-xs-6 col-md-3">
                        <h5>Department</h5>
                        <div class="cus-select">
                          <select class="learner_department">
                              <option value="">Select Department</option>
                              @foreach ($array_department as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Level</h5>
                        <div class="cus-select">
                          <select class="learner_level">
                            <option value="">Select Level</option>
                              @foreach ($array_level as $code)
                                 <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Function</h5>
                        <div class="cus-select">
                         <select class="learner_function">
                            <option value="">Select Function</option>
                              @foreach ($array_function as $code)
                                 <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                              @endforeach
                         </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Status</h5>
                        <div class="cus-select">
                          <select class="learner_status">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-9">
                        <h5>Search</h5>
                        <input class="form-control" type="text" id="searchlearnertxt">
                      </div>
                      <div class="col-xs-6 col-md-3">
                        
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <h5></h5>
                        <button class="btn btn-primary" id="test_learner_searchButton" type="submit">Search</button>
                        <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                    </div> -->
                  </div>
                  <table class="table table-hover table-striped invite_test_learner" cellspacing="0" width="100%">
                    
                    <tbody>
                    </tbody>
                  </table>
                    <div class="col-sm-12 col-md-12 m-t-md text-right">
                      <button type="button" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-primary invite_learner_to_test">Invite Learners</button>
                    </div>
                </div>
              </div>
      			  <div class="tab-pane fade" id="tab-7">
                <p>Raw denim you probably haven't heard of them jean shorts Austin. </p>
              </div>
      			  <div class="tab-pane fade" id="tab-8">
                <p>Raw denim you probably haven't heard of them jean shorts Austin. </p>
              </div>
    			  </div>
    			</div>
        </div>
      </div>
    </div>
  </div>
 <script src="{{ url('public/adminlte/js/test') }}/add_test.js"></script> 
    @stop