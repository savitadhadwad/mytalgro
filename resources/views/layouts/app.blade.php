<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials.head')
  </head>
  <body class="layout layout-header-fixed">
    @include('partials.topbar')
      <div class="layout-main">
            @include('partials.sidebar')
            <div id="" class="layout-content">
                  
                  @yield('content')
                  
               
            </div>

    
            @include('partials.footer')
            <!-- </div> -->

            @include('partials.javascripts')
            
    </div>
  </body>
</html>