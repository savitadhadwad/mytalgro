@extends('layouts.app')

@section('content')
<!--     @if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif -->
<div class="title-bar clear">
			<div class="col-xs-12 col-sm-8 col-md-8">
            <h1 class="title-bar-title">
				<i class="icon icon-list-ul"></i>
                <span class="d-ib">Profile Details</span>
            </h1>
            <p class="title-bar-description">
              <small>Update your profile</small>
            </p>
			</div>
			
</div>
<div class="layout-content-body">
                    @if (Session::has('message'))
                        <div class="note note-info">
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        </div>
                    @endif
<div class="row">

	<div class="col-xs-12">
		<div class="col-md-12" style="border:none;">
			<div class="panel" style="margin-bottom: 20px;background-color: #fff;border: 1px solid transparent;border-radius: 4px;">
				<div class="panel-body" style="padding: 15px;">
					<div class="col-md-12 pro_pics" style="margin-left: 43%;">							
								 <form class="form" id="photo_form" method="post" enctype="multipart/form-data" autocomplete="off" action="{{ url('/uploadimg') }}">
								 	<input type="hidden" value="{{ csrf_token() }}" name="_token">
								 <!-- <input name="action" type="hidden" value="upload_photo">
								 <input type="hidden" name="cluser_user_id" value="150"> -->
								 	 <label class="upload_files">
								 	 	
								 	<img src="{{ url('public/img') }}/{{auth()->user()->profile_image}}" id="proimg"><span class="fa fa-camera" style="position: absolute;top: 110px;left: 125px;color: #f5efee;background-color: #f90606;height: 20px;width: 20px;padding-left: 3px;padding-top: 3px;z-index: 100;"></span>
             <input type="file" name="photo" id="photo" accept="image/jpeg,image/png,image/gif" style="display: none;">	</label>
             				@if ($errors->has('photo'))
	            	<span class="help-block">
	                	<strong>{{ $errors->first('photo') }}</strong>
	            	</span>
	        	@endif
              </form>
			</div>
<br><br>
<br><br>
<div class="editprofile">
@php 
$name = explode(" ",auth()->user()->name);
$fname = $name[0];
if(isset($name[1]))
	$lname = $name[1];
else
	$lname="";


$title = explode("(",$getdata[0]['title']);
$title = $title[0];
@endphp

	{{Form::open(array('id'=>'profile_form','method'=>'post','url'=>'/updateProfile'))}}
	<input type="hidden" value="{{ csrf_token() }}" name="_token">
			<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Employee Code',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('empcode')?'has-error':''}}">{{Form::text('empcode',$value=  auth()->user()->employee_code,array('class'=>'form-control','id'=>'empcode','readonly'=>'readonly'))}}@if ($errors->any())<small class="text-danger">{{ $errors->first('empcode') }}</small>@endif</div>
		</div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Function',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('funcion')?'has-error':''}}">{{Form::text('function',$value=  $title,array('class'=>'form-control','id'=>'function','readonly'=>'readonly'))}}@if ($errors->any())<small class="text-danger">{{ $errors->first('function') }}</small>@endif</div>
		</div>
		<div class="form-group" style="height: 25px;"></div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('First Name*',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('fproname')?'has-error':''}}">{{Form::text('fproname',$value=  $fname,array('class'=>'form-control','id'=>'fproname'))}}@if ($errors->any())<small class="text-danger">{{ $errors->first('fproname') }}</small>@endif</div>
		</div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Last Name*',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('lproname')?'has-error':''}}">{{Form::text('lproname',$value= $lname,array('class'=>'form-control','id'=>'lproname'))}}@if ($errors->any())<small class="text-danger">{{ $errors->first('lproname') }}</small>@endif</div>
		</div>
		<div class="form-group" style="height: 25px;"></div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Username*',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('prousername')?'has-error':''}}">{{Form::email('prousername',$value= auth()->user()->email,array('class'=>'form-control','id'=>'prousername'))}}
				@if ($errors->any())<small class="text-danger">{{ $errors->first('prousername') }}</small>@endif
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Gender*',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('gender')?'has-error':''}}"> {!! Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), auth()->user()->gender, array('class' => 'form-control')) !!}@if ($errors->any())<small class="text-danger">{{ $errors->first('gender') }}</small>@endif</div>
		</div>
		<div class="form-group" style="height: 25px;"></div>
				<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Mobile Number',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('mobileno')?'has-error':''}}">{{Form::text('mobileno',$value= auth()->user()->mobile_no,array('class'=>'form-control','id'=>'mobileno'))}}
				@if ($errors->any())<small class="text-danger">{{ $errors->first('mobileno') }}</small>@endif
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Website URL',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('websiteurl')?'has-error':''}}">{{Form::text('websiteurl',$value= auth()->user()->website_url,array('class'=>'form-control','id'=>'websiteurl'))}}
				@if ($errors->any())<small class="text-danger">{{ $errors->first('websiteurl') }}</small>@endif
			</div>
		</div>
		<div class="form-group" style="height: 25px;"></div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Country',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('country')?'has-error':''}}">
				<select class="form-control" id="country" name="country">
				@foreach($countryList as $country)
				<option value="{{$country['id']}}" {{ ($country['id'] ==  auth()->user()->country) ? ' selected="selected"' : '' }} >{{$country['country_name']}}</option>
				@endforeach
				</select>
				@if ($errors->any())<small class="text-danger">{{ $errors->first('country') }}</small>@endif
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('State',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('state')?'has-error':''}}">
				<select class="form-control" id="stateselect" name="state">
				@foreach($stateList as $state)
				<option value="{{$state['id']}}" {{ ($state['id'] ==  auth()->user()->state) ? ' selected="selected"' : '' }} >{{$state['state_name']}}</option>
				@endforeach
				</select>
				@if ($errors->any())<small class="text-danger">{{ $errors->first('state') }}</small>@endif
			</div>
		</div>
		<div class="form-group" style="height: 25px;"></div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('City',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('city')?'has-error':''}}">{{Form::text('city',$value= auth()->user()->city,array('class'=>'form-control','id'=>'city'))}}
				@if ($errors->any())<small class="text-danger">{{ $errors->first('city') }}</small>@endif
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Pincode',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('pincode')?'has-error':''}}">{{Form::text('pincode',$value= auth()->user()->pincode,array('class'=>'form-control','id'=>'pincode'))}}
				@if ($errors->any())<small class="text-danger">{{ $errors->first('pincode') }}</small>@endif
			</div>
		</div>
		<div class="form-group" style="height: 25px;"></div>
			<div class="form-group">
			<div class="col-md-2">
				{{Form::label('Address',$value=null,array("class"=>"pull-right"))}}	
			</div>
			<div class="col-md-4 {{$errors->has('address')?'has-error':''}}">{{Form::textarea('address',$value= auth()->user()->address,array('class'=>'form-control','id'=>'address','rows' => 2, 'cols' => 40))}}
				@if ($errors->any())<small class="text-danger">{{ $errors->first('address') }}</small>@endif
			</div>
		</div>
		<div class="form-group" style="height: 50px;"></div>
		<div class="form-group">
			<div class="col-md-5">
					
			</div>
			<div class="col-md-3">{{Form::submit('Update',array('class'=>'form-control btn btn-primary','id'=>'profile_update')) }}</div>
<!-- 			<div class="col-md-3">
				{{Form::button('Cancel',array('class'=>'form-control btn btn-default','id'=>'profile_cancel')) }}
			</div> -->
		</div>
	{{Form::close()}}
</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@stop