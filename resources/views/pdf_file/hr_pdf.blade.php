<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered" style="table-layout: fixed">
		<thead>
			<tr style="word-wrap: break-word">
				<th>EMPCode</th>
				<th>Name</th>
				<th>Email</th>
				<th>Mobile</th>
				<th>Department</th>
				<th>Function</th>
				<th>Level</th>
				<th>Manager</th>
				<th>Courses</th>
				<th>Course Enrolled</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($users as $key => $value){?>
			<tr>
				<td>{{ $value->employee_code}}</td>
				<td>{{ $value->user_firstname }} {{$value->user_lastname}}</td>
				<td>{{ $value->email }}</td>
				<td>{{ $value->mobile_no }}</td>
				<td>{{ $value->tdm_department }}</td>
				<td>{{ $value->tfm_function }}</td>
				<td>{{ $value->tsl_seniority }}</td>
				<td>{{ $value->manager_name }}</td>
				<td>{{ $value->mobile_no }}</td>
				<?php
				$course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_learner_course_releation tlcr on u.id=tlcr.tlcr_learner_id
                    inner join tbl_courses tc on tc.tc_id=tlcr.tlcr_course_id 
                    where tlcr_learner_id=".$value->id." group by tc_title") );
                    $course=json_decode(json_encode($course),true);
                    if($course){
                       	$CourseEnrolled=$course[0]['course_title'];
                    }else{
                        $CourseEnrolled="No Course Enrolled";
                    }

                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                    from users u inner join tbl_hr_course_relation thcr on u.id=thcr.thcr_hr_id
                    inner join tbl_courses tc on tc.tc_id=thcr.thcr_course_id 
                    where thcr_hr_id=".$value->id." group by tc_title") );
                    $course=json_decode(json_encode($course),true);
                    if($course){
                       	$Courses=$course[0]['course_title'];
                    }else{
                        $Courses="No Course Enrolled";
                    }
				?>
				<td>{{ $Courses }}</td>
				<td>{{ $CourseEnrolled}}</td>
			</tr>
		<?php }?>
			
		</tbody>
	</table>
</body>
<html>