<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered" style="table-layout: fixed">
		<thead>
			<tr>
				<th>Test Name</th>
				<th>Parameters</th>
				<th>Associated Courses</th>
				<th>Test Function</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($test as $key => $value)
			<tr>
				<td>{{ $value->tte_test_name }}</td>
				<td>{{ $value->tte_parameters }}</td>
				<td>{{ $value->tc_title}}</td>
				<td>{{ $value->tte_test_function }}</td>
				<td>{{ $value->tte_start_date }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
<html>