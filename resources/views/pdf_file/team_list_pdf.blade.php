<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Team</th>
				<th>Learners</th>
				<th>Course Assigned</th>
				<th>Learning Path Assigned</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($learners as $key => $value)
			<tr>
				<td>{{ $value['tt_title'] }}</td>
				<td>{{ $value['learners'] }}</td>
				<td>{{ $value['course_assigned'] }}</td>
				<td>{{ $value['learning_path'] }}</td>												
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
<html>