@extends('layouts.app')

@section('content')
        <div class="title-bar clear">
            <div class="col-xs-12 col-sm-8 col-md-8">
            <h1 class="title-bar-title">
                <i class="icon icon-file-text"></i>
                <span class="d-ib">Change password</span>
            </h1>
<!--             <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
            </p> -->
            </div>
            
            
        </div>

 <div class="layout-content-body">
                    @if (Session::has('message'))
                        <div class="note note-info">
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        </div>
                    @endif

    @if(session('success'))
        <!-- If password successfully show message -->
        <div class="row">
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        </div>
    @else
        {!! Form::open(['method' => 'PATCH', 'route' => ['auth.change_password']]) !!}
        <!-- If no success message in flash session show change password form  -->
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('global.app_edit')
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('current_password', 'Current password*', ['class' => 'control-label']) !!}
                        {!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('current_password'))
                            <small style="color: red;">
                                {{ $errors->first('current_password') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('new_password', 'New password*', ['class' => 'control-label']) !!}
                        {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('new_password'))
                            <small style="color: red;">
                                {{ $errors->first('new_password') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('new_password_confirmation', 'New password confirmation*', ['class' => 'control-label']) !!}
                        {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('new_password_confirmation'))
                            <small style="color: red;">
                                {{ $errors->first('new_password_confirmation') }}
                            </small>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        {!! Form::submit(trans('global.app_save'), ['class' => 'form-control btn btn-primary','style'=>'width: 100px;']) !!}
        {!! Form::close() !!}
    @endif

</div>    
@stop

