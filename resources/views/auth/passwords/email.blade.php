

<!DOCTYPE html>
<html lang="en">
   <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TalGro</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
   <link href="{{ asset('public/css/login/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/login/TalGro.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/login/login.min.css') }}" rel="stylesheet">
</head>
  
  <body class="bg-default">
    <div class="login">
      <div class="login-body">
        <a class="login-brand" href="javascript:void(0)">
          <img class="img-responsive" src="{!! asset('public/img/logo.svg') !!}" alt="Elephant">
        </a>
        <div class="login-form">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were problems with input:
                    <br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
          <form data-toggle="md-validator" role="form" method="POST" action="{{ url('password/email') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="md-form-group md-label-floating">
              <input class="md-form-control" type="email" name="email" value="{{ old('email') }}" spellcheck="false" autocomplete="off" data-msg-required="Please enter your email address." required>
              <label class="md-control-label">Email address</label>
              <span class="md-help-block">We'll send you an email to reset your password.</span>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Send password reset email</button>
          </form>
        </div>
      </div>
      <!-- <div class="login-footer">
        Don't have an account? <a href="signup-3.html">Sign Up</a>
      </div> -->
    </div>
    <script type="text/javascript" src="{!! asset('public/js/login/vendor.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/js/login/TalGro.min.js') !!}"></script>
  </body>
</html>