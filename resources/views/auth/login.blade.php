<!DOCTYPE html>
<html lang="en">
 <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TalGro</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
   <link href="{{ asset('public/css/login/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/login/TalGro.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/login/login.min.css') }}" rel="stylesheet">
</head>
  
  <body class="bg-default">

    <div class="login" >
        <ul class="nav nav-tabs nav-justified">
            <li class="active">
                <a href="login.html#home-11" data-toggle="tab">Login</a>
            </li>
            <li>
                <a href="signup.html#profile-11" data-toggle="tab">Sign Up</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="home-11">
                <div class="login-body">
                    <a class="login-brand" href="index.html">
                        <img class="img-responsive"  src="{!! asset('public/img/logo.svg') !!}" alt="TalGro">
                    </a>
                    <!--<h3 class="login-heading text-center">Login</h3>-->
                    <div class="login-form">
                        @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <!-- <strong>Whoops!</strong> There were problems with input:
                              <br><br> -->
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                        @endif
                        <form data-toggle="md-validator" class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="md-form-group md-label-floating">
                                <input class="md-form-control" type="email" value="{{ old('email') }}" name="email" spellcheck="false" autocomplete="off" data-msg-required="Please enter your email address." required>
                                <label class="md-control-label">Email</label>
                            </div>
                            <div class="md-form-group md-label-floating">
                                <input class="md-form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
                                <label class="md-control-label">Password</label>
                            </div>
                            <div class="md-form-group md-custom-controls">
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" checked="checked">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-label">Keep me signed in</span>
                                </label>
                                <span aria-hidden="true"> · </span>
                                <a href="{{ route('auth.password.reset') }}">Forgot password?</a>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Log in</button>
                        </form>
                    </div>
                </div>
                <div class="login-footer">
                    Don't have an account? <a href="#profile-11" data-toggle="tab">Sign Up</a>
                </div>
            </div>
            <div class="tab-pane fade" id="profile-11">
                <div class="login-body">
                    <a class="login-brand" href="index.html">
                       <img class="img-responsive"  src="{!! asset('public/img/logo.svg') !!}" alt="TalGro">
                    </a>
                    <!--<h3 class="login-heading text-center">Sign Up</h3>-->
                    <div class="login-form">
                        
                        <form data-toggle="md-validator">

                            <div class="md-form-group md-label-floating">
                                <input class="md-form-control" type="email" name="email" spellcheck="false" autocomplete="off" data-msg-required="Please enter your email address." required>
                                <label class="md-control-label">Email</label>
                            </div>
                            <div class="md-form-group md-label-floating">
                                <input class="md-form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
                                <label class="md-control-label">Password</label>
                            </div>
                            <div class="md-form-group md-label-floating">
                                <input class="md-form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
                                <label class="md-control-label">Confirm Password</label>
                            </div>
                            <div class="md-form-group md-custom-controls">
                                <label class="custom-control custom-control-primary custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" checked="checked">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-label">Accept</span>
                                </label>
                                <span aria-hidden="true"> · </span>
                                <a href="#">Terms & Conditions</a>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Sign Up</button>
                            
                        </form>
                    </div>
                </div>
                <div class="login-footer">
                    Already have an account? <a href="#home-11" data-toggle="tab">Login</a>
                </div>
            </div>
        </div>
    </div>

    
    
    <script type="text/javascript" src="{!! asset('public/js/login/vendor.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('public/js/login/TalGro.min.js') !!}"></script>
    
  </body>
</html>